// DTO DEL PROYECTO NORMAL //

package clinicauna.model;

import clinicauna.services.UsuariosService;
import clinicauna.util.LocalDateAdapter;
import clinicauna.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "MantmedicosDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class MedicoDto {
    
    @XmlTransient
    public SimpleStringProperty idmed;
    @XmlTransient
    public SimpleStringProperty codigomed;
    @XmlTransient
    public SimpleStringProperty foliomed;
    @XmlTransient
    public SimpleStringProperty carnemed;
    @XmlTransient
    public ObjectProperty<LocalDateTime> inijornadamed;
    @XmlTransient
    public ObjectProperty<LocalDateTime> finjornadamed;
    @XmlTransient
    public SimpleStringProperty espaciosmed;
    @XmlTransient
    public ObjectProperty<String> estadomed;
    
    @XmlTransient
    public UsuarioDto idus;   // FK DEL MANTENIMIENTO DE USUARIOS //
    @XmlTransient
    public PersonaDto idPers;
    
    @XmlTransient
    public boolean nuevo;
    
    public MedicoDto(){
        this.idmed = new SimpleStringProperty("");
        this.codigomed = new SimpleStringProperty("");
        this.foliomed = new SimpleStringProperty("");
        this.carnemed = new SimpleStringProperty("");
        this.inijornadamed = new SimpleObjectProperty("");
        this.finjornadamed = new SimpleObjectProperty("");
        this.espaciosmed = new SimpleStringProperty("");
        this.estadomed = new SimpleObjectProperty();
        
        this.idus = new UsuarioDto(); // FK DEL MANTENIMIENTO DE USUARIOS //
        this.idPers = new PersonaDto(); // FK DE LA TABLA DE PERSONAS //
    }

   public Long getIdmed() {
        if(idmed.get()!=null && !idmed.get().isEmpty())
            return Long.valueOf(idmed.get());
        else
            return null;
    }

    public void setIdmed(Long idmed) {
        this.idmed.set(idmed.toString());
    }

    // FK DEL MANTENIMIENTO DE USUARIOS //
    public UsuarioDto getIdus() {
        return idus;
    }

    public void setIdus(UsuarioDto idus) {
        this.idus = idus;
    }
    
    // FK DEL ID LA TABLA PERSONA //

    public PersonaDto getIdPers() {
        return idPers;
    }

    public void setIdPers(PersonaDto idPers) {
        this.idPers = idPers;
    }
    
    public String getCodigomed() {
        return codigomed.get();
    }

    public void setCodigomed(String codigomed) {
        this.codigomed.set(codigomed);
    }

    public String getFoliomed() {
        return foliomed.get();
    }

    public void setFoliomed(String foliomed) {
        this.foliomed.set(foliomed);
    }

    public String getCarnemed() {
        return carnemed.get();
    }

    public void setCarnemed(String carnemed) {
        this.carnemed.set(carnemed);
    }
        
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getInijornadamed() {
        return inijornadamed.get();
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setInijornadamed(LocalDateTime inijornadamed) {
        this.inijornadamed.set(inijornadamed);
    }
    
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getFinjornadamed() {
        return finjornadamed.get();
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setFinjornadamed(LocalDateTime finjornadamed) {
        this.finjornadamed.set(finjornadamed);
    }    
    
    public String getEspaciosmed() {
        return espaciosmed.get();
    }

    public void setEspaciosmed(String espaciosmed) {
        this.espaciosmed.set(espaciosmed);
    }

    public String getEstadomed() {
        return estadomed.get();
    }

    public void setEstadomed(String estadomed) {
        this.estadomed.set(estadomed);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "MantmedicosDto{" + "idmed = " + idmed + ", codigomed = " + codigomed + ", foliomed = " + foliomed +
                ", carnemed = " + carnemed + ", inijornadamed = " + inijornadamed + ", finjornadamed = " + finjornadamed + 
                ", espaciosmed = " + espaciosmed + ", estadomed = " + estadomed + ", FK idus = " + idus.toString() + '}';
    }

    
}
