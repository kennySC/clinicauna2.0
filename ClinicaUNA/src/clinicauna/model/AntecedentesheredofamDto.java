// DTO DEL CLIENTE //

package clinicauna.model;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "AntecedentesheredofamDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class AntecedentesheredofamDto {

    @XmlTransient
    private SimpleStringProperty idantecedheredfam;
    @XmlTransient
    public SimpleStringProperty enfermheredanteced;
    @XmlTransient
    public SimpleStringProperty parentescoanteced;
    // FK //
    @XmlTransient
    public EncabezadoexpedienteDto idencabexp;
    @XmlTransient
    public boolean nuevo;
    
    public AntecedentesheredofamDto() {
        this.idantecedheredfam = new SimpleStringProperty();
        this.enfermheredanteced = new SimpleStringProperty("");
        this.parentescoanteced = new SimpleStringProperty("");
        this.idencabexp = new EncabezadoexpedienteDto();
    }

    public Long getIdantecedheredfam() {
        if (idantecedheredfam.get() != null && !idantecedheredfam.get().isEmpty()) {
            return Long.valueOf(idantecedheredfam.get());
        } else {
            return null;
        }
    }

    public void setIdantecedheredfam(Long idantecedheredfam) {
        this.idantecedheredfam.set(idantecedheredfam.toString());
    }

    public String getEnfermheredanteced() {
        return enfermheredanteced.get();
    }

    public void setEnfermheredanteced(String enfermheredanteced) {
        this.enfermheredanteced.set(enfermheredanteced);
    }

    public String getParentescoanteced() {
        return parentescoanteced.get();
    }

    public void setParentescoanteced(String parentescoanteced) {
        this.parentescoanteced.set(parentescoanteced);
    }

    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto idencabexp) {
        this.idencabexp = idencabexp;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "AntecedentesheredofamDto{" + "idantecedheredfam=" + idantecedheredfam + 
                ", enfermheredanteced=" + enfermheredanteced + ", parentescoanteced=" + parentescoanteced + 
                ", idencabexp=" + idencabexp + '}';
    }

    
    
}
