// DTO DEL PROYECTO NORMAL //

package clinicauna.model;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "EncabezadoexpedienteDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class EncabezadoexpedienteDto {
    
    @XmlTransient
    public SimpleStringProperty idencabexp;
    @XmlTransient
    public SimpleStringProperty hospitalizacionesencabexp;
    @XmlTransient
    public SimpleStringProperty operacionesencabexp;
    @XmlTransient
    public SimpleStringProperty alergiasencabexp;
    @XmlTransient
    public SimpleStringProperty tratamientosencabexp;
    @XmlTransient
    public SimpleStringProperty patologicoencabexp;
    @XmlTransient
    public boolean nuevo;
    
    @XmlTransient
    public RegistropacientesDto idpaciente;   // FK DEL REGISTRO DE PACIENTES //
    
    public EncabezadoexpedienteDto(){
        this.idencabexp = new SimpleStringProperty();
        this.hospitalizacionesencabexp = new SimpleStringProperty();
        this.operacionesencabexp = new SimpleStringProperty();
        this.alergiasencabexp = new SimpleStringProperty();
        this.tratamientosencabexp = new SimpleStringProperty();
        this.patologicoencabexp = new SimpleStringProperty();
        
        this.idpaciente = new RegistropacientesDto();  // FK DEL REGISTRO DE PACIENTES //
    }

    public Long getIdencabexp() {
        if(idencabexp.get()!=null && !idencabexp.get().isEmpty())
            return Long.valueOf(idencabexp.get());
        else
            return null;
    }

    public void setIdencabexp(Long idencabexp) {
        this.idencabexp.set(idencabexp.toString());
    }
    
    // FK DEL REGISTRO DE PACIENTES //

    public RegistropacientesDto getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(RegistropacientesDto idpaciente) {
        this.idpaciente = idpaciente;
    }
    

    public String getHospitalizacionesencabexp() {
        return hospitalizacionesencabexp.get();
    }

    public void setHospitalizacionesencabexp(String hospitalizacionesencabexp) {
        this.hospitalizacionesencabexp.set(hospitalizacionesencabexp);
    }

    public String getOperacionesencabexp() {
        return operacionesencabexp.get();
    }

    public void setOperacionesencabexp(String operacionesencabexp) {
        this.operacionesencabexp.set(operacionesencabexp);
    }

    public String getAlergiasencabexp() {
        return alergiasencabexp.get();
    }

    public void setAlergiasencabexp(String alergiasencabexp) {
        this.alergiasencabexp.set(alergiasencabexp);
    }

    public String getTratamientosencabexp() {
        return tratamientosencabexp.get();
    }

    public void setTratamientosencabexp(String tratamientosencabexp) {
        this.tratamientosencabexp.set(tratamientosencabexp);
    }

    public String getPatologicoencabexp() {
        return patologicoencabexp.get();
    }

    public void setPatologicoencabexp(String patologicoencabexp) {
        this.patologicoencabexp.set(patologicoencabexp);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "EncabezadoexpedienteDto{" + "idencabexp = " + idencabexp + ", hospitalizacionesencabexp = " + 
                hospitalizacionesencabexp + ", operacionesencabexp = " + operacionesencabexp +
                ", alergiasencabexp = " + alergiasencabexp + ", tratamientosencabexp = " + tratamientosencabexp + 
                ", patologicoencabexp = " + patologicoencabexp + ", FK idp = " + idpaciente + '}';
    }
    
    
    
}
