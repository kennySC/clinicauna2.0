// DTO DEL PROYECTO NORMAL //

package clinicauna.model;

import clinicauna.util.LocalDateAdapter;
import clinicauna.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "DetalleexpedienteDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class DetalleexpedienteDto {
    
    @XmlTransient
    public SimpleStringProperty iddetexp;
    @XmlTransient
    public ObjectProperty<LocalDate> fechadetexp;
    // PROBABLEMENTE NO VA A SERVIR YA QUE ES HORA Y LOCALDATE NO INCLUYE HORA //
    @XmlTransient
    public ObjectProperty<LocalDateTime> horadetexp;
    @XmlTransient
    public SimpleStringProperty presiondetexp;
    @XmlTransient
    public SimpleStringProperty freccarddetexp;
    @XmlTransient
    public SimpleStringProperty pesodetexp;
    @XmlTransient
    public SimpleStringProperty talladetexp;
    @XmlTransient
    public SimpleStringProperty temperaturadetexp;
    @XmlTransient
    public SimpleStringProperty imcdetexp;
    @XmlTransient
    public SimpleStringProperty motivoconsultadetexp;
    @XmlTransient
    public SimpleStringProperty planatenciondetexp;
    @XmlTransient
    public SimpleStringProperty anotacionesdetexp;
    @XmlTransient
    public SimpleStringProperty observdetexp;
    @XmlTransient
    public SimpleStringProperty examfisicodetexp;
    @XmlTransient
    public SimpleStringProperty tratamientodetexp;
    @XmlTransient
    public boolean nuevo;
    
    @XmlTransient
    public EncabezadoexpedienteDto idencabexp;   // FK DEL ENCABEZADO DEL EXPEDIENTE //
    
    // CONSTRUCTOR //
    public DetalleexpedienteDto(){
        this.iddetexp = new SimpleStringProperty("");
        this.fechadetexp = new SimpleObjectProperty();
        this.horadetexp = new SimpleObjectProperty();
        this.presiondetexp = new SimpleStringProperty("");
        this.freccarddetexp = new SimpleStringProperty("");
        this.pesodetexp = new SimpleStringProperty("");
        this.talladetexp = new SimpleStringProperty("");
        this.temperaturadetexp = new SimpleStringProperty("");
        this.imcdetexp = new SimpleStringProperty("");
        this.motivoconsultadetexp = new SimpleStringProperty("");
        this.planatenciondetexp = new SimpleStringProperty("");
        this.anotacionesdetexp = new SimpleStringProperty("");
        this.observdetexp = new SimpleStringProperty("");
        this.examfisicodetexp = new SimpleStringProperty("");
        this.tratamientodetexp = new SimpleStringProperty("");
        
        this.idencabexp = new EncabezadoexpedienteDto();   // FK DEL ENCABEZADO DEL EXPEDIENTE //
    }

    public Long getIddetexp() {
        if(iddetexp.get()!=null && !iddetexp.get().isEmpty())
            return Long.valueOf(iddetexp.get());
        else
            return null;
    }

    public void setIddetexp(Long iddetexp) {
        this.iddetexp.set(iddetexp.toString());
    }
    
    // FK DEL ENCABEZADO DEL EXPEDIENTE //
    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto idencabexp) {
        this.idencabexp = idencabexp;
    }
    
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFechadetexp() {
        return fechadetexp.getValue();
    }
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFechadetexp(LocalDate fechadetexp) {
        this.fechadetexp.setValue(fechadetexp);
    }
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getHoradetexp() {
        return horadetexp.getValue();
    }
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setHoradetexp(LocalDateTime horadetexp) {
        this.horadetexp.setValue(horadetexp);
    }

    public double getPresiondetexp() {
        return Double.parseDouble(presiondetexp.get());
    }

    public void setPresiondetexp(double presiondetexp) {
        this.presiondetexp.set(String.valueOf(presiondetexp));
    }

    public String getFreccarddetexp() {
        return freccarddetexp.get();
    }

    public void setFreccarddetexp(String freccarddetexp) {
        this.freccarddetexp.set(freccarddetexp);
    }

    public double getPesodetexp() {
        return Double.parseDouble(pesodetexp.get());
    }

    public void setPesodetexp(double pesodetexp) {
        this.pesodetexp.set(String.valueOf(pesodetexp));
    }

    public String getTalladetexp() {
        return talladetexp.get();
    }

    public void setTalladetexp(String talladetexp) {
        this.talladetexp.set(talladetexp);
    }

    public double getTemperaturadetexp() {
        return Double.parseDouble(temperaturadetexp.get());
    }

    public void setTemperaturadetexp(double temperaturadetexp) {
        this.temperaturadetexp.set(String.valueOf(temperaturadetexp));
    }

    public double getImcdetexp() {
        return Double.parseDouble(imcdetexp.get());
    }

    public void setImcdetexp(double imcdetexp) {
        this.imcdetexp.set(String.valueOf(imcdetexp));
    }

    public String getMotivoconsultadetexp() {
        return motivoconsultadetexp.get();
    }

    public void setMotivoconsultadetexp(String motivoconsultadetexp) {
        this.motivoconsultadetexp.set(motivoconsultadetexp);
    }

    public String getPlanatenciondetexp() {
        return planatenciondetexp.get();
    }

    public void setPlanatenciondetexp(String planatenciondetexp) {
        this.planatenciondetexp.set(planatenciondetexp);
    }

    public String getAnotacionesdetexp() {
        return anotacionesdetexp.get();
    }

    public void setAnotacionesdetexp(String anotacionesdetexp) {
        this.anotacionesdetexp.set(anotacionesdetexp);
    }

    public String getObservdetexp() {
        return observdetexp.get();
    }

    public void setObservdetexp(String observdetexp) {
        this.observdetexp.set(observdetexp);
    }

    public String getExamfisicodetexp() {
        return examfisicodetexp.get();
    }

    public void setExamfisicodetexp(String examfisicodetexp) {
        this.examfisicodetexp.set(examfisicodetexp);
    }

    public String getTratamientodetexp() {
        return tratamientodetexp.get();
    }

    public void setTratamientodetexp(String tratamientodetexp) {
        this.tratamientodetexp.set(tratamientodetexp);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    @Override
    public String toString() {
        return "DetalleexpedienteDto{" + "iddetexp = " + iddetexp + ", fechadetexp = " + fechadetexp +
                ", horadetexp = " + horadetexp + ", presiondetexp = " + presiondetexp + ", freccarddetexp = " +
                freccarddetexp + ", pesodetexp = " + pesodetexp + ", talladetexp = " + talladetexp + 
                ", temperaturadetexp = " + temperaturadetexp + ", imcdetexp = " + imcdetexp + ", motivoconsultadetexp = " +
                motivoconsultadetexp + ", planatenciondetexp = " + planatenciondetexp + ", anotacionesdetexp = " + 
                anotacionesdetexp + ", observdetexp = " + observdetexp + ", examfisicodetexp = " + examfisicodetexp +
                ", tratamientodetexp = " + tratamientodetexp + ", FK idencabexp = " + idencabexp + '}';
    }
    
    
    
}
