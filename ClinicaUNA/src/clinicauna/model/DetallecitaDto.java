// DTO DEL PROYECTO NORMAL //
package clinicauna.model;

import clinicauna.services.PacienteService;
import clinicauna.util.LocalDateAdapter;
import clinicauna.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "DetallecitaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class DetallecitaDto {

    @XmlTransient
    public SimpleStringProperty iddetcita;
    @XmlTransient
    public ObjectProperty<LocalDate> fechacita;
    @XmlTransient
    public ObjectProperty<LocalDateTime> horacita;
    @XmlTransient
    private SimpleStringProperty espaciomedcita;
    @XmlTransient
    public SimpleStringProperty estadocita;
    @XmlTransient
    public SimpleStringProperty motivocita;

    // FK QUE RECIBE EL DETALLE DE CITA //
    @XmlTransient
    public MedicoDto idmed;
    @XmlTransient
    public UsuarioDto idus;
    @XmlTransient
    public RegistropacientesDto idpaciente;
    @XmlTransient
    public boolean nuevo;

    // CONSTRUCTOR //
    public DetallecitaDto() {
        this.iddetcita = new SimpleStringProperty();
        this.fechacita = new SimpleObjectProperty();
        this.horacita = new SimpleObjectProperty();
        this.espaciomedcita = new SimpleStringProperty();
        this.estadocita = new SimpleStringProperty();
        this.motivocita = new SimpleStringProperty();

        // FK QUE RECIBE EL DETALLE DE CITA //
        this.idmed = new MedicoDto();
        this.idus = new UsuarioDto();
        this.idpaciente = new RegistropacientesDto();
    }

    public Long getIddetcita() {
        if (iddetcita.get() != null && !iddetcita.get().isEmpty()) {
            return Long.valueOf(iddetcita.get());
        } else {
            return null;
        }
    }

    public void setIddetcita(Long iddetcita) {
        this.iddetcita.set(iddetcita.toString());
    }
    
    // SET Y GET DE LAS LLAVES FORANEAS DEL ID USUARIO, MEDICO, Y PACIENTES //
    public MedicoDto getIdmed() {
        return idmed;
    }

    public void setIdmed(MedicoDto idmed) {
        this.idmed = idmed;
    }

    public UsuarioDto getIdus() {
        return idus;
    }

    public void setIdus(UsuarioDto idus) {
        this.idus = idus;
    }

    public RegistropacientesDto getIdpaciente() {
        return idpaciente;
    }
    
    public void setIdpaciente(RegistropacientesDto idpaciente) {
        this.idpaciente = idpaciente;
    }

    ///////////////////////////////////////////////////////////////////////////
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFechacita() {
        return fechacita.getValue();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFechacita(LocalDate fechacita) {
        this.fechacita.setValue(fechacita);
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getHoracita() {
        return horacita.getValue();
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setHoracita(LocalDateTime horacita) {
        this.horacita.setValue(horacita);
    }

    public String getEspaciocita() {
        return espaciomedcita.get();
    }

    public void setEspaciocita(String espaciomedcita) {
        this.espaciomedcita.set(espaciomedcita);
    }

    public String getEstadocita() {
        return estadocita.get();
    }

    public void setEstadocita(String estadocita) {
        this.estadocita.set(estadocita);
    }

    public String getMotivocita() {
        return motivocita.get();
    }

    public void setMotivocita(String motivocita) {
        this.motivocita.set(motivocita);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
}
