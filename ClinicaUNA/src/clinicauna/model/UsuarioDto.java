// DTO DEL PROYECTO NORMAL //

package clinicauna.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "MantusuariosDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class UsuarioDto {
    
    @XmlTransient
    public SimpleStringProperty idus;    
    @XmlTransient
    public ObjectProperty<String> tipous;
    @XmlTransient
    public SimpleStringProperty usernameus;
    @XmlTransient
    public SimpleStringProperty passwordus;
    @XmlTransient
    public ObjectProperty<String> estadous;
    @XmlTransient
    public ObjectProperty<String> idiomaus;
    
    // FK DE LA TABLA DE PERSONA //
    @XmlTransient
    public PersonaDto idpers;
    
    @XmlTransient
    public boolean nuevo;
    
    public UsuarioDto(){
        this.idus = new SimpleStringProperty("");
        this.tipous = new SimpleObjectProperty("");
        this.usernameus = new SimpleStringProperty("");
        this.passwordus = new SimpleStringProperty("");
        this.estadous = new SimpleObjectProperty("");
        this.idiomaus = new SimpleObjectProperty("");
        this.idpers = new PersonaDto();
    }

    public Long getIdus() {
        if(idus.get()!=null && !idus.get().isEmpty())
            return Long.valueOf(idus.get());
        else
            return null;
    }

    public void setIdus(Long idus) {
        this.idus.set(idus.toString());
    }

    public PersonaDto getIdpers() {
        return idpers;
    }

    public void setIdpers(PersonaDto idpers) {
        this.idpers = idpers;
    }   

    public String getTipous() {
        return tipous.get();
    }

    public void setTipous(String tipous) {
        this.tipous.set(tipous);
    }

    public String getUsernameus() {
        return usernameus.get();
    }

    public void setUsernameus(String usernameus) {
        this.usernameus.set(usernameus);
    }

    public String getPasswordus() {
        return passwordus.get();
    }

    public void setPasswordus(String passwordus) {
        this.passwordus.set(passwordus);
    }

    public String getEstadous() {
        return estadous.get();
    }

    public void setEstadous(String estadous) {
        this.estadous.set(estadous);
    }

    public String getIdiomaus() {
        return idiomaus.get();
    }

    public void setIdiomaus(String idiomaus) {
        this.idiomaus.set(idiomaus);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "MantusuariosDto{" + "idus=" + idus + ", tipous=" + tipous + ", usernameus=" + usernameus + 
                ", passwordus=" + passwordus + ", estadous=" + estadous + ", idiomaus=" + idiomaus + 
                ", idpers=" + idpers + '}';
    }

    
    
    
    
}
