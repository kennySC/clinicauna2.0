// DTO DEL PROYECTO NORMAL //

package clinicauna.model;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */

@XmlRootElement(name = "PersonaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class PersonaDto {
    
    @XmlTransient
    public SimpleStringProperty idpers;
    @XmlTransient
    public SimpleStringProperty nombrepers;
    @XmlTransient
    public SimpleStringProperty apellidospersona;
    @XmlTransient
    public SimpleStringProperty cedulapers;
    @XmlTransient
    public SimpleStringProperty correopers;
    @XmlTransient
    public boolean nuevo;
    
    public PersonaDto(){
        this.idpers = new SimpleStringProperty();
        this.nombrepers = new SimpleStringProperty("");
        this.apellidospersona = new SimpleStringProperty("");
        this.cedulapers = new SimpleStringProperty("");
        this.correopers = new SimpleStringProperty("");
    }
    
    public Long getIdpers() {
        if(idpers.get()!=null && !idpers.get().isEmpty())
            return Long.valueOf(idpers.get());
        else
            return null;
    }

    public void setIdpers(Long idpers) {
        this.idpers.set(idpers.toString());
    }

    public String getNombrepers() {
        return nombrepers.get();
    }

    public void setNombrepers(String nombrepers) {
        this.nombrepers.set(nombrepers);
    }

    public String getApellidospersona() {
        return apellidospersona.get();
    }

    public void setApellidospersona(String apellidospersona) {
        this.apellidospersona.set(apellidospersona);
    }

    public String getCedulapers() {
        return cedulapers.get();
    }

    public void setCedulapers(String cedulapers) {
        this.cedulapers.set(cedulapers);
    }

    public String getCorreopers() {
        return correopers.get();
    }

    public void setCorreopers(String correopers) {
        this.correopers.set(correopers);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
}
