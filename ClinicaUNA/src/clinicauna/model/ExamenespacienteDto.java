// DTO DEL PROYECTO NORMAL //  

package clinicauna.model;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "ExamenespacienteDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class ExamenespacienteDto {
    
    @XmlTransient
    public SimpleStringProperty idexamp;
    @XmlTransient
    public SimpleStringProperty nombreexamp;
    @XmlTransient
    public ObjectProperty<LocalDate> fechaexamp;
    @XmlTransient
    public SimpleStringProperty anotacionesexamp;
    
    @XmlTransient
    public SimpleStringProperty idencabexp;   // FK DEL DETALLE DE EXPEDIENTE //
    @XmlTransient
    public boolean nuevo;
    
    public ExamenespacienteDto(){
        this.idexamp = new SimpleStringProperty();
        this.nombreexamp = new SimpleStringProperty();
        this.fechaexamp = new SimpleObjectProperty();
        this.anotacionesexamp = new SimpleStringProperty();
        
        this.idencabexp = new SimpleStringProperty(); // FK DEL DETALLE DE EXPEDIENTE //
    }

    public Long getIdexamp() {
        if(idexamp.get()!=null && !idexamp.get().isEmpty())
            return Long.valueOf(idexamp.get());
        else
            return null;
    }

    public void setIdexamp(Long idexamp) {
        this.idexamp.set(idexamp.toString());
    }
    
    // FK DEL DETALLE DE EXPEDIENTE //
    public Long getIdencabexp() {
        if(idencabexp.get()!=null && !idencabexp.get().isEmpty())
            return Long.valueOf(idencabexp.get());
        else
            return null;
    }

    public void setIdencabexp(Long iddetexp) {
        this.idencabexp.set(iddetexp.toString());
    }

    public String getNombreexamp() {
        return nombreexamp.get();
    }

    public void setNombreexamp(String nombreexamp) {
        this.nombreexamp.set(nombreexamp);
    }

    public ObjectProperty<LocalDate> getFechaexamp() {
        return fechaexamp;
    }

    public void setFechaexamp(ObjectProperty<LocalDate> fechaexamp) {
        this.fechaexamp = fechaexamp;
    }

    public String getAnotacionesexamp() {
        return anotacionesexamp.get();
    }

    public void setAnotacionesexamp(String anotacionesexamp) {
        this.anotacionesexamp.set(anotacionesexamp);
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "ExamenespacienteDto{" + "idexam = " + idexamp + ", nombreexamp = " + nombreexamp + 
                ", fechaexamp = " + fechaexamp + ", anotacionesexamp = " + anotacionesexamp + 
                ", FK iddetexp = " + idencabexp + '}';
    }
    
    
    
}
