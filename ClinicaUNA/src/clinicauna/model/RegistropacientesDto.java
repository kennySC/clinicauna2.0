// DTO DEL WEBSERVICE //
package clinicauna.model;

import clinicauna.util.LocalDateAdapter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "RegistropacientesDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class RegistropacientesDto {

    @XmlTransient
    public SimpleStringProperty idpaciente;
    @XmlTransient
    public SimpleObjectProperty <LocalDate>  fechanacimientop;
    @XmlTransient
    public SimpleObjectProperty<String> generop;
    @XmlTransient
    public PersonaDto idpers;
    @XmlTransient
    public SimpleStringProperty telefonoPaciente;
    public Boolean nuevo;

    // CONSTRUCTOR VACIO //
    public RegistropacientesDto() {
        this.fechanacimientop = new SimpleObjectProperty();
        this.generop = new SimpleObjectProperty("");
        this.idpaciente = new SimpleStringProperty();
        this.idpers = new PersonaDto();
        this.telefonoPaciente = new SimpleStringProperty("");
        this.nuevo = true;
    }

    public Long getIdpaciente() {
        if(this.idpaciente!=null && this.idpaciente.getValue()!=null){
            return Long.valueOf(this.idpaciente.getValue());            
        }else{
            return null;
        }
    }

    public void setIdpaciente(Long idp) {
        this.idpaciente.setValue(idp.toString());
    }

    public PersonaDto getIdpers() {
        return idpers;
    }

    public void setIdpers(PersonaDto idpers) {
        this.idpers = idpers;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFechanacimientop() {
        return fechanacimientop.getValue();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFechanacimientop(LocalDate fechanacimientop) {
        this.fechanacimientop.setValue(fechanacimientop);
    }

    public String getGenerop() {
        return generop.get();
    }

    public void setGenerop(String generop) {
        this.generop.set(generop);
    }

    public String getTelefonoPaciente() {
        return telefonoPaciente.getValue();
    }

    public void setTelefonoPaciente(String numtelefpaciente) {
        this.telefonoPaciente.setValue(numtelefpaciente);
    }

    public Boolean getNuevo() {
        return nuevo;
    }

    public void setNuevo(Boolean nuevo) {
        this.nuevo = nuevo;
    }

    @Override
    public String toString() {
        return "RegistropacientesDto{" + "idpaciente=" + idpaciente + ", fechanacimientop=" + fechanacimientop
                + ", generop=" + generop + ", idpers=" + idpers + '}';
    }

}
