package clinicauna.services;

import clinicauna.model.MedicoDto;
import clinicauna.model.UsuarioDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matthew Miranda
 */
public class MedicoService {

    public Respuesta guardarMedico(MedicoDto medico) {
        try {
            medico.idus.setIdpers(medico.idPers);
            //  Guardamos primero al usuario del medico, que a su vez guardara la persona
            Respuesta resUs = new UsuariosService().guardarUsuario(medico.getIdus());
            if (resUs.getEstado()) {
                //  Ahora guardamos el medico
                medico.setIdus((UsuarioDto) resUs.getResultado("Usuario"));
                medico.setIdPers(medico.getIdus().getIdpers());
                System.out.println(medico.toString());
                Request request = new Request("MedicoController/GuardarMedico");
                request.post(medico);
                if (request.isError()) {
                    System.out.println(request.getError());
                    return new Respuesta(false, "Error al guardar el medico", request.getError());
                }
                MedicoDto medDto = (MedicoDto) request.readEntity(MedicoDto.class);
                return new Respuesta(true, "Medico guardado con exito", "", "Medico", medDto);
            } else {
                return new Respuesta(false, "Error al guardar al usuario", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando al medico ", ex);
            return new Respuesta(false, "Error guardando al medico.", "GuardarMedico " + ex.getMessage());
        }
    }

    public Respuesta getMedicos(String cedula, String nombre, String apellidos/*, Integer carne*/) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("cedula", cedula);
            parametros.put("nombre", nombre);
            parametros.put("apellidos", apellidos);
//            parametros.put("carne", carne);
            Request req = new Request("MedicoController/medicos", "/{cedula}/{nombre}/{apellidos}"/*/{carne}*/, parametros);
            req.get();
            if (req.isError()) {
                return new Respuesta(false, req.getError(), "");
            }
            List<MedicoDto> medicos = (List<MedicoDto>) req.readEntity(new GenericType<List<MedicoDto>>() {});
            return new Respuesta(true, "", "", "Medicos", medicos);
        } catch (Exception e) {
            Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error obteniendo medicos.", e);
            return new Respuesta(false, "Error obteniendo medicos.", "getMedicos " + e.getMessage());
        }
    }

    public Respuesta eliminarMedico(MedicoDto medDto) {
        try {
            Long id = medDto.getIdmed();
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("MedicoController/eliminarMedico", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            Respuesta respMed = new UsuariosService().eliminarUsuario(medDto.idus);
            if (respMed.getEstado()) {
                return new Respuesta(true, "", "");
            } else {
                return new Respuesta(false, "", "");
            }

        } catch (Exception e) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error eliminando al medico.", e);
            return new Respuesta(false, "Error eliminando al medico.", "eliminarMedico " + e.getMessage());
        }
    }

}
