package clinicauna.services;

import clinicauna.controllers.ControllerCorreo;
import clinicauna.model.UsuarioDto;
import clinicauna.model.PersonaDto;
import clinicauna.util.Correo;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Kenneth Sibaja
 */
public class UsuariosService {

    public Respuesta validarUsuario(String usuario, String clave) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("usuario", usuario);
            parametros.put("clave", clave);
            Request request = new Request("UsuarioController/validarUsuario", "/{usuario}/{clave}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, " ", "", "Usuario", usuarioDto);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" + usuario + "]", ex);
            return new Respuesta(false, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta guardarUsuario(UsuarioDto usuario) {

        try {
            Respuesta resPers = new PersonaService().guardarPersona(usuario.getIdpers());
            if (resPers.getEstado()) {
                usuario.setIdpers((PersonaDto) resPers.getResultado("Persona"));
                Request request = new Request("UsuarioController/guardarUsuario");
                request.post(usuario);
                if (request.isError()) {
                    return new Respuesta(false, "Error al guardar el usuario", request.getError());
                }
                UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
                String correo = usuarioDto.idpers.getCorreopers();
                ControllerCorreo cCorreo = new ControllerCorreo();
//                
//                            cCorreo.getCorreo().formatoActivacion("Activacion Usuario", usuarioDto.getIdus());
//                            Respuesta resCorreo = cCorreo.enviarEmailHTML(correo);
//                                 
                return new Respuesta(true, "Usuario guardado con exito", "", "Usuario", usuarioDto);
            } else {
                return new Respuesta(false, "Error al guardar Persona", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando el usuario ", ex);
            return new Respuesta(false, "Error guardando el usuario.", "guardarUsuario " + ex.getMessage());
        }

    }

    public Respuesta getUsuarios(String cedula, String nombre, String apellidos) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("cedula", cedula);
            parametros.put("nombre", nombre);
            parametros.put("apellidos", apellidos);
            Request request = new Request("UsuarioController/usuarios", "/{cedula}/{nombre}/{apellidos}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            List<UsuarioDto> usuarios = (List<UsuarioDto>) request.readEntity(new GenericType<List<UsuarioDto>>() {
            });

            return new Respuesta(true, "", "", "Usuarios", usuarios);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios.", ex);
            return new Respuesta(false, "Error obteniendo usuarios.", "getUsuarios " + ex.getMessage());
        }
    }

    public Respuesta eliminarUsuario(UsuarioDto usuario) {
        try {
            Long id = usuario.getIdus();
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("UsuarioController/eliminarUsuario", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            Respuesta resPersona = new PersonaService().eliminarPersona(usuario.idpers.getIdpers());
            if (resPersona.getEstado()) {
                return new Respuesta(true, "", "");
            } else {
                return new Respuesta(false, "", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error eliminando el usuario.", ex);
            return new Respuesta(false, "Error eliminando el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

    public Respuesta recuperarContrasenaUsuario(String username) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("username", username);
            Request request = new Request("UsuarioController/recuperarContrasena", "/{username}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            UsuarioDto usuario = (UsuarioDto) request.readEntity(UsuarioDto.class);

            return new Respuesta(true, "", "", "Usuario", usuario);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios.", ex);
            return new Respuesta(false, "Error obteniendo usuarios.", "getUsuarios " + ex.getMessage());
        }
    }

    public Respuesta cambiarContrasenaUsuario(Long id, String newPassword) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            parametros.put("newPassword", newPassword);
            Request request = new Request("UsuarioController/cambiarContrasena", "/{id}/{newPassword}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            UsuarioDto usuario = (UsuarioDto) request.readEntity(UsuarioDto.class);

            return new Respuesta(true, "", "", "Usuario", usuario);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios.", ex);
            return new Respuesta(false, "Error obteniendo usuarios.", "getUsuarios " + ex.getMessage());
        }
    }

}
