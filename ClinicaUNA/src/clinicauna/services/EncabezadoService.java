/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.services;

import clinicauna.model.EncabezadoexpedienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kenneth Sibaja
 */
public class EncabezadoService {

    public Respuesta guardarExpediente(EncabezadoexpedienteDto expediente) {

        try {
            Request request = new Request("ExpedienteController/guardarExpediente");
            request.post(expediente);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "Error al guardar el expediente");
            }
            EncabezadoexpedienteDto expedienteDto = (EncabezadoexpedienteDto) request.readEntity(EncabezadoexpedienteDto.class);

            return new Respuesta(true, "Expediente guardado con exito", "", "Expediente", expedienteDto);

        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando el paciente ", ex);
            return new Respuesta(false, "Error guardando el Paciente.", "guardarPaciente " + ex.getMessage());
        }

    }
    
    public Respuesta cargarExpedinte(Long idPaciente){
        
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idPaciente", idPaciente);
            Request request = new Request("ExpedienteController/expediente", "/{idPaciente}", parametros);
            request.get();
            if(request.isError()){
                return new Respuesta(false, request.getError(), "Error al cargar el expediente");
            }
            EncabezadoexpedienteDto expedienteDto = (EncabezadoexpedienteDto) request.readEntity(EncabezadoexpedienteDto.class);
            return new Respuesta(true, "Expediente guardado con exito", "", "Expediente", expedienteDto);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error cargando el expediente ", ex);
            return new Respuesta(false, "Error cargando el expediente.", "cargarExpediente " + ex.getMessage());
        }
        
    }
    public Respuesta eliminarExpediente(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ExpedienteController/eliminarExpediente","/{id}",parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(PersonaService.class.getName()).log(Level.SEVERE, "Error eliminando el expediente.", ex);
            return new Respuesta(false, "Error eliminando el expediente.", "eliminarExpediente " + ex.getMessage());
        }
    }
    
}
