package clinicauna.services;

import clinicauna.model.DetallecitaDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Kenneth Sibaja
 */
public class DetalleCitaService {

    public Respuesta guardarDetCita(DetallecitaDto detCitaDto) {
        try {
            Request req = new Request("DetalleCitaController/guardarDetCita");
            req.post(detCitaDto);
            if (req.isError()) {
                return new Respuesta(false, req.getError(), "");
            }
            DetallecitaDto detcita = (DetallecitaDto) req.readEntity(DetallecitaDto.class);
            return new Respuesta(true, " ", "", "Cita", detcita);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando la cita.", ex);
            return new Respuesta(false, "Error guardando la cita.", "guardarDetCita " + ex.getMessage());
        }
    }

    public Respuesta eliminarCita(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("DetalleCitaController/eliminarCita", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(PersonaService.class.getName()).log(Level.SEVERE, "Error eliminando la cita.", ex);
            return new Respuesta(false, "Error eliminando la cita.", "eliminarCita " + ex.getMessage());
        }
    }

    public Respuesta getCitas(Long idMed) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idMed", idMed);
            Request request = new Request("DetalleCitaController/obtenerCitas", "/{idMed}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            List<DetallecitaDto> citas = (List<DetallecitaDto>) request.readEntity(new GenericType<List<DetallecitaDto>>(){});

            return new Respuesta(true, "", "", "Citas", citas);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error obteniendo pacientes.", ex);
            return new Respuesta(false, "Error obteniendo pacientes.", "getPacientes " + ex.getMessage());
        }

    }
    
    public Respuesta obtenerCitaPorPaciente(Long idpaciente) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idpaciente", idpaciente);
            Request request = new Request("DetalleCitaController/obtenerCitasPaciente", "/{idpaciente}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            List<DetallecitaDto> citas = (List<DetallecitaDto>) request.readEntity(new GenericType<List<DetallecitaDto>>(){});

            return new Respuesta(true, "", "", "Citas", citas);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error obteniendo pacientes.", ex);
            return new Respuesta(false, "Error obteniendo pacientes.", "getPacientes " + ex.getMessage());
        }

    }

}
