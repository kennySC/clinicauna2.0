/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.services;

import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperPrint;
import org.w3c.tools.codec.Base64Decoder;

/**
 *
 * @author Kenneth Sibaja
 */
public class ReportesService {

    public Respuesta reporteAgenda(Long idMed, String fecha1, String fecha2) {
        try {
            Map<String, Object> parametros = new HashMap<>();
//            parametros.put("reporte", reporte);
//            parametros.put("clave", clave);
            Request request = new Request("reporteController/validarreporte", "/{reporte}/{clave}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            return new Respuesta(true, " ", "", "reporte", "");
        } catch (Exception ex) {
            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, "Error obteniendo el reporte ", ex);
            return new Respuesta(false, "Error obteniendo el reporte.", "getreporte " + ex.getMessage());
        }
    }

    public Respuesta reporteExpediente(Long idPaciente) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idPaciente", idPaciente);
            Request request = new Request("ReportesController/generarReporteExpediente", "/{idPaciente}", parametros);
            request.get();
            if (request.isError()) {
                System.out.println("VaCiO");
                return new Respuesta(false, request.getError(), "");
            }
            String reporte = (String) request.readEntity(String.class);

            byte[] reporteBytes = Base64.getDecoder().decode(reporte);
            ByteArrayInputStream in = new ByteArrayInputStream(reporteBytes);
            ObjectInputStream is = new ObjectInputStream(in);
            JasperPrint report = (JasperPrint) is.readObject();                        
            return new Respuesta(true, " ", "", "reporte", report);
        } catch (Exception ex) {
            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, "Error obteniendo el reporte", ex);
            return new Respuesta(false, "Error obteniendo el reporte.", "getreporte " + ex.getMessage());
        }
    }

}
