// SERIVCE DEL CLIENTE //
package clinicauna.services;

import clinicauna.model.AntecedentesheredofamDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matthew Miranda
 */
public class AntecedentesHeredoFamService {

    public Respuesta guardarAntecedHF(AntecedentesheredofamDto antecedhfDto) {
        try {
            Request req = new Request("AntecedentesHFController/guardarAntecedente");
            req.post(antecedhfDto);
            if (req.isError()) {
                return new Respuesta(false, req.getError(), "");
            }
            AntecedentesheredofamDto anteced = (AntecedentesheredofamDto) req.readEntity(AntecedentesheredofamDto.class);
            return new Respuesta(true, " ", "", "DetExp", anteced);
        } catch (Exception ex) {
            Logger.getLogger(AntecedentesHeredoFamService.class.getName()).log(Level.SEVERE, "Error guardando el antecedente heredo familiar.", ex);
            return new Respuesta(false, "Error guardando el antecedente heredo familiar.", "guardarAntecedHF " + ex.getMessage());
        }
    }
    public Respuesta cargarAntecedHF(Long idExpediente){
        
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idExpediente", idExpediente);
            Request request = new Request("AntecedentesHFController/Antecedentes", "/{idExpediente}", parametros);
            request.get();
            if(request.isError()){
                return new Respuesta(false, request.getError(), "Error al cargar los antecedentes");
            }
            List<AntecedentesheredofamDto> antecedentes = (List<AntecedentesheredofamDto>) request.readEntity(new GenericType<List<AntecedentesheredofamDto>>() {});
            return new Respuesta(true, "", "", "Antecedentes", antecedentes);
        } catch (Exception ex) {
            Logger.getLogger(AntecedentesHeredoFamService.class.getName()).log(Level.SEVERE, "Error cargando el antecedentes ", ex);
            return new Respuesta(false, "Error cargando los antecedentes.", "cargarAntecedHF " + ex.getMessage());
        }
        
    }
    
    public Respuesta eliminarAntecedente(Long id) {
        try {            
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("AntecedentesHFController/eliminarAntecedente", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(Boolean.TRUE, "Eliminado Correctamente", "Antecedente eliminado Correctamente");
        } catch (Exception ex) {
            Logger.getLogger(AntecedentesHeredoFamService.class.getName()).log(Level.SEVERE, "Error eliminando el Antecedente.", ex);
            return new Respuesta(false, "Error eliminando el Antecedente.", "eliminarAntecedente " + ex.getMessage());
        }
    }
    
}
