/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.util;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.UsuarioDto;

/**
 *
 * @author Kenneth Sibaja
 */
public class Correo {

    private String usuarioCorreo = "clinicauna.prograiii@gmail.com";
    private String contrasena = "uqbtjxqtiixkgxoy";
    private String formatoHTML;
    private String asunto;
    private String linkActivacion = "http://localhost:8989/WsClinicaUNA/ws/UsuarioController/activarUsuario/";

    public Correo() {
    }

    public String getUsuarioCorreo() {
        return usuarioCorreo;
    }

    public void setUsuarioCorreo(String usuarioCorreo) {
        this.usuarioCorreo = usuarioCorreo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getFormatoHTML() {
        return formatoHTML;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public void formatoActivacion(String titulo, Long idUs) {

        String Contenido = "De clic en el enlace si desea activar su usuario,"
                + "<br> esto es necesario para hacer uso de la aplicaion<br> ClinicaUNA";

        linkActivacion += idUs;
        formatoHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "\n"
                + "<head>\n"
                + "    <meta charset=\"UTF-8\">\n"
                + "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n"
                + "    <meta name=\"x-apple-disable-message-reformatting\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta content=\"telephone=no\" name=\"format-detection\">\n"
                + "    <title></title>\n"
                + "    <!--[if (mso 16)]>\n"
                + "    <style type=\"text/css\">\n"
                + "    a {text-decoration: none;}\n"
                + "    </style>\n"
                + "    <![endif]-->\n"
                + "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n"
                + "    <!--[if !mso]><!-- -->\n"
                + "    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "    <div class=\"es-wrapper-color\">\n"
                + "        <!--[if gte mso 9]>\n"
                + "			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n"
                + "				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\n"
                + "			</v:background>\n"
                + "		<![endif]-->\n"
                + "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "            <tbody>\n"
                + "                <tr>\n"
                + "                    <td class=\"esd-email-paddings\" valign=\"top\">\n"
                + "                        <table class=\"es-content esd-footer-popover\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n"
                + "                            <tbody>\n"
                + "                                <tr>\n"
                + "                                    <td class=\"esd-stripe\" style=\"background-color: rgb(208, 224, 227); background-size: cover;\" bgcolor=\"#d0e0e3\" align=\"center\">\n"
                + "                                        <table class=\"es-content-body\" style=\"background-color: transparent;\" width=\"640\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f6f6f6\" align=\"center\">\n"
                + "                                            <tbody>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p10t es-p20r es-p20l\" align=\"left\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td class=\"esd-block-image es-p40t es-p25b\" align=\"center\"><a href target=\"_blank\"><img src=\"https://demo.stripocdn.email/content/guids/fbb7d359-012a-4d16-93f2-f0aa9b5903ec/images/36281572729336698.png\" style=\"display: block;\" alt=\"Logo\" title=\"Logo\" width=\"335\"></a></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"center\" class=\"esd-block-text es-p25t es-p30b\">\n"
                + "                                                                                        <h1 style=\"color: #333333;\">" + titulo + "</h1>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p20t es-p15b es-p20r es-p20l\" align=\"left\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td esdev-links-color=\"#b7bdc9\" class=\"esd-block-text es-p15t es-p20b\" align=\"center\">\n"
                + "                                                                                        <p style=\"color: #333333; font-family: 'open sans', 'helvetica neue', helvetica, arial, sans-serif;\">" + Contenido + ".</p>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td class=\"esd-block-button es-p5t es-p40b\" align=\"center\"><span class=\"es-button-border\"><a href=\"" + linkActivacion + "\" class=\"es-button\" target=\"_blank\" style=\"color: rgb(255, 255, 255);\">Activar Cuenta</a></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                            </tbody>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </tbody>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </tbody>\n"
                + "        </table>\n"
                + "    </div>\n"
                + "</body>\n"
                + "\n"
                + "</html>";
    }

    public void formatoCambioContrasena(UsuarioDto usuario) {

        String contenido = "Cambio de contraseña<br>    "
                + usuario.idpers.getNombrepers() + "usted ha solicitado una contraseña de recuperacion<br>"
                + " Sus nuevas credenciales son: <br>       Usuario: "
                + usuario.getUsernameus() + "<br>        Contraseña: "
                + usuario.getPasswordus();

        formatoHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "\n"
                + "<head>\n"
                + "    <meta charset=\"UTF-8\">\n"
                + "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n"
                + "    <meta name=\"x-apple-disable-message-reformatting\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta content=\"telephone=no\" name=\"format-detection\">\n"
                + "    <title></title>\n"
                + "    <!--[if (mso 16)]>\n"
                + "    <style type=\"text/css\">\n"
                + "    a {text-decoration: none;}\n"
                + "    </style>\n"
                + "    <![endif]-->\n"
                + "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n"
                + "    <!--[if !mso]><!-- -->\n"
                + "    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "    <div class=\"es-wrapper-color\">\n"
                + "        <!--[if gte mso 9]>\n"
                + "			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n"
                + "				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\n"
                + "			</v:background>\n"
                + "		<![endif]-->\n"
                + "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "            <tbody>\n"
                + "                <tr class=\"gmail-fix\" height=\"0\">\n"
                + "                    <td>\n"
                + "                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\">\n"
                + "                            <tbody>\n"
                + "                                <tr>\n"
                + "                                    <td cellpadding=\"0\" cellspacing=\"0\" border=\"0\" height=\"0\" style=\"line-height: 1px; min-width: 600px;\"><img src=\"https://esputnik.com/repository/applications/images/blank.gif\" width=\"600\" height=\"1\" style=\"display: block; max-height: 0px; min-height: 0px; min-width: 600px; width: 600px;\" alt></td>\n"
                + "                                </tr>\n"
                + "                            </tbody>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "                <tr>\n"
                + "                    <td class=\"esd-email-paddings\" valign=\"top\">\n"
                + "                        <table class=\"esd-footer-popover es-content es-visible-simple-html-only\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n"
                + "                            <tbody>\n"
                + "                                <tr>\n"
                + "                                    <td class=\"esd-stripe\" style=\"background-color: rgb(208, 224, 227); background-size: cover;\" bgcolor=\"#d0e0e3\" align=\"center\">\n"
                + "                                        <table class=\"es-content-body\" style=\"background-color: transparent;\" width=\"640\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"transparent\" align=\"center\">\n"
                + "                                            <tbody>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p10t es-p20r es-p20l\" align=\"left\" style=\"background-position: center center; background-color: transparent;\" bgcolor=\"transparent\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td class=\"esd-block-image es-p40t es-p25b\" align=\"center\"><a href target=\"_blank\"><img src=\"https://demo.stripocdn.email/content/guids/522f7257-d437-4530-9fe8-c47e1a4d9195/images/64311572727901569.png\" style=\"display: block;\" alt=\"Logo\" title=\"Logo\" width=\"335\"></a></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"center\" class=\"esd-block-text es-p25t es-p30b\">\n"
                + "                                                                                        <h1 style=\"color: rgb(51, 51, 51); text-align: center;\">\"Contraseña de recuperacion\"</h1>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p20t es-p15b es-p20r es-p20l\" align=\"left\" style=\"background-position: left top; background-color: transparent;\" bgcolor=\"transparent\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td esdev-links-color=\"#b7bdc9\" class=\"esd-block-text es-p15t es-p20b\" align=\"left\">\n"
                + "                                                                                        <p style=\"color: rgb(51, 51, 51); text-align: center;\">"+ contenido +"</p>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                            </tbody>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </tbody>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </tbody>\n"
                + "        </table>\n"
                + "    </div>\n"
                + "</body>\n"
                + "\n"
                + "</html>";

    }
    public void formatoPaciente(DetallecitaDto cita, String titulo){
        String contenido;
        if(titulo.equalsIgnoreCase("Recordatorio")){
            contenido = "Estimado: "+ cita.idpaciente.idpers.getNombrepers()+" "+cita.idpaciente.idpers.getApellidospersona()+ ", "
                + "se le rcuerda que tiene una cita programada para el día"+cita.getFechacita().toString()+ " a las "+cita.getHoracita().toLocalTime().toString()+"h <br>";
        }else{
           contenido = "Estimado: "+ cita.idpaciente.idpers.getNombrepers()+" "+cita.idpaciente.idpers.getApellidospersona()+ ", "
                + "se le comunica que su cita ha sido programada exitosamente para el día "+cita.getFechacita().toString()+ " a las "+cita.getHoracita().toLocalTime().toString()+"h por el motivo"+cita.getMotivocita()+"<br>";
        }
        
                formatoHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "\n"
                + "<head>\n"
                + "    <meta charset=\"UTF-8\">\n"
                + "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n"
                + "    <meta name=\"x-apple-disable-message-reformatting\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta content=\"telephone=no\" name=\"format-detection\">\n"
                + "    <title></title>\n"
                + "    <!--[if (mso 16)]>\n"
                + "    <style type=\"text/css\">\n"
                + "    a {text-decoration: none;}\n"
                + "    </style>\n"
                + "    <![endif]-->\n"
                + "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n"
                + "    <!--[if !mso]><!-- -->\n"
                + "    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\">\n"
                + "    <!--<![endif]-->\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "    <div class=\"es-wrapper-color\">\n"
                + "        <!--[if gte mso 9]>\n"
                + "			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n"
                + "				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\n"
                + "			</v:background>\n"
                + "		<![endif]-->\n"
                + "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "            <tbody>\n"
                + "                <tr class=\"gmail-fix\" height=\"0\">\n"
                + "                    <td>\n"
                + "                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\">\n"
                + "                            <tbody>\n"
                + "                                <tr>\n"
                + "                                    <td cellpadding=\"0\" cellspacing=\"0\" border=\"0\" height=\"0\" style=\"line-height: 1px; min-width: 600px;\"><img src=\"https://esputnik.com/repository/applications/images/blank.gif\" width=\"600\" height=\"1\" style=\"display: block; max-height: 0px; min-height: 0px; min-width: 600px; width: 600px;\" alt></td>\n"
                + "                                </tr>\n"
                + "                            </tbody>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "                <tr>\n"
                + "                    <td class=\"esd-email-paddings\" valign=\"top\">\n"
                + "                        <table class=\"esd-footer-popover es-content es-visible-simple-html-only\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n"
                + "                            <tbody>\n"
                + "                                <tr>\n"
                + "                                    <td class=\"esd-stripe\" style=\"background-color: rgb(208, 224, 227); background-size: cover;\" bgcolor=\"#d0e0e3\" align=\"center\">\n"
                + "                                        <table class=\"es-content-body\" style=\"background-color: transparent;\" width=\"640\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"transparent\" align=\"center\">\n"
                + "                                            <tbody>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p10t es-p20r es-p20l\" align=\"left\" style=\"background-position: center center; background-color: transparent;\" bgcolor=\"transparent\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td class=\"esd-block-image es-p40t es-p25b\" align=\"center\"><a href target=\"_blank\"><img src=\"https://demo.stripocdn.email/content/guids/522f7257-d437-4530-9fe8-c47e1a4d9195/images/64311572727901569.png\" style=\"display: block;\" alt=\"Logo\" title=\"Logo\" width=\"335\"></a></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"center\" class=\"esd-block-text es-p25t es-p30b\">\n"
                + "                                                                                        <h1 style=\"color: rgb(51, 51, 51); text-align: center;\">\""+ titulo +" de cita\"</h1>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                                <tr>\n"
                + "                                                    <td class=\"esd-structure es-p20t es-p15b es-p20r es-p20l\" align=\"left\" style=\"background-position: left top; background-color: transparent;\" bgcolor=\"transparent\">\n"
                + "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                            <tbody>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"esd-container-frame\" width=\"600\" valign=\"top\" align=\"center\">\n"
                + "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                            <tbody>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td esdev-links-color=\"#b7bdc9\" class=\"esd-block-text es-p15t es-p20b\" align=\"left\">\n"
                + "                                                                                        <p style=\"color: rgb(51, 51, 51); text-align: center;\">"+ contenido +"</p>\n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </tbody>\n"
                + "                                                                        </table>\n"
                + "                                                                    </td>\n"
                + "                                                                </tr>\n"
                + "                                                            </tbody>\n"
                + "                                                        </table>\n"
                + "                                                    </td>\n"
                + "                                                </tr>\n"
                + "                                            </tbody>\n"
                + "                                        </table>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </tbody>\n"
                + "                        </table>\n"
                + "                    </td>\n"
                + "                </tr>\n"
                + "            </tbody>\n"
                + "        </table>\n"
                + "    </div>\n"
                + "</body>\n"
                + "\n"
                + "</html>";
    }

}
