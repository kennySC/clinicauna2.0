
package clinicauna.util;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.UsuarioDto;
import clinicauna.services.DetalleCitaService;
import com.jfoenix.controls.JFXButton;
import java.time.LocalDate;
import java.time.LocalTime;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/*
 * @author Kenneth Sibaja
 */
public class ContenedorCita extends AnchorPane {

    public Label paciente;
    public Label horaCita;
    public Label estado;
    public LocalTime horacitaLT;
    public DetallecitaDto cita;

    public JFXButton btnEstado;
    public JFXButton btnCancelar;
    public JFXButton btnAtender;
    public Boolean vacio;
    public int espacioCita;

    private double posX, posY;
    private LocalDate fecha;
    private double orgSceneX, orgSceneY;
    private double orgTranslateX, orgTranslateY;
    private double diferenciaX, diferenciaY;
    private double newTranslateX, newTranslateY;

    private String idioma = (String) AppContext.getInstance().get("Idioma");

    UsuarioDto usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");

    public ContenedorCita(DetallecitaDto cita) {    // Constructor para cargar la cita en ese espacio

        this.cita = cita;

        this.paciente = new Label(cita.idpaciente.idpers.getNombrepers() + " " + cita.idpaciente.idpers.getApellidospersona());
        this.paciente.setTextFill(Paint.valueOf("#ffcc00"));

        horacitaLT = LocalTime.of(cita.getHoracita().getHour(), cita.getHoracita().getMinute());
        this.horaCita = new Label(String.valueOf(horacitaLT));

        this.btnEstado = new JFXButton(idioma.equals("E") ? "Estado" : idioma.equals("I") ? "State" : "Estado");
        this.btnCancelar = new JFXButton(idioma.equals("E") ? "Cancelar" : idioma.equals("I") ? "Cancel" : "Cancelar");
        this.btnAtender = new JFXButton(idioma.equals("E") ? "Atender" : idioma.equals("I") ? "Attend" : "Atento");

        vacio = Boolean.FALSE;

        this.estado = new Label();

        fecha = (LocalDate) AppContext.getInstance().get("FechaCita");

        asignarEstado();
        iniciarEstilo();
        eventoAsignarCita();
        eventosBotones();

    }

    public ContenedorCita(int hora, int minuto, int espacio) {  // Construcctor usado para crear un contenedor vacio 

        fecha = (LocalDate) AppContext.getInstance().get("FechaCita");
        horacitaLT = LocalTime.of(hora, minuto);

        this.paciente = new Label(idioma.equals("E") ? "Espacio libre" : idioma.equals("I") ? "Free space" : "Espaço livre");
        this.paciente.setTextFill(Paint.valueOf("#ffffff"));

        this.horaCita = new Label(String.valueOf(horacitaLT));

        this.estado = new Label(idioma.equals("E") ? "No reservada" : idioma.equals("I") ? "Not assigned" : "Não atribuído");
        this.estado.setTextFill(Paint.valueOf("#ffffff"));

        vacio = Boolean.TRUE;

        this.cita = new DetallecitaDto();
        this.espacioCita = espacio;

        this.btnEstado = new JFXButton(idioma.equals("E") ? "Estado" : idioma.equals("I") ? "State" : "Estado");
        this.btnCancelar = new JFXButton(idioma.equals("E") ? "Cancelar" : idioma.equals("I") ? "Cancel" : "Cancelar");
        this.btnAtender = new JFXButton(idioma.equals("E") ? "Atender" : idioma.equals("I") ? "Attend" : "Atento");

        iniciarEstilo();
        eventoAsignarCita();

    }

    private void iniciarEstilo() {

        if (cita.getIddetcita() != null) {
            this.getStyleClass().add("estiloCitas");
        } else {
            this.getStyleClass().add("estiloCitasVacias");
        }

        this.setMinWidth(200);
        this.setMinHeight(100);
        this.setPrefWidth(200);
        this.setPrefHeight(100);
        this.setMaxWidth(200);
        this.setMaxHeight(100);

        horaCita.setLayoutX(10);
        horaCita.setLayoutY(8);
        horaCita.setTextFill(Paint.valueOf("#ffffff"));
        horaCita.getStyleClass().add("lbCitasEstilo");

        paciente.setLayoutX(10);
        paciente.setLayoutY(40);

        paciente.getStyleClass().add("lbCitasEstilo");

        estado.setLayoutX(10);
        estado.setLayoutY(70);
        estado.getStyleClass().add("lbCitasEstilo");

        btnEstado.setPrefWidth(70);
        btnEstado.setPrefHeight(15);
        btnEstado.setLayoutX(120);
        btnEstado.setLayoutY(8);
        btnEstado.getStyleClass().add("jfx-button");

        btnCancelar.setPrefWidth(70);
        btnCancelar.setPrefHeight(15);
        btnCancelar.setLayoutX(120);
        btnCancelar.setLayoutY(65);
        btnCancelar.getStyleClass().add("jfx-button");

        btnAtender.setPrefWidth(70);
        btnAtender.setPrefHeight(15);
        btnAtender.setLayoutX(120);
        btnAtender.setLayoutY(36.5);
        btnAtender.getStyleClass().add("jfx-button");

        this.getChildren().add(paciente);
        this.getChildren().add(horaCita);
        this.getChildren().add(estado);
        this.getChildren().add(btnEstado);
        this.getChildren().add(btnCancelar);
        this.getChildren().add(btnAtender);
        Tooltip tp = new Tooltip(idioma.equals("E") ? "Doble clic para asignar la cita"
                : idioma.equals("I") ? "Double click to set the appoinment" : "Clique duas vezes para atribuir o compromisso");
        Tooltip.install(this, tp);

    }

    private void asignarEstado() {
        this.estado.setText("");
        switch (cita.getEstadocita()) {
            case "Pr":  // Cita programada
                this.estado.setText(idioma.equals("E") ? "Asignada" : idioma.equals("I") ? "Booked" : "Agendada");
                this.estado.setTextFill(Paint.valueOf("#ffffff"));  // Black
                break;
            case "At":  // Cita atendida
                this.estado.setText(idioma.equals("E") ? "Atendida" : idioma.equals("I") ? "Attended" : "Assistiu");
                this.estado.setTextFill(Paint.valueOf("#009900"));  //  Green   
                break;
            case "Ca":  // Cita cancelada
                this.estado.setText(idioma.equals("E") ? "Cancelada" : idioma.equals("I") ? "Cancelled" : "Cancelado");
                this.estado.setTextFill(Paint.valueOf("#990033"));  //  Red
                break;
            case "Au":  // Citas ausente
                this.estado.setText(idioma.equals("E") ? "Ausente" : idioma.equals("I") ? "Absent" : "Ausente");
                this.estado.setTextFill(Paint.valueOf("#ffcc00"));  //  Yellow
                break;
        }
    }

    private void eventoAsignarCita() {

        this.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                if (fecha.isBefore(LocalDate.now())) {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Editar Cita", null, "No se le puede realizar cambios a una cita que ya paso");
                } else {
                    if (cita.getIddetcita() != null) {
                        AppContext.getInstance().set("CitaEditar", cita);
                    } else {
                        AppContext.getInstance().delete("CitaEditar");
                    }
                    AppContext.getInstance().set("HoraCita", horacitaLT);
                    AppContext.getInstance().set("EspacioCita", espacioCita);
                    FlowController.getInstance().goviewInWindowModal("GuardarCita", (Stage) this.getScene().getWindow(), Boolean.FALSE);
                    DetallecitaDto citaAux = (DetallecitaDto) AppContext.getInstance().get("Cita");
                    if (citaAux != null) {
                        cita = citaAux;
                        asignarEstado();
                        this.paciente.setText(cita.idpaciente.idpers.getNombrepers() + " "
                                + cita.idpaciente.idpers.getApellidospersona());
                        paciente.setTextFill(Paint.valueOf("#ffcc00"));
                        eventosBotones();
                        this.getStyleClass().clear();
                        this.getStyleClass().add("estiloCitas");
                    }
                }
            }
        });

        setOnDragDetected((event) -> {
            Dragboard db = this.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent clipB = new ClipboardContent();
            clipB.put(DataFormat.RTF, this);
            db.setContent(clipB);
            event.consume();
        });

        setOnDragOver(value -> {
            Dragboard db = value.getDragboard();
            if (db.hasContent(DataFormat.RTF)) {
                value.acceptTransferModes(TransferMode.MOVE);
            }
            value.consume();
        });

        setOnDragDropped(value -> {
            Dragboard db = value.getDragboard();
            if (db.hasContent(DataFormat.RTF)) {
                ContenedorCita cCita = (ContenedorCita) db.getContent(DataFormat.RTF);
                this.cita = cCita.getCita();
                asignarEstado();
                this.paciente.setText(cita.idpaciente.idpers.getNombrepers() + " "
                        + cita.idpaciente.idpers.getApellidospersona());
                paciente.setTextFill(Paint.valueOf("#ffcc00"));
                eventosBotones();
                this.getStyleClass().clear();
                this.getStyleClass().add("estiloCitas");
                value.setDropCompleted(true);
            }
            value.consume();
        });

    }

    private void eventosBotones() {
        if(cita.getEstadocita().equals("At")){
                btnAtender.setDisable(true);
                btnEstado.setDisable(true);
                btnCancelar.setDisable(true);
        }    
       
        this.btnEstado.setOnAction((event) -> {
            if (cita != null) {
                cambiarEstado();
            }
        });
        this.btnCancelar.setOnAction((event) -> {
            if (cita != null) {
                cancelarCita();
            }
        });
        this.btnAtender.setOnAction((event) -> {
            if (cita != null) {
                atenderCita();
            }
        });
    }

    public void cambiarEstado() {
        if (fecha.isBefore(LocalDate.now())) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Editar Cita":idioma.equals("I")?"Edit Appointment":"", null,
                    idioma.equals("E")?"No se le puede realizar cambios a una cita que esta en el pasado.":idioma.equals("I")?"Changes cannot be applied to an appointment in the past.":"Você não pode fazer alterações em um compromisso que está no passado.");
        } else {
            switch (cita.getEstadocita()) {
                case "Pr":
                    cita.setEstadocita("At");
                    asignarEstado();
                    break;
                case "At":
                    cita.setEstadocita("Au");
                    asignarEstado();
                    break;
                case "Ca":
                    cita.setEstadocita("Au");
                    asignarEstado();
                    break;
                case "Au":
                    cita.setEstadocita("Pr");
                    asignarEstado();
                    break;
            }
            Respuesta res = new DetalleCitaService().guardarDetCita(cita);
        }
    }

    public void nuevoContenedor() {
        this.paciente.setText(idioma.equals("E") ? "Espacio libre" : idioma.equals("I") ? "Free space" : "Espaço livre");
        this.horaCita.setText(String.valueOf(horacitaLT));
        this.estado.setText(idioma.equals("E") ? "No reservada" : idioma.equals("I") ? "Not assigned" : "Não atribuído");
        this.estado.setTextFill(Paint.valueOf("#ffffff"));
        this.cita = new DetallecitaDto();
    }

    public void cancelarCita() {
        if (fecha.isBefore(LocalDate.now())) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Editar Cita":idioma.equals("I")?"Edit Appointment":"", null,
                    idioma.equals("E")?"No se le puede realizar cambios a una cita que esta en el pasado.":idioma.equals("I")?"Changes cannot be applied to an appointment in the past.":"Você não pode fazer alterações em um compromisso que está no passado.");
        } else {
            if (Mensaje.showConfirmation(idioma.equals("E") ? "Cancelar Cita" : idioma.equals("I") ? "Cancel Appointment"
                    : "Cancelar compromisso", FlowController.getMainStage(), idioma.equals("E") ? "Desea cancelar la cita?" : idioma.equals("I") ? "Do you want to cancel the appointment?"
                    : "Você deseja cancelar o compromisso?")) {
                Respuesta res = new DetalleCitaService().eliminarCita(cita.getIddetcita());
                if (res.getEstado()) {
                    nuevoContenedor();
                    Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Cancelar Cita" : idioma.equals("I") ? "Cancel Appointment"
                            : "Cancelar compromisso", FlowController.getMainStage(), idioma.equals("E") ? "Cita cancelada" : idioma.equals("I") ? "Appointment calcelled"
                            : "Compromisso cancelado");
                } else {
                    Mensaje.showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Cancelar Cita" : idioma.equals("I") ? "Cancel Appointment"
                            : "Cancelar compromisso", FlowController.getMainStage(), idioma.equals("E") ? "Error al cancelar" : idioma.equals("I") ? "Error while cancelling"
                            : "Cancelar erro");
                }
            }
        }
    }

    public DetallecitaDto getCita() {
        return cita;
    }

    public void setCita(DetallecitaDto cita) {
        this.cita = cita;
    }

    public void atenderCita() {
        AppContext.getInstance().set("vistaAnterior", "AgendaCitas");
        AppContext.getInstance().set("citaAtender", this.cita);
        if (usuario.getTipous().equals("R")) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", btnAtender.getScene().getWindow(),
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");            
        } else {
            FlowController.getInstance().goviewInWindowModal("AtencionesPaciente", FlowController.getMainStage(), Boolean.TRUE);
            Boolean atendida = (Boolean) AppContext.getInstance().get("AtencionGuardada");
            if(atendida){
                btnAtender.setDisable(true);
                btnEstado.setDisable(true);
                btnCancelar.setDisable(true);
                cita.setEstadocita("At");
                Respuesta res = new DetalleCitaService().guardarDetCita(cita);
                asignarEstado();
            }
        }

    }

    public Boolean getVacio() {
        return vacio;
    }

    public void setVacio(Boolean vacio) {
        this.vacio = vacio;
    }

}
