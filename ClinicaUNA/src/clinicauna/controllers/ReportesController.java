
package clinicauna.controllers;

import clinicauna.model.RegistropacientesDto;
import clinicauna.services.ReportesService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class ReportesController extends Controller implements Initializable {

    @FXML
    private JFXButton btnVolver;
    @FXML
    private JFXButton btnReporte1;
    @FXML
    private JFXButton btnReporte2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        System.out.println("chompipe xddxxdxd");
    }
    
    @FXML
    private void VolverPantallaPrincipal(ActionEvent event) {
        FlowController.getInstance().goview("PantallaPrincipal");
    }

    @FXML
    private void reporte1(ActionEvent event) {
        JasperPrint report;
        FlowController.getInstance().goviewInWindowModal("FiltrarPacientes", getStage(), Boolean.FALSE);
        RegistropacientesDto paciente = (RegistropacientesDto) AppContext.getInstance().get("pacienteSelect");
        Respuesta res = new ReportesService().reporteExpediente(paciente.getIdpaciente());
        if(res.getEstado()){            
            report = (JasperPrint) res.getResultado("reporte");            
            JasperViewer rViwer = new JasperViewer(report);
            rViwer.setVisible(true);
            System.out.println("BIEN BEIN BEIN BEIN BEIN ");
        }else{
            System.out.println("MAL MAL MAL MAL MAL MAL MAL");
        }
    }

    @FXML
    private void reporte2(ActionEvent event) {
    }
    
}
