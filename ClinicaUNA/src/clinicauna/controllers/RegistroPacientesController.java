package clinicauna.controllers;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.RegistropacientesDto;
import clinicauna.services.DetalleCitaService;
import clinicauna.services.EncabezadoService;
import clinicauna.services.PacienteService;
import clinicauna.services.PersonaService;
import clinicauna.util.AppContext;
import clinicauna.util.BindingUtils;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DateCell;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class RegistroPacientesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNomPaciente;
    @FXML
    private JFXTextField txtApellidosPaciente;
    @FXML
    private JFXTextField txtCedPaciente;
    @FXML
    private JFXTextField txtCorreoPaciente;
    @FXML
    private JFXDatePicker dpFechaNacimiento;
    @FXML
    private JFXButton btnPacientesGuardar;
    @FXML
    private JFXButton btnPacientesVolver;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbMasculino;
    @FXML
    private ToggleGroup rbGeneroPaciente;
    @FXML
    private JFXRadioButton rbFemenino;
    @FXML
    private JFXButton btnPacientesEliminar;
    @FXML
    private ImageView ivFondoPacientes;
    @FXML
    private StackPane spParent;
    @FXML
    private VBox vBoxParent;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXTextField txtTelefPaciente;

    private PacienteService pacienteSer;

    private RegistropacientesDto paciente;
    private List<JFXTextField> necesarios;

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        rbFemenino.setUserData("F");
        rbMasculino.setUserData("M");
        necesarios = new ArrayList<>();
        necesarios.add(txtNomPaciente);
        necesarios.add(txtApellidosPaciente);
        necesarios.add(txtCedPaciente);
        necesarios.add(txtCorreoPaciente);
        necesarios.add(txtTelefPaciente);

        pacienteSer = new PacienteService();
        paciente = new RegistropacientesDto();

        dpFechaNacimiento.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) > 0);
            }
        });

        formatoDatos();
        
    }

    //OVERIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        nuevoPaciente();
    }

    @FXML
    private void guardarPaciente(ActionEvent event) {

        if (!comprobarTXTs()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Informacion Incompleta" : idioma.equals("I") ? "Incomplete Info" : "Informações incompleta", this.getStage(),
                    idioma.equals("E") ? "Porfavor revise la informacion, hay campos vacios." : idioma.equals("I") ? "Please check the information, there are empty spaces" : "Por favor, verifique as informações");
        } else {
            Respuesta res = pacienteSer.guardarPaciente(paciente);
            if (res.getEstado()) {
                paciente = (RegistropacientesDto) res.getResultado("Paciente");
                System.out.println(paciente.getIdpaciente());
                Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Exito" : idioma.equals("I") ? "Success" : "Êxito", getStage(),
                        idioma.equals("E") ? "Paciente guardado con exito." : idioma.equals("I") ? "Patient registered succesfully." : "Paciente salvo com sucesso.");
                nuevoPaciente();
                if (this.getStage().getModality().equals(Modality.WINDOW_MODAL)) {
                    AppContext.getInstance().set("Paciente", paciente);
                    this.getStage().close();
                }
            } else {

                new Mensaje().show(Alert.AlertType.ERROR, "Error", res.getMensaje());
            }
        }

    }

    // METODO PARA ABRIR LA PANTALLA DE BUSCAR/FILTRAR PACIENTES //
    @FXML
    private void BuscarPacientes(ActionEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarPacientes", this.getStage(), Boolean.FALSE);
        if (AppContext.getInstance().get("pacienteSelect") != null) {
            paciente = (RegistropacientesDto) AppContext.getInstance().get("pacienteSelect");
            paciente.setNuevo(Boolean.FALSE);
            bindPaciente();
        }
    }

    @FXML
    private void PacientesEliminar(ActionEvent event) {
        try {
            if (paciente.getIdpaciente() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Eliminar Paciente" : idioma.equals("I") ? "Patient Removal" : "Excluir paciente", getStage(),
                        idioma.equals("E") ? "Debe cargar al paciente que desea eliminar." : idioma.equals("I") ? "You must load the patient you wish to remove." : "Você deve cobrar o paciente que deseja remover.");
            } else {
                Respuesta resExp = new EncabezadoService().cargarExpedinte(paciente.getIdpaciente());
                Respuesta resCita = new DetalleCitaService().obtenerCitaPorPaciente(paciente.getIdpaciente());
                List<DetallecitaDto> citas =(List<DetallecitaDto>) resCita.getResultado("Citas");
                if (resExp.getResultado("Expediente")==null&&citas.isEmpty()) {

                    Respuesta res = new PersonaService().eliminarPersona(paciente.idpers.getIdpers());
                    if (!res.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Eliminar Paciente" : idioma.equals("I") ? "Patient Removal" : "Excluir paciente", getStage(),
                                idioma.equals("E") ? "Hubo un error al eliminar al paciente." : idioma.equals("I") ? "An error ocurred while trying to remove the patient." : "Ocorreu um erro ao excluir o paciente.");
                    } else {
                        nuevoPaciente();
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Eliminar Paciente" : idioma.equals("I") ? "Patient Removal" : "Excluir paciente", getStage(),
                                idioma.equals("E") ? "Paciente eliminado con exito." : idioma.equals("I") ? "Patient removed successfully.." : "Paciente removido com sucesso.");
                        
                    }
                }else{
                    new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Eliminar Paciente" : idioma.equals("I") ? "Patient Removal" : "Excluir paciente", getStage(),
                                idioma.equals("E") ? "Para poder eliminar el paciente es necesario eliminar sus registros." : idioma.equals("I") ? "In order to remove the patient it is necessary to delete their records" : "Para eliminar o paciente, é necessário excluir seus registros");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroPacientesController.class.getName()).log(Level.SEVERE, "Error eliminando el paciente.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Eliminar Paciente" : idioma.equals("I") ? "Patient Removal" : "Excluir paciente", getStage(),
                    idioma.equals("E") ? "Hubo un error al eliminar al paciente." : idioma.equals("I") ? "An error ocurred while trying to remove the patient." : "Ocorreu um erro ao excluir o paciente.");
        }
    }

    public void nuevoPaciente() {
        unbindPaciente();
        paciente = new RegistropacientesDto();
        bindPaciente();
    }

    public void bindPaciente() {

        txtNomPaciente.textProperty().bindBidirectional(paciente.idpers.nombrepers);
        txtApellidosPaciente.textProperty().bindBidirectional(paciente.idpers.apellidospersona);
        txtCorreoPaciente.textProperty().bindBidirectional(paciente.idpers.correopers);
        txtCedPaciente.textProperty().bindBidirectional(paciente.idpers.cedulapers);
        txtTelefPaciente.textProperty().bindBidirectional(paciente.telefonoPaciente);
        dpFechaNacimiento.valueProperty().bindBidirectional(paciente.fechanacimientop);
        BindingUtils.bindToggleGroupToProperty(rbGeneroPaciente, paciente.generop);

    }

    public void unbindPaciente() {

        txtNomPaciente.textProperty().unbindBidirectional(paciente.idpers.nombrepers);
        txtApellidosPaciente.textProperty().unbindBidirectional(paciente.idpers.apellidospersona);
        txtCorreoPaciente.textProperty().unbindBidirectional(paciente.idpers.correopers);
        txtCedPaciente.textProperty().unbindBidirectional(paciente.idpers.cedulapers);
        txtTelefPaciente.textProperty().unbindBidirectional(paciente.telefonoPaciente);
        dpFechaNacimiento.valueProperty().unbindBidirectional(paciente.fechanacimientop);
        BindingUtils.unbindToggleGroupToProperty(rbGeneroPaciente, paciente.generop);

    }

    public Boolean comprobarTXTs() {

        Boolean faltan = Boolean.TRUE;
        if (!(rbGeneroPaciente.getSelectedToggle().isSelected())) {   // ARREGLAR POR QUE NO LO ESTA VERIFICANDO //
            faltan = Boolean.FALSE;
        } else {
            if (dpFechaNacimiento.getValue() == null) {
                faltan = Boolean.FALSE;
            } else {
                for (JFXTextField t : necesarios) {
                    if (t.getText() == null || t.getText().isEmpty()) {
                        faltan = Boolean.FALSE;
                    }
                }
            }
        }
        return faltan;
    }

    @FXML
    private void PacientesVolverPantPrincipal(ActionEvent event) {
        if (this.getStage().getModality().equals(Modality.WINDOW_MODAL)) {
            this.getStage().close();
        } else {
            if (Mensaje.showConfirmation(idioma.equals("E") ? "Salir" : idioma.equals("I") ? "Exit" : "Sair", getStage(),
                    idioma.equals("E") ? "¿Esta seguro que desea volver al menu principal?" : idioma.equals("I") ? "Are you sure you want to exit the main menu?" : "Tem certeza de que deseja retornar ao menu principal?")) {
                FlowController.getInstance().goview("PantallaPrincipal");
            }
        }

    }

    // METODO PARA LIMPIAR LO QUE HAY EN PANTALLA //
    @FXML
    private void LimpiarPaciente(ActionEvent event) {
        nuevoPaciente();
    }

    public void formatoDatos(){
        txtNomPaciente.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtApellidosPaciente.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtCedPaciente.setTextFormatter(Formato.getInstance().cedulaFormat(12));
        txtCorreoPaciente.setTextFormatter(Formato.getInstance().cedulaFormat(100));
        txtTelefPaciente.setTextFormatter(Formato.getInstance().maxLengthFormat(20));
    }
    
}
