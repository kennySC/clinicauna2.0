
package clinicauna.controllers;

import clinicauna.model.RegistropacientesDto;
import clinicauna.services.PacienteService;
import clinicauna.util.AppContext;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class FiltrarPacientesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtFiltrarNombre;
    @FXML
    private JFXTextField txtFiltrarCedula;
    @FXML
    private JFXTextField txtFiltrarApellidos;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private TableView<RegistropacientesDto> tvFiltroPacientes;
    @FXML
    private ImageView ivFondoFiltrar;   
    @FXML
    private TableColumn<RegistropacientesDto, String> tcNombre;
    @FXML
    private TableColumn<RegistropacientesDto, String> tcApellidos;
    @FXML
    private TableColumn<RegistropacientesDto, String> tcCedula;
    @FXML
    private TableColumn<RegistropacientesDto, String> tcGenero;
    
    private List<RegistropacientesDto> listPacientes;
    
    String idioma = (String) AppContext.getInstance().get("Idioma");
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listPacientes = new ArrayList();
        tcNombre.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getNombrepers()));
        tcApellidos.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getApellidospersona()));
        tcCedula.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getCedulapers()));
        tcGenero.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getGenerop()));
        
        formatoTextos();
    }    
    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {    
        filtro();
    }
            
    @FXML
    private void AceptarFiltrarPacientes(ActionEvent event) {
        
        if(tvFiltroPacientes.getSelectionModel().getSelectedItem() != null){
            AppContext.getInstance().set("pacienteSelect", tvFiltroPacientes.getSelectionModel().getSelectedItem());
            this.getStage().close();
        }else{
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), idioma.equals("E")?"No hay ningun paciente selecionado. \nPor favor seleccione uno." :
                    idioma.equals("I")?"There must be a patient selected, please select one." : "Deve haver um paciente selecionado, por favor selecione um");
        }
    }

    @FXML
    private void CancelarFiltrarPacientes(ActionEvent event) {
        ((Stage) btnCancelar.getScene().getWindow()).close();
    }   

    @FXML
    private void Filtrar(KeyEvent event) {
        filtro();
    }        
    
    public void filtro(){
        String ced = "%", nom = "%", ape = "%";                       
        
        tvFiltroPacientes.getItems().clear();
               
        if(!txtFiltrarCedula.getText().isEmpty()){
            ced = txtFiltrarCedula.getText();
        }        
        if(!txtFiltrarNombre.getText().isEmpty()){
            nom = txtFiltrarNombre.getText();
        }        
        if(!txtFiltrarApellidos.getText().isEmpty()){
            ape = txtFiltrarApellidos.getText();
        }                        

        Respuesta res = new PacienteService().getPacientes("%" + ced + "%", "%" + nom + "%" , "%" + ape + "%");       
        
        listPacientes = (List<RegistropacientesDto>) res.getResultado("Pacientes");
        if(listPacientes != null){
            tvFiltroPacientes.getItems().addAll(FXCollections.observableArrayList(listPacientes));
        }
    }
    
    public void formatoTextos(){
        txtFiltrarNombre.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarApellidos.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarCedula.setTextFormatter(Formato.getInstance().cedulaFormat(12));
    }
    
}
