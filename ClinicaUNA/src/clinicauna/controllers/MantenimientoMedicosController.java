package clinicauna.controllers;

import clinicauna.model.MedicoDto;
import clinicauna.services.MedicoService;
import clinicauna.services.PersonaService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author Kenneth Sibaja
 */
public class MantenimientoMedicosController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNomMed;
    @FXML
    private JFXTextField txtApellidosMed;
    @FXML
    private JFXTextField txtCedMed;
    @FXML
    private JFXTextField txtCorreoMed;
    @FXML
    private JFXTextField txtUsuarioMed;
    @FXML
    private JFXPasswordField txtPasswordMed;
    @FXML
    private JFXTextField txtCarneMed;
    @FXML
    private JFXComboBox cmbEspaciosHoraMed;
    @FXML
    private JFXTimePicker tpIniJornadaMed;
    @FXML
    private JFXTimePicker tpFinJornadaMed;
    @FXML
    private JFXRadioButton rbActivoMed;
    @FXML
    private ToggleGroup tbEstadoMed;
    @FXML
    private JFXRadioButton rbInactivoMed;
    @FXML
    private JFXComboBox cmbIdiomaUsMed;
    @FXML
    private JFXButton btnMedGuardar;
    @FXML
    private JFXButton btnBuscarMed;
    @FXML
    private JFXButton btnMantMedEliminar;
    @FXML
    private JFXButton btnMantMedVolver;
    @FXML
    private ImageView ivFondoMedicos;
    @FXML
    private JFXTextField txtCodMed;
    @FXML
    private JFXTextField txtFolioMed;
    @FXML
    private StackPane spMantMedicos;
    @FXML
    private JFXButton btnNuevoMed;

    private MedicoDto medDto;
    private MedicoService medserv;
    private List<JFXTextField> necesarios = new ArrayList<>();

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cmbEspaciosHoraMed.getItems().add("1");
        cmbEspaciosHoraMed.getItems().add("2");
        cmbEspaciosHoraMed.getItems().add("3");
        cmbEspaciosHoraMed.getItems().add("4");

        cmbIdiomaUsMed.getItems().add("Español");
        cmbIdiomaUsMed.getItems().add("Ingles");
        cmbIdiomaUsMed.getItems().add("Portugues");

        //  ToggleGroup medico
        rbActivoMed.setUserData("A");
        rbInactivoMed.setUserData("I");

        tpIniJornadaMed._24HourViewProperty().set(Boolean.TRUE);
        tpFinJornadaMed._24HourViewProperty().set(Boolean.TRUE);

        medDto = new MedicoDto();
        medserv = new MedicoService();

        necesarios.add(txtNomMed);
        necesarios.add(txtApellidosMed);
        necesarios.add(txtCorreoMed);
        necesarios.add(txtCedMed);
        necesarios.add(txtUsuarioMed);
        necesarios.add(txtCodMed);
        necesarios.add(txtFolioMed);
        necesarios.add(txtNomMed);

        formato();

    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        nuevoMedico();
    }

    @FXML
    private void GuardarMedico(ActionEvent event) {

        if (!comprobarEspacios()) {
            new Mensaje().show(Alert.AlertType.WARNING, idioma.equals("E") ? "Informacion Incompleta" : idioma.equals("I") ? "Incomplete Information" : "Informações Incompletas",
                    idioma.equals("E") ? "Algunos espacios estan vacios o sin marcar\nPorfavor reviselos." : idioma.equals("I") ? "Whoops, looks like there's missing info.\nPlease check that everythings complete." : "Alguns espaços estão vazios ou sem marcas.\nPor favor, verifique-os.");
        } else {
            getHorasIngresoSalida();
            medDto.setEstadomed("I");
            medDto.idus.setEstadous("A");
            medDto.idus.setTipous("M");
            medDto.idus.setIdiomaus(getIdioma());
            Respuesta res = medserv.guardarMedico(medDto);
            if (res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Exito" : idioma.equals("I")?"Success": "Êxito", getStage(), 
                        idioma.equals("E")?"Medico guardado con exito.": idioma.equals("I")?"Doctor registered succesfully.":"Médico salvo com sucesso.");
                nuevoMedico();
            } else {
                new Mensaje().show(Alert.AlertType.ERROR, "Error", idioma.equals("E")?"Hubo un error al guardar al medico.":idioma.equals("I")?"An error ocurred while registering the doctor."
                        :"Houve um erro ao salvar o médico.");
            }
        }

    }

    @FXML
    private void BuscarMedico(ActionEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarMedicos", this.getStage(), false);
        if (AppContext.getInstance().get("medicoSelect") != null) {
            nuevoMedico();
            medDto = (MedicoDto) AppContext.getInstance().get("medicoSelect");
            bindMedico();
            cmbIdiomaUsMed.getSelectionModel().clearSelection();
            for (Object pro : cmbIdiomaUsMed.getItems()) {
                if (pro.toString().equals("Portugues") && medDto.getIdus().getIdiomaus().equals("P")) {
                    cmbIdiomaUsMed.getSelectionModel().select(pro);

                }
                if (pro.toString().equals("Ingles") && medDto.getIdus().getIdiomaus().equals("I")) {
                    cmbIdiomaUsMed.getSelectionModel().select(pro);

                }
                if (pro.toString().equals("Español") && medDto.getIdus().getIdiomaus().equals("E")) {
                    cmbIdiomaUsMed.getSelectionModel().select(pro);

                }
            }
            tpFinJornadaMed.setValue(medDto.getFinjornadamed().toLocalTime());
            tpIniJornadaMed.setValue(medDto.getInijornadamed().toLocalTime());
        }
    }

    @FXML
    private void EliminarMedico(ActionEvent event) {
        try {
            if (medDto.getIdmed() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Medico":idioma.equals("I")?"Doctor Removal":"Excluir médico", getStage(),
                       idioma.equals("E")?"Debe cargar al medico que desea eliminar.":idioma.equals("I")?"You must load the doctor you wish to remove.":"Você deve cobrar o médico que deseja remover.");
            } else {
                Respuesta res = new PersonaService().eliminarPersona(medDto.idPers.getIdpers());
                if (!res.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Medico":idioma.equals("I")?"Doctor Removal":"Excluir médico", getStage(), 
                            idioma.equals("E")?"Hubo un error al eliminar al medico.":idioma.equals("I")?"An error ocurred while trying to remove the doctor.":"Ocorreu um erro ao excluir o médico.");
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Eliminar Medico":idioma.equals("I")?"Doctor Removal":"Excluir médico", getStage(), 
                            idioma.equals("E")?"Medico eliminado con exito.":idioma.equals("I")?"Doctor removed successfully.":"Médico eliminado com sucesso");
                    nuevoMedico();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroPacientesController.class.getName()).log(Level.SEVERE, "Error eliminando al medico.", e);
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Medico":idioma.equals("I")?"Doctor Removal":"Excluir médico", getStage(), 
                            idioma.equals("E")?"Ocurrior un error al eliminar al medico.":idioma.equals("I")?"An error ocurred while trying to remove the doctor.":"Ocorreu um erro ao excluir o médico.");
        }
    }

    public Boolean comprobarEspacios() {

        Boolean faltan = Boolean.TRUE;
        if (!tbEstadoMed.getSelectedToggle().isSelected()) {
            faltan = Boolean.FALSE;
        } else {
            if (tpFinJornadaMed.getValue() == null || tpIniJornadaMed.getValue() == null) {
                faltan = Boolean.FALSE;
            } else {
                if (txtPasswordMed.getText().isEmpty() || txtPasswordMed.getText() == null) {
                    faltan = Boolean.FALSE;
                }
                for (JFXTextField t : necesarios) {
                    if (t.getText() == null || t.getText().isEmpty()) {
                        faltan = Boolean.FALSE;
                    }
                }
            }
        }
        return faltan;
    }

    void bindMedico() {
        txtNomMed.textProperty().bindBidirectional(medDto.idPers.nombrepers);
        txtApellidosMed.textProperty().bindBidirectional(medDto.idPers.apellidospersona);
        txtCedMed.textProperty().bindBidirectional(medDto.idPers.cedulapers);
        txtCorreoMed.textProperty().bindBidirectional(medDto.idPers.correopers);
        txtCarneMed.textProperty().bindBidirectional(medDto.carnemed);
        txtCodMed.textProperty().bindBidirectional(medDto.codigomed);
        txtFolioMed.textProperty().bindBidirectional(medDto.foliomed);
        txtUsuarioMed.textProperty().bindBidirectional(medDto.idus.usernameus);
        txtPasswordMed.textProperty().bindBidirectional(medDto.idus.passwordus);
        cmbEspaciosHoraMed.valueProperty().bindBidirectional(medDto.espaciosmed);
//        cmbIdiomaUsMed.valueProperty().bindBidirectional(medDto.idus.idiomaus);        
    }

    void unbindMedico() {
        txtNomMed.textProperty().unbindBidirectional(medDto.idPers.nombrepers);
        txtApellidosMed.textProperty().unbindBidirectional(medDto.idPers.apellidospersona);
        txtCedMed.textProperty().unbindBidirectional(medDto.idPers.cedulapers);
        txtCorreoMed.textProperty().unbindBidirectional(medDto.idPers.correopers);
        txtCarneMed.textProperty().unbindBidirectional(medDto.carnemed);
        txtCodMed.textProperty().unbindBidirectional(medDto.codigomed);
        txtFolioMed.textProperty().unbindBidirectional(medDto.foliomed);
        txtUsuarioMed.textProperty().unbindBidirectional(medDto.idus.usernameus);
        txtPasswordMed.textProperty().unbindBidirectional(medDto.idus.passwordus);
        cmbEspaciosHoraMed.valueProperty().unbindBidirectional(medDto.espaciosmed);
//        cmbIdiomaUsMed.valueProperty().unbindBidirectional(medDto.idus.idiomaus);
    }

    public String getIdioma() {
        String s = (String) cmbIdiomaUsMed.getSelectionModel().getSelectedItem();
        switch (s) {
            case "Español":
                return "E";
            case "Ingles":
                return "I";
            case "Portuguese":
                return "P";
        }
        return "N";
    }

    public void nuevoMedico() {
        unbindMedico();
        medDto = new MedicoDto();
        medserv = new MedicoService();
        cmbIdiomaUsMed.getSelectionModel().clearSelection();
        cmbEspaciosHoraMed.getSelectionModel().clearSelection();
        tpFinJornadaMed.setValue(null);
        tpIniJornadaMed.setValue(null);
        bindMedico();
    }

    private void getHorasIngresoSalida() {
        medDto.setInijornadamed(LocalDateTime.of(LocalDate.now(), tpIniJornadaMed.getValue()));
        medDto.setFinjornadamed(LocalDateTime.of(LocalDate.now(), tpFinJornadaMed.getValue()));
    }

    public void formato() {
        txtNomMed.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtApellidosMed.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtCedMed.setTextFormatter(Formato.getInstance().cedulaFormat(12));
        txtCorreoMed.setTextFormatter(Formato.getInstance().maxLengthFormat(100));
        txtUsuarioMed.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
        txtPasswordMed.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
        txtCodMed.setTextFormatter(Formato.getInstance().integerFormat());
        txtCarneMed.setTextFormatter(Formato.getInstance().integerFormat());
        txtFolioMed.setTextFormatter(Formato.getInstance().integerFormat());
        
    }

    @FXML
    private void VolverPantallaPrincipal(ActionEvent event) {
        if (Mensaje.showConfirmation(idioma.equals("E") ? "Salir" : idioma.equals("I") ? "Exit" : "Sair", getStage(),
                idioma.equals("E") ? "¿Esta seguro que desea volver al menu principal?" : idioma.equals("I") ? "Are you sure you want to exit the main menu?" : "Tem certeza de que deseja retornar ao menu principal?")) {
            FlowController.getInstance().goview("PantallaPrincipal");
        }
    }

    @FXML
    private void Nuevo(ActionEvent event) {
        nuevoMedico();
    }

}
