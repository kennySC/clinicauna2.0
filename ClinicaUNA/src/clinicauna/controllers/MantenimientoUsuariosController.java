package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.services.PersonaService;
import clinicauna.services.UsuariosService;
import clinicauna.util.AppContext;
import clinicauna.util.BindingUtils;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class MantenimientoUsuariosController extends Controller implements Initializable {

    // CON RESPECTO A LA PANTALLA DE USUARIOS //
    @FXML
    private StackPane spMantenimento;
    @FXML
    private JFXTextField txtNomUsuario;
    @FXML
    private JFXTextField txtApellidosUsuario;
    @FXML
    private JFXTextField txtCedUsuario;
    @FXML
    private JFXTextField txtCorreoUsuario;
    @FXML
    private JFXTextField txtUsername;
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private JFXButton btnMantGuardar;
    @FXML
    private JFXButton btnMantVolver1;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXComboBox cmbIdiomaUs;
    @FXML
    private JFXRadioButton rbAdmin;
    @FXML
    private JFXRadioButton rbRecep;
    @FXML
    private ToggleGroup tipoUsuario;
    @FXML
    private JFXButton btnMantEliminar;
    @FXML
    private ImageView ivFondoUsuarios;

    private UsuarioDto usuario;

    private UsuariosService usService;

    private List<JFXTextField> necesarios = new ArrayList();

    String idioma = (String) AppContext.getInstance().get("Idioma");
    
    @FXML
    private JFXButton btnLimpiar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = new UsuarioDto();
        usService = new UsuariosService();

        cmbIdiomaUs.getItems().clear();
        cmbIdiomaUs.getItems().add("Español");
        cmbIdiomaUs.getItems().add("Ingles");
        cmbIdiomaUs.getItems().add("Portugues");

        rbAdmin.setUserData("A");
        rbRecep.setUserData("R");

        // PARA SETEAR EL FORMATO DE LOS CAMPOS DE TEXTO //
        Formato();              
    }

    // OVERIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {       
        nuevoUsuario(); 
    }

    @FXML
    private void GuardarUsuario(ActionEvent event) {
        indicarRequeridos();
        if (!comprobarEspacios()) {
            new Mensaje().show(Alert.AlertType.WARNING, idioma.equals("E") ? "Informacion Incompleta" : idioma.equals("I") ? "Incomplete Information" : "Informações Incompletas",
                    idioma.equals("E") ? "Algunos espacios estan vacios o sin marcar\nPorfavor reviselos." : idioma.equals("I") ? "Whoops, looks like there's missing info.\nPlease check that everythings complete." : "Alguns espaços estão vazios ou sem marcas.\nPor favor, verifique-os.");
        } else {
            usuario.setIdiomaus(getIdioma());
            usuario.setEstadous("A");
            Respuesta res = usService.guardarUsuario(usuario);
            if (res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Exito" : idioma.equals("I")?"Success": "Êxito", getStage(), 
                        idioma.equals("E")?"Usuario guardado con exito.": idioma.equals("I")?"User registered succesfully.":"Usuário salvo com sucesso.");
                usuario = (UsuarioDto) res.getResultado("Usuario");
                String correo = usuario.idpers.getCorreopers();
                ControllerCorreo cCorreo = new ControllerCorreo();
                Platform.runLater(
                        () -> {
                            cCorreo.getCorreo().formatoActivacion("Activacion Usuario", usuario.getIdus());
                            Respuesta resCorreo = cCorreo.enviarEmailHTML(correo);
                        }
                );  
                nuevoUsuario();
            } else {
                new Mensaje().show(Alert.AlertType.ERROR, "Error", idioma.equals("E")?"Hubo un error al guardar al usuario.":idioma.equals("I")?"An error ocurred while registering the user."
                        :"Houve um erro ao salvar o usuário.");
            }
        }
    }

    // METODO DE BUSQUEDA/OBTENER LISTA DE USUARIOS ("FILTRAR")
    @FXML
    private void BuscarUsuario(ActionEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarUsuarios", this.getStage(), false);

        if (AppContext.getInstance().get("usuarioSelect") != null) {
            usuario = (UsuarioDto) AppContext.getInstance().get("usuarioSelect");
            bindUsuario();
            cmbIdiomaUs.getSelectionModel().clearSelection();
            for (Object pro : cmbIdiomaUs.getItems()) {
                System.out.println(usuario.getIdiomaus());
                if (pro.toString().equals("Portugues") && usuario.getIdiomaus().equals("P")) {
                    cmbIdiomaUs.getSelectionModel().select(pro);
                }
                if (pro.toString().equals("Ingles") && usuario.getIdiomaus().equals("I")) {
                    cmbIdiomaUs.getSelectionModel().select(pro);
                }
                if (pro.toString().equals("Español") && usuario.getIdiomaus().equals("E")) {
                    cmbIdiomaUs.getSelectionModel().select(pro);
                }
            }
        }
    }

    @FXML
    private void EliminarUsuario(ActionEvent event) {
        try {
            if (usuario.getIdus() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Usuario":idioma.equals("I")?"User Removal":"Excluir usuário", getStage(),
                       idioma.equals("E")?"Debe cargar al medico que desea eliminar.":idioma.equals("I")?"You must load the user you wish to remove.":"Você deve cobrar o usuário que deseja remover.");
            } else {
                Respuesta res = new PersonaService().eliminarPersona(usuario.idpers.getIdpers());
                if (!res.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Usuario":idioma.equals("I")?"User Removal":"Excluir usuário", getStage(), 
                            idioma.equals("E")?"Hubo un error al eliminar al usuario.":idioma.equals("I")?"An error ocurred while trying to remove the user.":"Ocorreu um erro ao excluir o usuário.");
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Eliminar Usuario":idioma.equals("I")?"User Removal":"Excluir usuário", getStage(), 
                            idioma.equals("E")?"Usuario eliminado con exito.":idioma.equals("I")?"User removed successfully.":"Usuário eliminado com sucesso");
                    nuevoUsuario();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MantenimientoUsuariosController.class.getName()).log(Level.SEVERE, "Error eliminando el usuario.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Eliminar Usuario":idioma.equals("I")?"User Removal":"Excluir usuário", getStage(), 
                            idioma.equals("E")?"Hubo un error al eliminar al usuario.":idioma.equals("I")?"An error ocurred while trying to remove the user.":"Ocorreu um erro ao excluir o usuário.");
        }
    }

    public void nuevoUsuario() {
        unbindUsuario();
        usuario = new UsuarioDto();
        usService = new UsuariosService();
        cmbIdiomaUs.getSelectionModel().clearSelection();
        //tipoUsuario.selectToggle(null);
        bindUsuario();
    }

    void bindUsuario() {
        txtNomUsuario.textProperty().bindBidirectional(usuario.idpers.nombrepers);
        txtApellidosUsuario.textProperty().bindBidirectional(usuario.idpers.apellidospersona);
        txtCedUsuario.textProperty().bindBidirectional(usuario.idpers.cedulapers);
        txtCorreoUsuario.textProperty().bindBidirectional(usuario.idpers.correopers);
        txtUsername.textProperty().bindBidirectional(usuario.usernameus);
        txtPassword.textProperty().bindBidirectional(usuario.passwordus);
        BindingUtils.bindToggleGroupToProperty(tipoUsuario, usuario.tipous);
    }

    void unbindUsuario() {
        txtNomUsuario.textProperty().unbindBidirectional(usuario.idpers.nombrepers);
        txtApellidosUsuario.textProperty().unbindBidirectional(usuario.idpers.apellidospersona);
        txtCedUsuario.textProperty().unbindBidirectional(usuario.idpers.cedulapers);
        txtCorreoUsuario.textProperty().unbindBidirectional(usuario.idpers.correopers);
        txtUsername.textProperty().unbindBidirectional(usuario.usernameus);
        txtPassword.textProperty().unbindBidirectional(usuario.passwordus);
        BindingUtils.bindToggleGroupToProperty(tipoUsuario, usuario.tipous);
    }

    public String getIdioma() {
        String s = (String) cmbIdiomaUs.getSelectionModel().getSelectedItem();
        switch (s) {
            case "Español":
                return "E";
            case "Ingles":
                return "I";
            case "Portuguese":
                return "P";
        }
        return "N";
    }

    public void Formato() {
        txtNomUsuario.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtApellidosUsuario.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtCedUsuario.setTextFormatter(Formato.getInstance().cedulaFormat(12));
        txtCorreoUsuario.setTextFormatter(Formato.getInstance().maxLengthFormat(100));
        txtUsername.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
        txtPassword.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
    }

    public void indicarRequeridos() {
        necesarios.clear();
        necesarios.addAll(Arrays.asList(txtUsername, txtUsername, txtApellidosUsuario, txtCedUsuario, txtCorreoUsuario, txtNomUsuario));
    }

    public Boolean comprobarEspacios() {

        Boolean faltan = Boolean.TRUE;
        if (tipoUsuario.getSelectedToggle() == null) {
            faltan = Boolean.FALSE;
        } else if (/*cmbIdiomaUs.getSelectionModel().isEmpty() && */cmbIdiomaUs.getSelectionModel() == null) {
            faltan = Boolean.FALSE;
        } else if (txtPassword.getText() == null) {
            faltan = Boolean.FALSE;
        }
        for (JFXTextField t : necesarios) {
            if (t.getText() == null || t.getText().isEmpty()) {
                faltan = Boolean.FALSE;
            }
        }
        return faltan;
    }

    @FXML
    private void MantUsVolverPantPrincipal(ActionEvent event) {
        if (Mensaje.showConfirmation(idioma.equals("E") ? "Salir" : idioma.equals("I") ? "Exit" : "Sair", getStage(),
                idioma.equals("E") ? "¿Esta seguro que desea volver al menu principal?" : idioma.equals("I") ? "Are you sure you want to exit the main menu?" : "Tem certeza de que deseja retornar ao menu principal?")) {
            FlowController.getInstance().goview("PantallaPrincipal");
        }
    }

    // METODO PARA LIMPIAR LO QUE HAYA EN PANTALLA //
    @FXML
    private void LimpiarUsuario(ActionEvent event) {
        nuevoUsuario();
    }

}
