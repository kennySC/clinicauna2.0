package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.services.UsuariosService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class FiltrarUsuariosController extends Controller implements Initializable {

    @FXML
    private TableView<UsuarioDto> tvMantUsuarios;
    @FXML
    private JFXTextField txtFiltrarNombre;
    @FXML
    private JFXTextField txtFiltrarApellidos;
    @FXML
    private JFXTextField txtFiltrarCedula;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private ImageView ivFondoFiltrar;
    @FXML
    private TableColumn<UsuarioDto, String> C_Nombre;
    @FXML
    private TableColumn<UsuarioDto, String> C_Apellidos;
    @FXML
    private TableColumn<UsuarioDto, String> C_Cedula;
    @FXML
    private TableColumn<UsuarioDto, String> C_TipoUS;
    @FXML
    private TableColumn<UsuarioDto, String> C_NomUsuario;
    private List<UsuarioDto> listUsuarios;

    String idioma = (String) AppContext.getInstance().get("Idioma");
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
C_Nombre.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getNombrepers()));
        C_Apellidos.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getApellidospersona()));
        C_Cedula.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idpers.getCedulapers()));
        C_NomUsuario.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getUsernameus()));
        C_TipoUS.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getTipous()));
        
        formatoTextos();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        
        AppContext.getInstance().set("usuarioSelect", new UsuarioDto());
        Filtro();

    }

    // METODO PARA ACEPTAR AL USUARIO QUE SE ESTE BUSCANDO //
    @FXML
    private void AceptarFiltrar(ActionEvent event) {
        if (tvMantUsuarios.getSelectionModel().getSelectedItem() != null) {
            AppContext.getInstance().set("usuarioSelect", tvMantUsuarios.getSelectionModel().getSelectedItem());
            this.getStage().close();
        } else{
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), idioma.equals("E")?"No hay ningun usuario selecionado. \nPor favor seleccione uno." :
                    idioma.equals("I")?"There must be a user selected, please select one." : "Deve haver um usuário selecionado, por favor selecione um");
        }

    }

    // METODO PARA ACEPTAR AL USUARIO QUE SE ESTE BUSCANDO //
    @FXML
    private void CancelarFiltrar(ActionEvent event) {
        ((Stage) btnCancelar.getScene().getWindow()).close();
    }

    @FXML
    private void FIltrar(KeyEvent event) {
        Filtro();
    }

    void Filtro() {
        String ced = "%", nom = "%", ape = "%";

        tvMantUsuarios.getItems().clear();

        if (!txtFiltrarCedula.getText().isEmpty()) {
            ced = txtFiltrarCedula.getText();
        }
        if (!txtFiltrarNombre.getText().isEmpty()) {
            nom = txtFiltrarNombre.getText();
        }
        if (!txtFiltrarApellidos.getText().isEmpty()) {
            ape = txtFiltrarApellidos.getText();
        }

        Respuesta res = new UsuariosService().getUsuarios("%" + ced + "%", "%" + nom + "%", "%" + ape + "%");

        listUsuarios = (List<UsuarioDto>) res.getResultado("Usuarios");
        listUsuarios.stream().forEach(e -> System.out.println(e.getIdpers().getIdpers()));
        if (listUsuarios != null) {
            tvMantUsuarios.getItems().addAll(FXCollections.observableArrayList(listUsuarios));
        }
    }
    
    public void formatoTextos(){
        txtFiltrarNombre.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarApellidos.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarCedula.setTextFormatter(Formato.getInstance().cedulaFormat(12));
    }
}
