package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class PantallaPrincipalController extends Controller implements Initializable {

    @FXML
    private JFXButton btnCerrarSesion;
    @FXML
    private StackPane spPantPrincipal;
    @FXML
    private ImageView ivfondo;
    @FXML
    private HBox hboxBottom;
    @FXML
    private HBox hboxCenter;
    @FXML
    private AnchorPane apRoot;
    @FXML
    private VBox vBoxPadre;

    String idioma = (String) AppContext.getInstance().get("Idioma");
    UsuarioDto usuario;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
//        OpcionesPantallaPrincipal();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");   
        AppContext.getInstance().delete("vistaAnterior");
    }

    // METODO PARA EL BOTON DE CERRAR SESION //
    @FXML
    private void CerrarSesionUsuario(ActionEvent event) {
        if (Mensaje.showConfirmation(idioma.equals("E") ? "Salir" : idioma.equals("I") ? "Exit" : "Sair", getStage(),
                idioma.equals("E") ? "¿Esta seguro que desea volver a la pantalla del log in?" : idioma.equals("I") ? "Are you sure you want to exit the log in page?" : "Tem certeza de que deseja retornar à tela de login?")) {
            FlowController.getInstance().cerrar(this.getStage()); 
            FlowController.getInstance().initialize();
            FlowController.getInstance().goviewInWindowModal("LogIn", this.getStage(), false);
        }
    }


}
