package clinicauna.controllers;

import clinicauna.ClinicaUNA;
import clinicauna.model.UsuarioDto;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class VistaBaseController extends Controller implements Initializable {

    UsuarioDto usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
//    public static String 
    public static String idioma = "E";

    public Stage pantalla;
    public boolean pantallaMin = false;
    public boolean pantallaMax = false;
    @FXML
    private VBox vbOpciones;
    @FXML
    private ImageView ivUserIcon;
    @FXML
    private JFXButton btnAgenda;
    @FXML
    private ImageView ivAgenda;
    @FXML
    private JFXButton btnRegPacientes;
    @FXML
    private ImageView ivPacientes;
    @FXML
    private JFXButton btnMantUsuarios;
    @FXML
    private ImageView ivMantUs;
    @FXML
    private JFXButton btnMantMedicos;
    @FXML
    private ImageView ivMantMed;
    @FXML
    private JFXButton btnExpediente;
    @FXML
    private ImageView ivExpediente;

    @FXML
    private Label lblNombre;
    @FXML
    private Label lblApellidos;
    @FXML
    private JFXButton btnReportes;
    @FXML
    private ImageView ivReportes;
    @FXML
    private BorderPane bpVistaBaseMod;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        bpVistaBase.setMinSize(840, 625);   
        idioma = (String) AppContext.getInstance().get("Idioma");
        lblNombre.setText(usuario.idpers.getNombrepers());
        lblApellidos.setText(usuario.idpers.getApellidospersona());
        if (usuario.getTipous().equals("A")) {

            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/Admin.png")));
        }
        if (usuario.getTipous().equals("R")) {

            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/recepcionist1.png")));
        }
        if (usuario.getTipous().equals("M")) {
            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/stethoscope.png")));
        }
    }

    // OVERIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
    }

    @FXML
    private void PantallaAgenda(ActionEvent event) {
        FlowController.getInstance().goview("AgendaCitas");

    }

    @FXML
    private void PantallaRegistroPacientes(ActionEvent event) {
        FlowController.getInstance().goview("RegistroPacientes");

    }

    @FXML
    private void PantallaMantUsuarios(ActionEvent event) {
        if (usuario.getTipous().equals("A")) {
            FlowController.getInstance().goview("MantenimientoUsuarios");
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        }

    }

    @FXML
    private void PantallaMantMedicos(ActionEvent event) {
        if (usuario.getTipous().equals("A")) {
            FlowController.getInstance().goview("MantenimientoMedicos");

        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        }

    }

    @FXML
    private void PantallaExpedientes(ActionEvent event) {
        if (usuario.getTipous().equals("R")) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        } else {
            FlowController.getInstance().goview("Expedientes");
        }
    }
    
    @FXML
    private void PantallaReportes(ActionEvent event) {
        if (usuario.getTipous().equals("R")) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        } else {
            FlowController.getInstance().goview("Reportes");
        }
    }
    
}
