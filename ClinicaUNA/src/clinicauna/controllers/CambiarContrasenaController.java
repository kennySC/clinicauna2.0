package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.services.UsuariosService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class CambiarContrasenaController extends Controller implements Initializable {

    @FXML
    private JFXButton btnConfirmContrasena;
    @FXML
    private AnchorPane apCambioContra;
    @FXML
    private ImageView imgCambioContrasena;
    @FXML
    private JFXPasswordField txtNuevaContra;
    @FXML
    private JFXPasswordField txtConfirmNuevaContra;

    private UsuarioDto usuario;
    
    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        formatoTextos();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {

    }

    // METODO DEL BOTON PARA CONFIRMAR LA NUEVA CONTRASENNA //  
    @FXML
    private void ConfirmNuevaContrasena(ActionEvent event) {

        if (!txtNuevaContra.getText().isEmpty() && !txtConfirmNuevaContra.getText().isEmpty()) {
            if (txtNuevaContra.getText().equals(txtConfirmNuevaContra.getText())) {
                Respuesta res = new UsuariosService().cambiarContrasenaUsuario(usuario.getIdus(), txtNuevaContra.getText());
                if (res.getEstado()) {
                    Mensaje.showModal(Alert.AlertType.INFORMATION, "Exito", getStage(), "La contrasena ha sido cambiada exitosamente");
                    AppContext.getInstance().set("Idioma", usuario.getIdiomaus());
                    FlowController.CambiarIdioma(usuario.getIdiomaus());
                    FlowController.getInstance().goMain();
                    FlowController.getInstance().goview("PantallaPrincipal");
                    getStage().close();
                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR,"Error", getStage(), "Ha ocurrido un error.");
                }
            } 
            // ELSE DE QUE LAS DOS CONTRASENAS NO SON IGUALES, AMBOS CAMPOS DE TEXTO NO SON IDENTICOS //
            else {
                new Mensaje().showModal(Alert.AlertType.ERROR,"Error", getStage(),"Las contraseñas deben de ser iguales en ambos campos de texto.");
            }
        }
        // ELSE DE QUE LOS CAMPOS DE TEXTO ESTAN VACIOS //
        else {
            new Mensaje().showModal(Alert.AlertType.ERROR,"Espacios Vacios", getStage(),"Porfavor verifique ambos campos de texto, ninguno puede ir vacio.");
        }

    }
    
    public void formatoTextos(){
        txtNuevaContra.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtConfirmNuevaContra.setTextFormatter(Formato.getInstance().letrasFormat(30));
    }

}
