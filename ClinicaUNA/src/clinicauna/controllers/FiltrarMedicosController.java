
package clinicauna.controllers;

import clinicauna.model.MedicoDto;
import clinicauna.services.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class FiltrarMedicosController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtFiltrarNombre;
    @FXML
    private JFXTextField txtFiltrarApellidos;
    @FXML
    private JFXTextField txtFiltrarCedula;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private TableView<MedicoDto> tvFiltroMedicos;

    private List<MedicoDto> listMedicos;
    @FXML
    private TableColumn<MedicoDto, String> tcNombre;
    @FXML
    private TableColumn<MedicoDto, String> tcApellidos;
    @FXML
    private TableColumn<MedicoDto, String> tcCedula;
    @FXML
    private TableColumn<MedicoDto, String> tcCarne;
    @FXML
    private JFXTextField txtFiltrarCarne;
    
    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        listMedicos = new ArrayList();
        tcNombre.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idPers.getNombrepers()));
        tcApellidos.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idPers.getApellidospersona()));
        tcCedula.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().idPers.getCedulapers()));
        tcCarne.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getCarnemed()));
        
        formatoTextos();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        
        Filtro();
        
    }

    @FXML
    private void AceptarFiltrar(ActionEvent event) {
        if(tvFiltroMedicos.getSelectionModel().getSelectedItem() != null){
            AppContext.getInstance().set("medicoSelect", tvFiltroMedicos.getSelectionModel().getSelectedItem());
            this.getStage().close();
        }else{
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), idioma.equals("E")?"No hay ningun medico selecionado. \nPor favor seleccione uno." :
                    idioma.equals("I")?"There must be a doctor selected, please select one." : "Deve haver um médico selecionado, por favor selecione um");
        }
    }

    // METODO QUE FILTRA A TIEMPO REAL MIENTRAS QUE EL USUARIO ESCRIBA EN CUALQUIERA DE LOS CAMPOS DE TEXTO //
    @FXML
    private void FiltrarMedicos(KeyEvent event) {
        Filtro();
    }
    
    void Filtro(){
        String ced = "%", nom = "%", ape = "%", carne = "%";

        tvFiltroMedicos.getItems().clear();

        if (!txtFiltrarCedula.getText().isEmpty()) {
            ced = txtFiltrarCedula.getText();
        }
        if (!txtFiltrarNombre.getText().isEmpty()) {
            nom = txtFiltrarNombre.getText();
        }
        if (!txtFiltrarApellidos.getText().isEmpty()) {
            ape = txtFiltrarApellidos.getText();
        }
        if (!txtFiltrarCarne.getText().isEmpty()) {
            carne = txtFiltrarCarne.getText();
        }

        Respuesta resp = new MedicoService().getMedicos("%" + ced + "%", "%" + nom + "%", "%" + ape + "%");
        listMedicos = (List<MedicoDto>) resp.getResultado("Medicos");
        if (listMedicos != null) {
            tvFiltroMedicos.getItems().addAll(FXCollections.observableArrayList(listMedicos));
        }
    }
    
    @FXML
    private void CancelarFiltrar(ActionEvent event) {
        ((Stage) btnCancelar.getScene().getWindow()).close();
    }

    public void formatoTextos(){
        txtFiltrarNombre.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarApellidos.setTextFormatter(Formato.getInstance().letrasFormat(100));
        txtFiltrarCedula.setTextFormatter(Formato.getInstance().cedulaFormat(12));
    }
    
}
