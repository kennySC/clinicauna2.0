package clinicauna.controllers;

import clinicauna.model.AntecedentesheredofamDto;
import clinicauna.model.DetalleexpedienteDto;
import clinicauna.model.EncabezadoexpedienteDto;
import clinicauna.model.RegistropacientesDto;
import clinicauna.services.AntecedentesHeredoFamService;
import clinicauna.services.DetalleExpedienteService;
import clinicauna.services.EncabezadoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class ExpedientesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNomPacienteCargado;
    @FXML
    private JFXTextField txtNombrePacienteInfo;
    @FXML
    private StackPane spReportes;
    @FXML
    private ImageView imgBusqueda;
    @FXML
    private JFXTextField txtHospitalizaciones;
    @FXML
    private JFXTextField txtOperaciones;
    @FXML
    private JFXTextArea txtAlergias;
    @FXML
    private JFXTextArea txtTratamientos;
    @FXML
    private JFXTextArea txtPatologico;
    @FXML
    private JFXTextField txtCedPaciente;
    @FXML
    private JFXTextField txtGeneroPaciente;
    @FXML
    private JFXTextField txtFechaNPaciente;
    @FXML
    private TableView<DetalleexpedienteDto> tvDetExp;
    @FXML
    private JFXButton btnGuardExped;
    @FXML
    private JFXButton btnExpedientesVolver;
    @FXML
    private TableColumn<DetalleexpedienteDto, LocalDate> tcFechaCita;
    @FXML
    private TableColumn<DetalleexpedienteDto, LocalTime> tcHoraCita;
    @FXML
    private ImageView imgEliminar;
    @FXML
    private JFXTextField txtFrecCard;
    @FXML
    private JFXTextField txtPeso;
    @FXML
    private JFXTextField txtTalla;
    @FXML
    private JFXTextField txtTemperatura;
    @FXML
    private JFXTextField txtIMC;
    @FXML
    private JFXTextField txtPresion;
    @FXML
    private JFXTextArea txtMotivConsulta;
    @FXML
    private JFXTextArea txtPlanAten;
    @FXML
    private JFXTextArea txtAnot;
    @FXML
    private JFXTextArea txtObserv;
    @FXML
    private JFXTextArea txtExamenes;
    // CON RESPECTO A LAS ENFERMEDADES HEREDO FAMILIARES //
    @FXML
    private TableView<AntecedentesheredofamDto> tvEnfermHF;
    @FXML
    private TableColumn<AntecedentesheredofamDto, String> tcEnfermedadHered;
    @FXML
    private TableColumn<AntecedentesheredofamDto, String> tcParentesco;
    @FXML
    private JFXTextField txtEnfermedadHF;
    @FXML
    private JFXTextField txtParentescoHF;
    @FXML
    private JFXButton btnAgregarEnfermHered;
    @FXML
    private JFXButton btnElimEnfermHered;
    @FXML
    private JFXButton btnLimpiarAntecedentes;

    @FXML
    private LineChart<String, Number> graficoIMC;
    @FXML
    private NumberAxis ejeYgrafIMC;
    @FXML
    private CategoryAxis ejeXgrafIMC;

    private RegistropacientesDto pacienteDto;
    private EncabezadoexpedienteDto encabexpDto;
    private DetalleexpedienteDto detexpDto;
    private List<AntecedentesheredofamDto> antecedentes;
    private List<DetalleexpedienteDto> detalles;
    private List<JFXTextArea> taDetExp = new ArrayList();
    private List<JFXTextField> tfEncabExp = new ArrayList();
    private List<JFXTextArea> taEncabExp = new ArrayList();
    private AntecedentesheredofamDto antecedenteDto;
    private final String idioma = (String) AppContext.getInstance().get("Idioma");

    @FXML
    private JFXButton btnElimExp;

    EncabezadoService encabService;
    @FXML
    private HBox hbGraficoIMC;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfEncabExp.add(txtHospitalizaciones);
        tfEncabExp.add(txtEnfermedadHF);
        tfEncabExp.add(txtParentescoHF);
        tfEncabExp.add(txtOperaciones);
        taEncabExp.add(txtAlergias);
        taEncabExp.add(txtPatologico);
        taDetExp.add(txtAnot);
        taDetExp.add(txtObserv);
        taDetExp.add(txtPlanAten);
        taDetExp.add(txtTratamientos);
        taDetExp.add(txtExamenes);

        txtNomPacienteCargado.setEditable(false);
        txtFechaNPaciente.setEditable(false);
        txtGeneroPaciente.setEditable(false);
        txtCedPaciente.setEditable(false);

        tcFechaCita.setCellValueFactory(value -> new SimpleObjectProperty(value.getValue().getFechadetexp()));
        tcHoraCita.setCellValueFactory(value -> new SimpleObjectProperty(value.getValue().getHoradetexp().toLocalTime()));
        tcEnfermedadHered.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getEnfermheredanteced()));
        tcParentesco.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getParentescoanteced()));

        tvDetExp.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
                if (tvDetExp.getSelectionModel().getSelectedItem() != null) {
                    detexpDto = tvDetExp.getSelectionModel().getSelectedItem();
                    bindDetExpediente();
                    editableDetExp(Boolean.TRUE);
                }
            }
        });
        tvEnfermHF.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
                antecedenteDto = tvEnfermHF.getSelectionModel().getSelectedItem();
                bindAntecedente();
            }
        });

    }

    //OVERRIDE GENERADO POR EL EXTENDS CONTROLLER//
    @Override
    public void initialize() {
        txtNombrePacienteInfo.setEditable(false);
        txtNomPacienteCargado.setEditable(false);
        txtFechaNPaciente.setEditable(false);
        txtGeneroPaciente.setEditable(false);
        txtCedPaciente.setEditable(false);
        txtFrecCard.setEditable(false);
        txtPeso.setEditable(false);
        txtTalla.setEditable(false);
        txtTemperatura.setEditable(false);
        txtIMC.setEditable(false);
        txtPresion.setEditable(false);
        encabexpDto = new EncabezadoexpedienteDto();
        pacienteDto = new RegistropacientesDto();
        detexpDto = new DetalleexpedienteDto();
        antecedenteDto = new AntecedentesheredofamDto();
        nuevoEncabezado();
        nuevoDetalle();
        nuevoAntecdente();
        nuevoPaciente();
        editableDetExp(Boolean.FALSE);
        editableEncabExp(Boolean.FALSE);
        tvDetExp.getItems().clear();
        tvEnfermHF.getItems().clear();
    }

    // ON ACTION EN EL IMAGEVIEW PARA BUSCAR EL PACIENTE DE EL QUE SE DESEE CARGAR UN EXPEDIENTE //
    @FXML
    private void buscarPaciente(MouseEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarPacientes", this.getStage(), false);
        if (AppContext.getInstance().get("pacienteSelect") != null) {
            pacienteDto = (RegistropacientesDto) AppContext.getInstance().get("pacienteSelect");
            cargarPaciente();
            cargarExpediente();
        }
    }

    // METODO DEL BOTON DE GUARDAR, PARA GUARDAR ENCABEZADO/DETALLE EXPEDIENTE //
    @FXML
    private void guardarExpedientePaciente(ActionEvent event) {

        encabexpDto.setIdpaciente(pacienteDto);
        bindEncabExpediente();
        Respuesta resEnc = new EncabezadoService().guardarExpediente(encabexpDto);
        if (resEnc.getEstado()) {
            Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Exito" : idioma.equals("I") ? "Success" : "Êxito", getStage(),
                    idioma.equals("E") ? "El expediente se ha guardado con exito." : idioma.equals("I") ? "The case file has been saved succesfully." : "O arquivo foi salvo com sucesso.");
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), resEnc.getMensaje());// MENSAJE DESDE EL SERVICE, CAMBIAR AHI //
        }

    }

    // METODO PARA LIMPIAR LA PANTALLA, METODO DE LA IMAGEN DEL BORRADOR //
    @FXML
    private void eliminarRegistro(MouseEvent event) {
        nuevoEncabezado();
        nuevoDetalle();
        nuevoAntecdente();
        nuevoPaciente();
        editableDetExp(Boolean.FALSE);
        editableEncabExp(Boolean.FALSE);
        tvDetExp.getItems().clear();
        tvEnfermHF.getItems().clear();
    }

    public void editableEncabExp(Boolean edit) {
        tfEncabExp.stream().forEach(x -> x.setEditable(edit));
        taEncabExp.stream().forEach(x -> x.setEditable(edit));
    }

    public void editableDetExp(Boolean edit) {
        taDetExp.stream().forEach(x -> x.setEditable(edit));
    }

    @FXML
    private void ExpedientesVolverPantPrincipal(ActionEvent event) {

        if (AppContext.getInstance().get("vistaAnterior") != null) {
            FlowController.getInstance().goview((String) AppContext.getInstance().get("vistaAnterior"));
        } else {
            FlowController.getInstance().goview("PantallaPrincipal");
        }

    }

    // BOTON PARA AGREGAR UNA ENFERMEDAD HEREDO FAMILIAR //
    @FXML
    private void AgregarEnfermHF(ActionEvent event) {
        if (!txtEnfermedadHF.getText().isEmpty() && !txtParentescoHF.getText().isEmpty()) {
            bindAntecedente();
            Respuesta res = new AntecedentesHeredoFamService().guardarAntecedHF(antecedenteDto);
            if (res.getEstado()) {
                nuevoAntecdente();
                cargarAntecedentes();
            } else {
                Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), res.getMensaje());// MENSAJE DESDE EL SERVICE, CAMBIAR AHI //
            }
        }
    }

    // BOTON PARA ELIMINAR UNA ENFERMEDAD HEREDO FAMILIAR //
    @FXML
    private void EliminarEnfermHF(ActionEvent event) {
        if (antecedenteDto.getIdantecedheredfam() != null) {
            Respuesta res = new AntecedentesHeredoFamService().eliminarAntecedente(antecedenteDto.getIdantecedheredfam());
            if (res.getEstado()) {
                cargarAntecedentes();
                nuevoAntecdente();
            } else {
                Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), "Error al eliminar el antecedente");
            }
        }
    }

    @FXML
    private void LimpiarAntecededntes(ActionEvent event) {
        nuevoAntecdente();
        cargarAntecedentes();
    }

    public void nuevoEncabezado() {
        unbindEncabExpediente();
        encabexpDto = new EncabezadoexpedienteDto();
        bindEncabExpediente();
    }

    public void nuevoPaciente() {
        AppContext.getInstance().delete("pacienteSelect");
        pacienteDto = new RegistropacientesDto();
        txtCedPaciente.textProperty().unbindBidirectional(encabexpDto.idpaciente.idpers.cedulapers);
        txtGeneroPaciente.textProperty().unbindBidirectional(encabexpDto.idpaciente.generop);
        txtNomPacienteCargado.clear();
        txtNombrePacienteInfo.clear();
        txtFechaNPaciente.clear();
        cargarPaciente();
    }

    public void nuevoDetalle() {
        unbindDetExpediente();
        tvDetExp.getItems().clear();
        detexpDto = new DetalleexpedienteDto();
        bindDetExpediente();
    }

    public void nuevoAntecdente() {
        unbindAntecedentes();
        antecedenteDto = new AntecedentesheredofamDto();
        bindAntecedente();
    }

    public void cargarPaciente() {
        txtCedPaciente.textProperty().bindBidirectional(pacienteDto.idpers.cedulapers);
        txtGeneroPaciente.textProperty().bindBidirectional(pacienteDto.generop);
        txtNomPacienteCargado.setText(pacienteDto.idpers.getNombrepers() + pacienteDto.idpers.getApellidospersona());
        txtNombrePacienteInfo.setText(pacienteDto.idpers.getNombrepers() + pacienteDto.idpers.getApellidospersona());
        if (pacienteDto.getFechanacimientop() != null) {
            txtFechaNPaciente.setText(pacienteDto.getFechanacimientop().toString());
        }

    }

    public void bindEncabExpediente() {
        txtAlergias.textProperty().bindBidirectional(encabexpDto.alergiasencabexp);
        txtHospitalizaciones.textProperty().bindBidirectional(encabexpDto.hospitalizacionesencabexp);
        txtOperaciones.textProperty().bindBidirectional(encabexpDto.operacionesencabexp);
        txtPatologico.textProperty().bindBidirectional(encabexpDto.patologicoencabexp);
    }

    public void unbindEncabExpediente() {
        txtAlergias.textProperty().unbindBidirectional(encabexpDto.alergiasencabexp);
        txtHospitalizaciones.textProperty().unbindBidirectional(encabexpDto.hospitalizacionesencabexp);
        txtOperaciones.textProperty().unbindBidirectional(encabexpDto.operacionesencabexp);
        txtPatologico.textProperty().unbindBidirectional(encabexpDto.patologicoencabexp);
    }

    public void bindDetExpediente() {
        txtFrecCard.textProperty().bindBidirectional(detexpDto.freccarddetexp);
        txtPeso.textProperty().bindBidirectional(detexpDto.pesodetexp);
        txtTalla.textProperty().bindBidirectional(detexpDto.talladetexp);
        txtTemperatura.textProperty().bindBidirectional(detexpDto.temperaturadetexp);
        txtIMC.textProperty().bindBidirectional(detexpDto.imcdetexp);
        txtPresion.textProperty().bindBidirectional(detexpDto.presiondetexp);
        txtMotivConsulta.textProperty().bindBidirectional(detexpDto.motivoconsultadetexp);
        txtPlanAten.textProperty().bindBidirectional(detexpDto.planatenciondetexp);
        txtAnot.textProperty().bindBidirectional(detexpDto.anotacionesdetexp);
        txtObserv.textProperty().bindBidirectional(detexpDto.observdetexp);
        txtExamenes.textProperty().bindBidirectional(detexpDto.examfisicodetexp);
        txtTratamientos.textProperty().bindBidirectional(detexpDto.tratamientodetexp);
    }

    public void unbindDetExpediente() {
        txtFrecCard.textProperty().unbindBidirectional(detexpDto.freccarddetexp);
        txtPeso.textProperty().unbindBidirectional(detexpDto.pesodetexp);
        txtTalla.textProperty().unbindBidirectional(detexpDto.talladetexp);
        txtTemperatura.textProperty().unbindBidirectional(detexpDto.temperaturadetexp);
        txtIMC.textProperty().unbindBidirectional(detexpDto.imcdetexp);
        txtPresion.textProperty().unbindBidirectional(detexpDto.presiondetexp);
        txtMotivConsulta.textProperty().unbindBidirectional(detexpDto.motivoconsultadetexp);
        txtPlanAten.textProperty().unbindBidirectional(detexpDto.planatenciondetexp);
        txtAnot.textProperty().unbindBidirectional(detexpDto.anotacionesdetexp);
        txtObserv.textProperty().unbindBidirectional(detexpDto.observdetexp);
        txtExamenes.textProperty().unbindBidirectional(detexpDto.examfisicodetexp);
        txtTratamientos.textProperty().unbindBidirectional(detexpDto.tratamientodetexp);
    }

    public void bindAntecedente() {
        antecedenteDto.setIdencabexp(encabexpDto);
        txtParentescoHF.textProperty().bindBidirectional(antecedenteDto.parentescoanteced);
        txtEnfermedadHF.textProperty().bindBidirectional(antecedenteDto.enfermheredanteced);
    }

    public void unbindAntecedentes() {
        txtParentescoHF.textProperty().unbindBidirectional(antecedenteDto.parentescoanteced);
        txtEnfermedadHF.textProperty().unbindBidirectional(antecedenteDto.enfermheredanteced);
    }

    public void cargarExpediente() {
        Respuesta res = new EncabezadoService().cargarExpedinte(pacienteDto.getIdpaciente());
        if (res.getEstado()) {
            editableEncabExp(Boolean.TRUE);
            encabexpDto = (EncabezadoexpedienteDto) res.getResultado("Expediente");
            bindEncabExpediente();
            cargarAntecedentes();
            cargarDetalles();
        } else {
            nuevoAntecdente();
            nuevoDetalle();
            editableEncabExp(Boolean.TRUE);
            Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), idioma.equals("E") ? "El paciente no cuenta con un expediente."
                    : idioma.equals("I") ? "The patient currently doesn't have a case file." : "O paciente não possui um arquivo.");
        }
    }

    public void cargarAntecedentes() {
        tvEnfermHF.getItems().clear();
        Respuesta res = new AntecedentesHeredoFamService().cargarAntecedHF(encabexpDto.getIdencabexp());
        if (res.getEstado()) {
            antecedentes = (List<AntecedentesheredofamDto>) res.getResultado("Antecedentes");
            tvEnfermHF.getItems().addAll(antecedentes);
        }
    }

    public void cargarDetalles() {
        tvDetExp.getItems().clear();
        detalles = new ArrayList();
        Respuesta res = new DetalleExpedienteService().cargarDetExpedientes(encabexpDto.getIdencabexp());
        if (res.getEstado()) {
            detalles = (List<DetalleexpedienteDto>) res.getResultado("DetExp");
            tvDetExp.getItems().addAll(detalles);
            graficaIMC();
        }
    }

    // METODO PARA LLENAR EL GRAFICO CON EL IMC CALCULADO DEL  //
    private void graficaIMC() {
        graficoIMC.getData().clear();
        XYChart.Series<String, Number> serieGrafico = new XYChart.Series<>();

        for (DetalleexpedienteDto det : detalles) {
            serieGrafico.getData().add(new XYChart.Data<>(det.getFechadetexp().toString(), det.getImcdetexp()));
        }
        serieGrafico.setName(idioma.equals("E") ? "IMC actual" : idioma.equals("I") ? "Current BMI" : "IMC atual");

        XYChart.Series<String, Number> imcBajoPeso = new XYChart.Series<>();
        for (DetalleexpedienteDto det : detalles) {
            imcBajoPeso.getData().add(new XYChart.Data<>(det.getFechadetexp().toString(), 18));
        }
        imcBajoPeso.setName(idioma.equals("E") ? "Bajo Peso" : idioma.equals("I") ? "Low Weight" : "Baixo peso");

        XYChart.Series<String, Number> imcObesidad = new XYChart.Series<>();
        for (DetalleexpedienteDto det : detalles) {
            imcObesidad.getData().add(new XYChart.Data<>(det.getFechadetexp().toString(), 30));
        }
        imcObesidad.setName(idioma.equals("E") ? "Obesidad" : idioma.equals("I") ? "Obesity" : "Obesidade");
        
        XYChart.Series<String, Number> imcSobrePeso= new XYChart.Series<>();
        for (DetalleexpedienteDto det : detalles) {
            imcSobrePeso.getData().add(new XYChart.Data<>(det.getFechadetexp().toString(), 22));
        }
        imcSobrePeso.setName(idioma.equals("E") ? "Sobrepeso" : idioma.equals("I") ? "Overweight" : "Excesso de peso");
        graficoIMC.getData().addAll(imcBajoPeso,imcObesidad,serieGrafico,imcSobrePeso);
    }

    @FXML
    private void eliminarExpediente(ActionEvent event) {

        if (AppContext.getInstance().get("pacienteSelect") != null && encabexpDto.idencabexp != null) {
            Boolean mensajeElimExp = new Mensaje().showConfirmation(idioma.equals("E") ? "Eliminar Expediente" : idioma.equals("I") ? "Case File Removal" : "Excluir arquivo", getStage(),
                    idioma.equals("E") ? "Esta seguro que desea eliminar el expediente del paciente?" : idioma.equals("I") ? "Are you sure you wish to delete the patient's case file?" : "Tem certeza de que deseja excluir o arquivo do paciente?");
            if (mensajeElimExp) {
                try {

                    Respuesta res = new EncabezadoService().eliminarExpediente(encabexpDto.getIdencabexp());
                    if (res.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Eliminar Expediente" : idioma.equals("I") ? "Case File Removal" : "Excluir arquivo", getStage(),
                                idioma.equals("E") ? "Expediente eliminado con exito." : idioma.equals("I") ? "Case file removed successfully." : "Arquivo eliminado com sucesso");
                        nuevoEncabezado();
                        nuevoDetalle();
                        nuevoAntecdente();

                    }
                } catch (Exception e) {
                    Logger.getLogger(RegistroPacientesController.class.getName()).log(Level.SEVERE, "Error eliminando al medico.", e);
                    new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Eliminar Medico" : idioma.equals("I") ? "Doctor Removal" : "Excluir médico", getStage(),
                            idioma.equals("E") ? "Ocurrior un error al eliminar al medico." : idioma.equals("I") ? "An error ocurred while trying to remove the doctor." : "Ocorreu um erro ao excluir o médico.");
                }
            }
        }
    }

}
