package clinicauna.controllers;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.UsuarioDto;
import clinicauna.services.DetalleCitaService;
import clinicauna.services.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.ContenedorCita;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class AgendaCitasController extends Controller implements Initializable {

    @FXML
    private JFXButton btnVolver;
    @FXML
    private JFXTextField txtNomMedCita;
    @FXML
    private JFXDatePicker dpDiaCita;
    @FXML
    private ImageView imgBusqueda;
    @FXML
    private VBox vBoxAgenda;
    @FXML
    private StackPane spAgenda;
    private UsuarioDto usuario;
    private MedicoDto medDto;
    private List<MedicoDto> meds;
    private List<DetallecitaDto> citas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        usuario=(UsuarioDto) AppContext.getInstance().get("Usuario");
        txtNomMedCita.setEditable(false);
        if(usuario.getTipous().equals("M")){
            imgBusqueda.setVisible(false);
            imgBusqueda.setDisable(true);
            Respuesta resp = new MedicoService().getMedicos(usuario.getIdpers().getCedulapers(), usuario.getIdpers().getNombrepers(), usuario.getIdpers().getApellidospersona());
            meds = (List<MedicoDto>) resp.getResultado("Medicos");
            medDto=meds.get(0);
            txtNomMedCita.setText(medDto.getIdPers().getNombrepers()+" "+medDto.getIdPers().getApellidospersona());
            
        }
    }


    @FXML
    private void volverPantallaPrincipal(ActionEvent event) {
        if (Mensaje.showConfirmation(AppContext.getInstance().get("Idioma").equals("E") ? "Salir" : AppContext.getInstance().get("Idioma").equals("I") ? "Exit" : "Sair",
                getStage(), AppContext.getInstance().get("Idioma").equals("E") ? "¿Esta seguro que desea volver al menu principal?" : AppContext.getInstance().get("Idioma").equals("I") ? "Are you sure you want to exit the main menu?" : "Tem certeza de que deseja retornar ao menu principal?")) {
            FlowController.getInstance().goview("PantallaPrincipal");
        }
    }

    // ON ACTION EN EL ICONO DE LUPA PARA BUSCAR A QUE MEDICO SE LE VA ASIGNAR UNA CITA //
    @FXML
    private void BusquedaMedicoCita(MouseEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarMedicos", this.getStage(), false);
        if (AppContext.getInstance().get("medicoSelect") != null) {
            medDto = (MedicoDto) AppContext.getInstance().get("medicoSelect");
            txtNomMedCita.setText(medDto.idPers.getNombrepers() + " " + medDto.idPers.getApellidospersona());
            cargarAgenda();
        }
    }

    public void cargarAgenda() {

        vBoxAgenda.getChildren().clear();

        if (medDto != null && dpDiaCita.getValue() != null) {

            Respuesta res = new DetalleCitaService().getCitas(medDto.getIdmed());

            if (res.getEstado()) {
                citas = (List<DetallecitaDto>) res.getResultado("Citas");               
            }

            AppContext.getInstance().set("FechaCita", dpDiaCita.getValue());
            int fil;
            if (medDto.getFinjornadamed().getHour() < medDto.getInijornadamed().getHour()) {
                fil = Math.toIntExact(ChronoUnit.HOURS.between(medDto.getFinjornadamed(), medDto.getInijornadamed()));
                fil = 24-fil;
            } else {
                fil = -Math.toIntExact(ChronoUnit.HOURS.between(medDto.getFinjornadamed(), medDto.getInijornadamed()));
            }
            int col = Integer.parseInt(medDto.getEspaciosmed());
            
            System.out.println(fil + " : " + col);
            
            GridPane gpAgenda = new GridPane();
            
            gpAgenda.setGridLinesVisible(true);

            int min = 0, hora = medDto.getInijornadamed().getHour();

            for (int i = 0; i < fil; i++) {
                for (int j = 0; j < col; j++) {
                    ContenedorCita cCita = null;
                    if (!citas.isEmpty()) {
                        for (DetallecitaDto cita : citas) {
                            if (cita.getFechacita().equals(dpDiaCita.getValue()) && cita.getHoracita().getHour() == hora
                                    && cita.getHoracita().getMinute() == min) {
                                cCita = new ContenedorCita(cita);     
                                break;
                            } else {
                                cCita = new ContenedorCita(hora, min, j + 1);                                
                            }                            
                        }
                    } else {
                        cCita = new ContenedorCita(hora, min, j + 1);
                    }
                    
                    gpAgenda.add(cCita, j, i);

                    if (Integer.parseInt(medDto.getEspaciosmed()) == 2) {
                        min += 30;
                    } else if (Integer.parseInt(medDto.getEspaciosmed()) == 3) {
                        min += 20;
                    } else if (Integer.parseInt(medDto.getEspaciosmed()) == 4) {
                        min += 15;
                    }
                    cCita.setOnDragOver(value ->{
                        Dragboard db = value.getDragboard();
                        if(db.hasContent(DataFormat.RTF)){
                            value.acceptTransferModes(TransferMode.MOVE);
                        }
                        value.consume();
                    });                    
                }

                gpAgenda.getChildren().get(0).setStyle("-fx-alignment: CENTER");
                min = 0;
                hora++;
                if(hora == 24){
                    hora = 0;
                }
            }

            FadeTransition trans1 = new FadeTransition(Duration.millis(300), vBoxAgenda);
            trans1.setByValue(-1);
            trans1.setOnFinished(f -> {
                vBoxAgenda.setAlignment(Pos.CENTER);
                vBoxAgenda.getChildren().add(gpAgenda);
                FadeTransition trans2 = new FadeTransition(Duration.millis(300), vBoxAgenda);
                trans2.setByValue(1);
                trans2.play();
            });
            trans1.play();
        }

    }

    @FXML
    private void cargarCitas(ActionEvent event) {
        AppContext.getInstance().set("fecha", dpDiaCita.getValue());
        cargarAgenda();
    }    

}
