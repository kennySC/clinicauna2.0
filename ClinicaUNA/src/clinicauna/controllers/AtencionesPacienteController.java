package clinicauna.controllers;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.DetalleexpedienteDto;
import clinicauna.model.EncabezadoexpedienteDto;
import clinicauna.services.DetalleExpedienteService;
import clinicauna.services.EncabezadoService;
import clinicauna.util.AppContext;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class AtencionesPacienteController extends Controller implements Initializable {

    @FXML
    private JFXDatePicker dpFechaCita;
    @FXML
    private JFXTimePicker tpHoraCita;
    @FXML
    private JFXTextField txtPresion;
    @FXML
    private JFXTextField txtFrecCard;
    @FXML
    private JFXTextField txtPeso;
    @FXML
    private JFXTextField txtTalla;
    @FXML
    private JFXTextField txtTemperatura;
    @FXML
    private JFXTextField txtIMC;
    @FXML
    private JFXTextArea txtMotivConsulta;
    @FXML
    private JFXTextArea txtPlanAten;
    @FXML
    private JFXTextArea txtAnot;
    @FXML
    private JFXTextArea txtObserv;
    @FXML
    private JFXTextArea txtExamenes;
    @FXML
    private JFXTextArea txtTratamientos;
    @FXML
    private JFXButton btnVolver;
    @FXML
    private ImageView imgLogoClinica;
    @FXML
    private Label lblAtenPac;
    @FXML
    private JFXButton btnGuardAten;
    @FXML
    private JFXTextField txtNombrePaciente;
    @FXML
    private JFXTextField txtCedPaciente;
    private List<DetalleexpedienteDto> detalles = new ArrayList();
    private DetallecitaDto cita;
    private DetalleexpedienteDto detalle;
    private EncabezadoexpedienteDto encabezado;
    private List<JFXTextField> necesarios = new ArrayList<>();

    String idioma = (String) AppContext.getInstance().get("Idioma");

    @FXML
    private JFXButton btnCalcIMC;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        editable();
        necesarios.add(txtPeso);
        necesarios.add(txtFrecCard);
        necesarios.add(txtIMC);
        necesarios.add(txtPeso);
        necesarios.add(txtPresion);
        necesarios.add(txtTalla);
        necesarios.add(txtTemperatura);

        formatoCamposdeTextos();

    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        AppContext.getInstance().set("AtencionGuardada", Boolean.FALSE);
        detalle = new DetalleexpedienteDto();
        cita = (DetallecitaDto) AppContext.getInstance().get("citaAtender");
        Respuesta resEnc = new EncabezadoService().cargarExpedinte(cita.idpaciente.getIdpaciente());
        encabezado = (EncabezadoexpedienteDto) resEnc.getResultado("Expediente");
        detalle.setFechadetexp(cita.getFechacita());
        detalle.setHoradetexp(cita.getHoracita());
        detalle.setMotivoconsultadetexp(cita.getMotivocita());
        detalle.setIdencabexp(encabezado);
        BindDetalleExp();
        dpFechaCita.setDisable(true);
        tpHoraCita.setDisable(true);
        txtIMC.setEditable(false);
    }

    @FXML
    private void guardarAtencionPaciente(ActionEvent event) {
        if (validarEspacios()) {
            Respuesta res = new DetalleExpedienteService().guardarDetExpediente(detalle);
            if (res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Exito" : idioma.equals("I") ? "Success" : "Êxito", getStage(),
                        idioma.equals("E") ? "La cita se ha guardado con exito" : idioma.equals("I") ? "The appointment has been saved succesfully." : "O compromisso foi salvo com sucesso.");
                AppContext.getInstance().set("AtencionGuardada", Boolean.TRUE);
                getStage().close();
            } else {
                Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), res.getMensaje());//  MENSAJE QUE VIENE DESDE EL SERVICE, CAMBIAR IDIOMA AHI //
            }
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Espacios vacios" : idioma.equals("I") ? "Missing information" : "Espaços vazios", getStage(),
                    idioma.equals("E") ? "Favor revise los esapcios vacios" : idioma.equals("I") ? "Please check the information, there are empty spaces." : "Por favor, verifique os espaços vazios");
        }
    }

    private Boolean validarEspacios() {
        Boolean faltan = Boolean.TRUE;
        if (dpFechaCita.getValue().isBefore(LocalDate.now())) {
            faltan = Boolean.FALSE;
        }
        for (JFXTextField t : necesarios) {
            if (t.getText() == null || t.getText().isEmpty()) {
                faltan = Boolean.FALSE;
            }
        }
        return faltan;
    }

    private void nuevoDetalle() {
        unBindDetalleExp();
        detalle = new DetalleexpedienteDto();
        BindDetalleExp();
    }

    private void BindDetalleExp() {
        txtNombrePaciente.setText(cita.getIdpaciente().getIdpers().getNombrepers() + cita.getIdpaciente().getIdpers().getApellidospersona());
        txtCedPaciente.setText(cita.idpaciente.idpers.getCedulapers());
        if (detalle.getFechadetexp() != null) {
            dpFechaCita.valueProperty().bindBidirectional(detalle.fechadetexp);
        }
        if (detalle.getHoradetexp() != null) {
            tpHoraCita.setValue(LocalTime.of(detalle.getHoradetexp().getHour(), detalle.getHoradetexp().getMinute()));
        }
        txtAnot.textProperty().bindBidirectional(detalle.anotacionesdetexp);
        txtExamenes.textProperty().bindBidirectional(detalle.examfisicodetexp);
        txtFrecCard.textProperty().bindBidirectional(detalle.freccarddetexp);
        txtIMC.textProperty().bindBidirectional(detalle.imcdetexp);
        txtMotivConsulta.textProperty().bindBidirectional(detalle.motivoconsultadetexp);
        txtObserv.textProperty().bindBidirectional(detalle.observdetexp);
        txtPeso.textProperty().bindBidirectional(detalle.pesodetexp);
        txtPlanAten.textProperty().bindBidirectional(detalle.planatenciondetexp);
        txtPresion.textProperty().bindBidirectional(detalle.presiondetexp);
        txtTalla.textProperty().bindBidirectional(detalle.talladetexp);
        txtTemperatura.textProperty().bindBidirectional(detalle.temperaturadetexp);
        txtTratamientos.textProperty().bindBidirectional(detalle.tratamientodetexp);
    }

    private void unBindDetalleExp() {
        txtNombrePaciente.clear();
        txtCedPaciente.clear();
        if (detalle.getFechadetexp() != null) {
            dpFechaCita.valueProperty().bindBidirectional(detalle.fechadetexp);
        }
        txtAnot.textProperty().unbindBidirectional(detalle.anotacionesdetexp);
        txtExamenes.textProperty().unbindBidirectional(detalle.examfisicodetexp);
        txtFrecCard.textProperty().unbindBidirectional(detalle.freccarddetexp);
        txtIMC.textProperty().unbindBidirectional(detalle.imcdetexp);
        txtMotivConsulta.textProperty().unbindBidirectional(detalle.motivoconsultadetexp);
        txtObserv.textProperty().unbindBidirectional(detalle.observdetexp);
        txtPeso.textProperty().unbindBidirectional(detalle.pesodetexp);
        txtPlanAten.textProperty().unbindBidirectional(detalle.planatenciondetexp);
        txtPresion.textProperty().unbindBidirectional(detalle.presiondetexp);
        txtTalla.textProperty().unbindBidirectional(detalle.talladetexp);
        txtTemperatura.textProperty().unbindBidirectional(detalle.temperaturadetexp);
        txtTratamientos.textProperty().unbindBidirectional(detalle.tratamientodetexp);
    }

    public void editable() {
        txtNombrePaciente.setEditable(Boolean.FALSE);
        txtCedPaciente.setEditable(Boolean.FALSE);
        txtMotivConsulta.setEditable(Boolean.FALSE);
        tpHoraCita.setEditable(Boolean.FALSE);
        dpFechaCita.setEditable(Boolean.FALSE);
    }

    public void formatoCamposdeTextos() {
        txtPresion.setTextFormatter(Formato.getInstance().twoDecimalFormat());
        txtFrecCard.setTextFormatter(Formato.getInstance().maxLengthFormat(6));
        txtPeso.setTextFormatter(Formato.getInstance().twoDecimalFormat());
        txtTalla.setTextFormatter(Formato.getInstance().integerFormat());
        txtTemperatura.setTextFormatter(Formato.getInstance().twoDecimalFormat());
        txtIMC.setTextFormatter(Formato.getInstance().maxLengthFormat(50));
        txtMotivConsulta.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
        txtPlanAten.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
        txtAnot.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
        txtObserv.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
        txtExamenes.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
        txtTratamientos.setTextFormatter(Formato.getInstance().maxLengthFormat(500));
    }

    // BOTON PARA CALCULAR EL IMC //
    @FXML
    private void calcularIMC(ActionEvent event) {
        if (txtPeso.getText().isEmpty() || txtTalla.getText().isEmpty()) {
            new Mensaje().show(Alert.AlertType.WARNING, "Error", idioma.equals("E") ? "Debe de ingresar el peso y la altura para calcular el IMC." : idioma.equals("I") ? "Please enter weight and height to calculate BMI."
                    : "Você deve inserir o peso e a altura para calcular o IMC.");
        } else {
            float peso = Float.parseFloat(txtPeso.getText());
            float altura = Float.parseFloat(txtTalla.getText());
            altura = altura / 100;
            float imc = peso / (altura * altura);
            txtIMC.setText(String.valueOf(imc));
            detalle.setImcdetexp(imc);
        }
    }

    // BOTON DE VOLVER //
    @FXML
    private void volverPantalla(ActionEvent event) {
        this.getStage().close();
    }

}
