package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.services.UsuariosService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class LogInController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXPasswordField txtClave;
    @FXML
    private JFXButton btnIngresar;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnOlvidoContra;
    @FXML
    private VBox vbLogIn;
    @FXML
    private ImageView ivTopLogin;
    @FXML
    private ImageView ivUserLogin;
    @FXML
    private ImageView ivPassLogin;
    @FXML
    private StackPane spParent;

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        formatoTextos();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
//        ivImagenFondoLogin.fitHeightProperty().bind(spRootLogIn.heightProperty());
//        ivImagenFondoLogin.fitWidthProperty().bind(spRootLogIn.widthProperty());        
        txtClave.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    IngresarSistema(new ActionEvent());
                }
            }
        });
    }

    @FXML
    private void IngresarSistema(ActionEvent event) {
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Validación de usuario",getStage(), "Es necesario digitar un usuario para ingresar al sistema.");
            } else if (txtClave.getText() == null || txtClave.getText().isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Validación de usuario",getStage(), "Es necesario digitar una contraseña para ingresar al sistema.");               
            } else {
                UsuariosService usService = new UsuariosService();
                Respuesta respuesta = usService.validarUsuario(txtUsuario.getText(), txtClave.getText());
                if (respuesta.getEstado()) {
                    UsuarioDto us = (UsuarioDto) respuesta.getResultado("Usuario");
                    AppContext.getInstance().set("Usuario", us);
                    if (us.getEstadous().equals("A")) {
                        if (isContrasenaTemporal(us.getPasswordus())) {
                            FlowController.getInstance().goviewInWindow("CambiarContrasena");
                        } else {
                            System.out.println(us.idpers.getNombrepers());
                            prepararApp(us);
                        }
                        getStage().close();
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, "Ingreso Invalido", getStage(), "Su usuario no esta activado.\nIngrese a su correo para activarlo.");
                    }
                } // USUARIO NO FUE ENCONTRADO O NO EXISTE //
                else {
                    Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), "Usuario no encontrado");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }

    }

    @FXML
    private void RecuperarContrasenna(ActionEvent event) {
        if (!txtUsuario.getText().isEmpty()) {

            Respuesta res = new UsuariosService().recuperarContrasenaUsuario(txtUsuario.getText());
            if (res.getEstado()) {
                UsuarioDto usuario = (UsuarioDto) res.getResultado("Usuario");
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Exito", this.getStage(), "Se le ha enviado un correo con una contraseña temporal.\nIngrese al sistema con ella y podra proceder a ingresar una nueva contraseña.");
                String correo = usuario.idpers.getCorreopers();
                ControllerCorreo cCorreo = new ControllerCorreo();
                Platform.runLater(
                        () -> {
                            cCorreo.getCorreo().formatoCambioContrasena(usuario);
                            Respuesta resCorreo = cCorreo.enviarEmailHTML(correo);
                        }
                );
            } else {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), res.getMensaje());
            }

        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", getStage(), "Porfavor ingrese su nombre de usuario.");
        }
    }

    @FXML
    private void SalirLogIn(ActionEvent event) {
        System.exit(0);
    }

    public Boolean isContrasenaTemporal(String password) {
        if (password.startsWith("cT") && password.endsWith("Tc")) {
            System.out.println("Contraseña Nueva");
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public void prepararApp(UsuarioDto us) {
        AppContext.getInstance().set("Idioma", us.getIdiomaus());
        FlowController.CambiarIdioma(us.getIdiomaus());
        FlowController.getInstance().goMain();
        FlowController.getInstance().goview("PantallaPrincipal");
    }
    
    public void formatoTextos(){
        txtUsuario.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
        txtClave.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
    }

}
