package clinicauna.controllers;

import clinicauna.model.DetallecitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.RegistropacientesDto;
import clinicauna.model.UsuarioDto;
import clinicauna.services.DetalleCitaService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Kenneth Sibaja
 */
public class GuardarCitaController extends Controller implements Initializable {

    @FXML
    private AnchorPane apFondoCita;
    @FXML
    private JFXTextField txtNombreCompleto;
    @FXML
    private ImageView imgBusqueda;
    @FXML
    private ImageView imgNuevo;
    @FXML
    private JFXDatePicker dpFechaCita;
    @FXML
    private JFXTextArea txtMotivoCita;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnCancelar;

    RegistropacientesDto paciente;
    MedicoDto medico;
    UsuarioDto usuario;
    DetallecitaDto cita;

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        formato();
    }

    @Override
    public void initialize() {
        txtNombreCompleto.setEditable(Boolean.FALSE);
        cita = new DetallecitaDto();
        if (AppContext.getInstance().get("CitaEditar") != null) {
            cita = (DetallecitaDto) AppContext.getInstance().get("CitaEditar");
            paciente = cita.getIdpaciente();
            txtNombreCompleto.setText(paciente.idpers.getNombrepers() + " " + paciente.idpers.getApellidospersona());
        }
        paciente = new RegistropacientesDto();
        medico = (MedicoDto) AppContext.getInstance().get("medicoSelect");
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        dpFechaCita.setValue((LocalDate) AppContext.getInstance().get("fecha"));
    }

    @FXML
    private void buscarPaciente(MouseEvent event) {
        FlowController.getInstance().goviewInWindowModal("FiltrarPacientes", this.getStage(), Boolean.FALSE);
        if (AppContext.getInstance().get("pacienteSelect") != null) {
            paciente = (RegistropacientesDto) AppContext.getInstance().get("pacienteSelect");
            txtNombreCompleto.setText(paciente.idpers.getNombrepers() + " " + paciente.idpers.getApellidospersona());
        }
    }

    @FXML
    private void nuevoPaciente(MouseEvent event) {
        FlowController.getInstance().goviewInWindowModal("RegistroPacientes", this.getStage(), Boolean.FALSE);
        if (AppContext.getInstance().get("Paciente") != null) {
            paciente = (RegistropacientesDto) AppContext.getInstance().get("Paciente");
            txtNombreCompleto.setText(paciente.idpers.getNombrepers() + " " + paciente.idpers.getApellidospersona());
        }
    }

    @FXML
    private void GuardarCita(ActionEvent event) {

        if (paciente != null && !txtMotivoCita.getText().isEmpty()) {
            cita.setIdus(usuario);
            cita.setIdmed(medico);
            cita.setIdpaciente(paciente);
            cita.setMotivocita(txtMotivoCita.getText());
            cita.setHoracita(LocalDateTime.of(LocalDate.now(), (LocalTime) AppContext.getInstance().get("HoraCita")));
            cita.setFechacita((LocalDate) AppContext.getInstance().get("FechaCita"));
            cita.setEspaciocita(String.valueOf((Integer) AppContext.getInstance().get("EspacioCita")));
            cita.setEstadocita("Pr");
            Respuesta res = new DetalleCitaService().guardarDetCita(cita);
            if (res.getEstado()) {
                Mensaje.showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Exito" : idioma.equals("I") ? "Success" : "Êxito", getStage(),
                        idioma.equals("E") ? "La cita se ha guardado con exito" : idioma.equals("I") ? "The appointment has been saved succesfully." : "O compromisso foi salvo com sucesso.");
                cita = (DetallecitaDto) res.getResultado("Cita");
                AppContext.getInstance().set("Cita", cita);
                cita = new DetallecitaDto();
                paciente = new RegistropacientesDto();
                FlowController.getInstance().desvincular("GuardarCita");
                this.getStage().close();
            } else {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), idioma.equals("E") ? "Error al guardar la cita"
                        : idioma.equals("I") ? "An error occured registering the appointment" : "Erro ao salvar o compromisso");
            }
        } // ELSE DE QUE HAY CAMPOS DE TEXTO VACIOS //
        else {
            Mensaje.showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Espacios vacios" : idioma.equals("I") ? "Missing information" : "Espaços vazios", getStage(),
                    idioma.equals("E") ? "Favor revise los esapcios vacios" : idioma.equals("I") ? "Please check the information, there are empty spaces." : "Por favor, verifique os espaços vazios");
        }
    }

    @FXML
    private void Cancelar(ActionEvent event) {
        FlowController.getInstance().desvincular("GuardarCita");
        this.getStage().close();
    }

    public void formato(){
        txtMotivoCita.setTextFormatter(Formato.getInstance().letrasFormat(500));
        txtNombreCompleto.setTextFormatter(Formato.getInstance().letrasFormat(100));
    }
    
}
