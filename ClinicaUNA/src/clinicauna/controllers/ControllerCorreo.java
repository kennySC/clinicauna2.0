/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controllers;

import clinicauna.util.Correo;
import clinicauna.util.Respuesta;
import java.util.Properties;
import javafx.scene.control.Alert;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Kenneth Sibaja
 */
public class ControllerCorreo {
    Correo correo = new Correo();
    
    public Respuesta enviarEmailHTML(String recipients){
        
        Alert alerta = new Alert(null);        
        
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");                    
        props.put("mail.smtp.host","smtp.gmail.com");                        
        props.put("mail.smtp.port", "587");


        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {                
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(correo.getUsuarioCorreo(), correo.getContrasena());
            };
        });
        
        try{                                                             
            
            Message mensaje = new MimeMessage(session);
            mensaje.setFrom(new InternetAddress(correo.getUsuarioCorreo()));
            mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
            mensaje.setSubject(correo.getAsunto());
            mensaje.setContent(correo.getFormatoHTML(), "text/html");
            
            
            Transport.send(mensaje);            
            
            System.out.println("Correo enviado correctamente");
            
//            alerta.setAlertType(Alert.AlertType.INFORMATION);
//            alerta.setContentText("Enviado Correctamente");
            
            return new Respuesta(Boolean.TRUE, "Correo Enviado Correctamente", "");
        }catch(Exception ex){
            ex.printStackTrace();
//            alerta.setAlertType(Alert.AlertType.ERROR);
//            alerta.setContentText("no enviado");
            return new Respuesta(Boolean.FALSE, "Error al enviar el correo", "");
        }
    }

    public Correo getCorreo() {
        return correo;
    }

    public void setCorreo(Correo correo) {
        this.correo = correo;
    }
    
    
    
}
