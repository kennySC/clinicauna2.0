package clinicaunaWS.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_DETALLECITA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDetallecita.findAll", query = "SELECT t FROM TbDetallecita t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbDetallecita.findByIddetcita", query = "SELECT t FROM TbDetallecita t WHERE t.iddetcita = :iddetcita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbDetallecita.findByFechacita", query = "SELECT t FROM TbDetallecita t WHERE t.fechacita = :fechacita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbDetallecita.findByHoracita", query = "SELECT t FROM TbDetallecita t WHERE t.horacita = :horacita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbDetallecita.findByEspaciomedcita", query = "SELECT t FROM TbDetallecita t WHERE t.espaciomedcita = :espaciomedcita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbDetallecita.findByEstadocita", query = "SELECT t FROM TbDetallecita t WHERE t.estadocita = :estadocita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    ,  @NamedQuery(name = "TbDetallecita.findByPaciente", query = "SELECT t FROM TbDetallecita t WHERE t.idpaciente.idpaciente = :idpaciente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))  
    , @NamedQuery(name = "TbDetallecita.findByMedicoFechaHora", query = "SELECT t FROM TbDetallecita t WHERE t.idmed.idmed = :idmedico" /*and t.fechacita = :fechaCita /*and t.horacita = :horaCita*/, hints = @QueryHint(name = "eclipselink.refresh", value = "true"))  
    , @NamedQuery(name = "TbDetallecita.findByMotivocita", query = "SELECT t FROM TbDetallecita t WHERE t.motivocita = :motivocita", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbDetallecita implements Serializable {

    @Basic(optional = false)
    @Column(name = "ESPACIOMEDCITA")
    private Integer espaciomedcita;
    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONCITA")
    private Integer versioncita;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_DETALLECITA_IDDETCITA_GENERATOR", sequenceName = "CLINICAUNA.SEQ_DETALLECITA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_DETALLECITA_IDDETCITA_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDDETCITA")
    private Long iddetcita;
    @Basic(optional = false)
    @Column(name = "FECHACITA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacita;
    @Basic(optional = false)
    @Column(name = "HORACITA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horacita;
    @Column(name = "ESTADOCITA")
    private String estadocita;
    @Column(name = "MOTIVOCITA")
    private String motivocita;
    @JoinColumn(name = "IDMED", referencedColumnName = "IDMED")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbMantmedicos idmed;
    @JoinColumn(name = "IDUS", referencedColumnName = "IDUS")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbMantusuarios idus;
    @JoinColumn(name = "IDPACIENTE", referencedColumnName = "IDPACIENTE")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbRegistropacientes idpaciente;

    public TbDetallecita() {
    }

    public TbDetallecita(Long iddetcita) {
        this.iddetcita = iddetcita;
    }

    public TbDetallecita(Long iddetcita, Date fechacita, Date horacita, Integer espaciomedcita) {
        this.iddetcita = iddetcita;
        this.fechacita = fechacita;
        this.horacita = horacita;
        this.espaciomedcita = espaciomedcita;
    }

    public TbDetallecita(DetallecitaDto detalle) {
        if (detalle.getIdpaciente() != null) {
            this.iddetcita = detalle.getIddetcita();
        }
        this.fechacita = Date.from(detalle.getFechacita().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        this.horacita = Date.from(detalle.getHoracita().atZone(ZoneId.systemDefault()).toInstant());
        this.estadocita = detalle.getEstadocita(); 
        this.motivocita = detalle.getMotivocita();
        this.espaciomedcita = detalle.getEspaciocita();
        this.idmed = new TbMantmedicos(detalle.getIdmed());
        this.idus = new TbMantusuarios(detalle.getIdus());
        this.idpaciente = new TbRegistropacientes(detalle.getIdpaciente());
    }

    public void actualizarDetCita(DetallecitaDto detalle) {
        this.iddetcita = detalle.getIddetcita();
        this.fechacita = Date.from(detalle.getFechacita().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        this.horacita = Date.from(detalle.getHoracita().atZone(ZoneId.systemDefault()).toInstant());
        this.estadocita = detalle.getEstadocita();
        this.motivocita = detalle.getMotivocita();
        this.espaciomedcita = detalle.getEspaciocita();
        this.idmed = new TbMantmedicos(detalle.getIdmed());
        this.idus = new TbMantusuarios(detalle.getIdus());
        this.idpaciente = new TbRegistropacientes(detalle.getIdpaciente());
    }

    public Long getIddetcita() {
        return iddetcita;
    }

    public void setIddetcita(Long iddetcita) {
        this.iddetcita = iddetcita;
    }

    public Date getFechacita() {
        return fechacita;
    }

    public void setFechacita(Date fechacita) {
        this.fechacita = fechacita;
    }

    public Date getHoracita() {
        return horacita;
    }

    public void setHoracita(Date horacita) {
        this.horacita = horacita;
    }

    public String getEstadocita() {
        return estadocita;
    }

    public void setEstadocita(String estadocita) {
        this.estadocita = estadocita;
    }

    public String getMotivocita() {
        return motivocita;
    }

    public void setMotivocita(String motivocita) {
        this.motivocita = motivocita;
    }

    public TbMantmedicos getIdmed() {
        return idmed;
    }

    public void setIdmed(TbMantmedicos idmed) {
        this.idmed = idmed;
    }

    public TbMantusuarios getIdus() {
        return idus;
    }

    public void setIdus(TbMantusuarios idus) {
        this.idus = idus;
    }

    public TbRegistropacientes getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(TbRegistropacientes idpaciente) {
        this.idpaciente = idpaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddetcita != null ? iddetcita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDetallecita)) {
            return false;
        }
        TbDetallecita other = (TbDetallecita) object;
        if ((this.iddetcita == null && other.iddetcita != null) || (this.iddetcita != null && !this.iddetcita.equals(other.iddetcita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbDetallecita[ iddetcita=" + iddetcita + " ]";
    }

    public Integer getEspaciomedcita() {
        return espaciomedcita;
    }

    public void setEspaciomedcita(Integer espaciomedcita) {
        this.espaciomedcita = espaciomedcita;
    }

    public Integer getVersioncita() {
        return versioncita;
    }

    public void setVersioncita(Integer versioncita) {
        this.versioncita = versioncita;
    }

}
