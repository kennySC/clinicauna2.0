// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kenneth Sibaja
 */

@XmlRootElement(name = "PersonaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonaDto {
    
    private Long idpers;
    private String nombrepers;
    private String apellidospersona;
    private String cedulapers;
    private String correopers;
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public PersonaDto(){
        
    }

    public PersonaDto(String nombrepers, String apellidospersona, String cedulapers, String correopers) {
        this.nombrepers = nombrepers;
        this.apellidospersona = apellidospersona;
        this.cedulapers = cedulapers;
        this.correopers = correopers;
    }
    
    
    
    public PersonaDto(TbPersona persona){
        this.idpers = persona.getIdpers();
        this.nombrepers = persona.getNombrepers();
        this.apellidospersona = persona.getApellidospersona();
        this.cedulapers = persona.getCedulapers();
        this.correopers = persona.getCorreopers();
    }

    public Long getIdpers() {
        return idpers;
    }

    public void setIdpers(Long idpers) {
        this.idpers = idpers;
    }
    
    public String getNombrepers() {
        return nombrepers;
    }

    public void setNombrepers(String nombrepers) {
        this.nombrepers = nombrepers;
    }

    public String getApellidospersona() {
        return apellidospersona;
    }

    public void setApellidospersona(String apellidospersona) {
        this.apellidospersona = apellidospersona;
    }

    public String getCedulapers() {
        return cedulapers;
    }

    public void setCedulapers(String cedulapers) {
        this.cedulapers = cedulapers;
    }

    public String getCorreopers() {
        return correopers;
    }

    public void setCorreopers(String correopers) {
        this.correopers = correopers;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "PersonaDto{" + "nombrepers=" + nombrepers + ", apellidospersona=" + apellidospersona +
                ", cedulapers=" + cedulapers + ", correopers=" + correopers + '}';
    }
    
    
    
}
