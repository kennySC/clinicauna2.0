package clinicaunaWS.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_DETALLEEXPEDIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDetalleexpediente.findAll", query = "SELECT t FROM TbDetalleexpediente t")
    , @NamedQuery(name = "TbDetalleexpediente.findByIddetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.iddetexp = :iddetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByFechadetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.fechadetexp = :fechadetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByHoradetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.horadetexp = :horadetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByPresiondetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.presiondetexp = :presiondetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByFreccarddetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.freccarddetexp = :freccarddetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByPesodetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.pesodetexp = :pesodetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByTalladetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.talladetexp = :talladetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByTemperaturadetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.temperaturadetexp = :temperaturadetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByImcdetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.imcdetexp = :imcdetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByMotivoconsultadetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.motivoconsultadetexp = :motivoconsultadetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByPlanatenciondetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.planatenciondetexp = :planatenciondetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByAnotacionesdetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.anotacionesdetexp = :anotacionesdetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByObservdetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.observdetexp = :observdetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByExamfisicodetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.examfisicodetexp = :examfisicodetexp")
    , @NamedQuery(name = "TbDetalleexpediente.findByIdExpediente", query = "SELECT t FROM TbDetalleexpediente t WHERE t.idencabexp.idencabexp = :idExpediente")
    , @NamedQuery(name = "TbDetalleexpediente.findByTratamientodetexp", query = "SELECT t FROM TbDetalleexpediente t WHERE t.tratamientodetexp = :tratamientodetexp")})
public class TbDetalleexpediente implements Serializable {

    @Basic(optional = false)
    @Column(name = "PRESIONDETEXP")
    private Double presiondetexp;
    @Basic(optional = false)
    @Column(name = "TEMPERATURADETEXP")
    private Double temperaturadetexp;
    @Basic(optional = false)
    @Column(name = "IMCDETEXP")
    private Double imcdetexp;
    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONDETEXP")
    private Integer versiondetexp;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_DETALLEEXPEDIENTE_IDDETEXP_GENERATOR", sequenceName = "CLINICAUNA.SEQ_DETALLEEXPEDIENTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_DETALLEEXPEDIENTE_IDDETEXP_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDDETEXP")
    private Long iddetexp;
    @Basic(optional = false)
    @Column(name = "FECHADETEXP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechadetexp;
    @Basic(optional = false)
    @Column(name = "HORADETEXP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horadetexp;
    @Basic(optional = false)
    @Column(name = "FRECCARDDETEXP")
    private String freccarddetexp;
    @Basic(optional = false)
    @Column(name = "PESODETEXP")
    private double pesodetexp;
    @Basic(optional = false)
    @Column(name = "TALLADETEXP")
    private String talladetexp;
    @Column(name = "MOTIVOCONSULTADETEXP")
    private String motivoconsultadetexp;
    @Column(name = "PLANATENCIONDETEXP")
    private String planatenciondetexp;
    @Column(name = "ANOTACIONESDETEXP")
    private String anotacionesdetexp;
    @Column(name = "OBSERVDETEXP")
    private String observdetexp;
    @Column(name = "EXAMFISICODETEXP")
    private String examfisicodetexp;
    @Column(name = "TRATAMIENTODETEXP")
    private String tratamientodetexp;
    @JoinColumn(name = "IDENCABEXP", referencedColumnName = "IDENCABEXP")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbEncabezadoexpediente idencabexp;

    public TbDetalleexpediente() {
    }

    public TbDetalleexpediente(Long iddetexp) {
        this.iddetexp = iddetexp;
    }

    public TbDetalleexpediente(DetalleexpedienteDto detExp) {
        if (detExp.getIddetexp() != null) {
            this.iddetexp = detExp.getIddetexp();
        }
        this.fechadetexp = Date.from(detExp.getFechadetexp().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        this.horadetexp = Date.from(detExp.getHoradetexp().atZone(ZoneId.systemDefault()).toInstant());
        this.presiondetexp = detExp.getPresiondetexp();
        this.freccarddetexp = detExp.getFreccarddetexp();
        this.pesodetexp = detExp.getPesodetexp();
        this.talladetexp = detExp.getTalladetexp();
        this.temperaturadetexp = detExp.getTemperaturadetexp();
        this.imcdetexp = detExp.getImcdetexp();
        this.motivoconsultadetexp = detExp.getMotivoconsultadetexp();
        this.planatenciondetexp = detExp.getPlanatenciondetexp();
        this.anotacionesdetexp = detExp.getPlanatenciondetexp();
        this.observdetexp = detExp.getObservdetexp();
        this.examfisicodetexp = detExp.getExamfisicodetexp();
        this.tratamientodetexp = detExp.getTratamientodetexp();
        this.idencabexp = new TbEncabezadoexpediente(detExp.getIdencabexp());
    }

    public TbDetalleexpediente(double presiondetexp, double temperaturadetexp, double imcdetexp, Long iddetexp, Date fechadetexp,
            Date horadetexp, String freccarddetexp, double pesodetexp, String talladetexp, String motivoconsultadetexp,
            String planatenciondetexp, String anotacionesdetexp, String observdetexp, String examfisicodetexp, String tratamientodetexp) {
        this.presiondetexp = presiondetexp;
        this.temperaturadetexp = temperaturadetexp;
        this.imcdetexp = imcdetexp;
        this.iddetexp = iddetexp;
        this.fechadetexp = fechadetexp;
        this.horadetexp = horadetexp;
        this.freccarddetexp = freccarddetexp;
        this.pesodetexp = pesodetexp;
        this.talladetexp = talladetexp;
        this.motivoconsultadetexp = motivoconsultadetexp;
        this.planatenciondetexp = planatenciondetexp;
        this.anotacionesdetexp = anotacionesdetexp;
        this.observdetexp = observdetexp;
        this.examfisicodetexp = examfisicodetexp;
        this.tratamientodetexp = tratamientodetexp;
    }

    public void actualizarDetExp(DetalleexpedienteDto detexpDto) {
        this.iddetexp = detexpDto.getIddetexp();
        this.fechadetexp = Date.from(detexpDto.getFechadetexp().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        this.horadetexp = Date.from(detexpDto.getHoradetexp().atZone(ZoneId.systemDefault()).toInstant());
        this.presiondetexp = detexpDto.getPresiondetexp();
        this.freccarddetexp = detexpDto.getFreccarddetexp();
        this.pesodetexp = detexpDto.getPesodetexp();
        this.talladetexp = detexpDto.getTalladetexp();
        this.temperaturadetexp = detexpDto.getTemperaturadetexp();
        this.imcdetexp = detexpDto.getImcdetexp();
        this.motivoconsultadetexp = detexpDto.getMotivoconsultadetexp();
        this.planatenciondetexp = detexpDto.getPlanatenciondetexp();
        this.anotacionesdetexp = detexpDto.getPlanatenciondetexp();
        this.observdetexp = detexpDto.getObservdetexp();
        this.examfisicodetexp = detexpDto.getExamfisicodetexp();
        this.tratamientodetexp = detexpDto.getTratamientodetexp();
        this.idencabexp = new TbEncabezadoexpediente(detexpDto.getIdencabexp());
    }

    public Long getIddetexp() {
        return iddetexp;
    }

    public void setIddetexp(Long iddetexp) {
        this.iddetexp = iddetexp;
    }

    public Date getFechadetexp() {
        return fechadetexp;
    }

    public void setFechadetexp(Date fechadetexp) {
        this.fechadetexp = fechadetexp;
    }

    public Date getHoradetexp() {
        return horadetexp;
    }

    public void setHoradetexp(Date horadetexp) {
        this.horadetexp = horadetexp;
    }

    public String getFreccarddetexp() {
        return freccarddetexp;
    }

    public void setFreccarddetexp(String freccarddetexp) {
        this.freccarddetexp = freccarddetexp;
    }

    public double getPesodetexp() {
        return pesodetexp;
    }

    public void setPesodetexp(double pesodetexp) {
        this.pesodetexp = pesodetexp;
    }

    public String getTalladetexp() {
        return talladetexp;
    }

    public void setTalladetexp(String talladetexp) {
        this.talladetexp = talladetexp;
    }

    public String getMotivoconsultadetexp() {
        return motivoconsultadetexp;
    }

    public void setMotivoconsultadetexp(String motivoconsultadetexp) {
        this.motivoconsultadetexp = motivoconsultadetexp;
    }

    public String getPlanatenciondetexp() {
        return planatenciondetexp;
    }

    public void setPlanatenciondetexp(String planatenciondetexp) {
        this.planatenciondetexp = planatenciondetexp;
    }

    public String getAnotacionesdetexp() {
        return anotacionesdetexp;
    }

    public void setAnotacionesdetexp(String anotacionesdetexp) {
        this.anotacionesdetexp = anotacionesdetexp;
    }

    public String getObservdetexp() {
        return observdetexp;
    }

    public void setObservdetexp(String observdetexp) {
        this.observdetexp = observdetexp;
    }

    public String getExamfisicodetexp() {
        return examfisicodetexp;
    }

    public void setExamfisicodetexp(String examfisicodetexp) {
        this.examfisicodetexp = examfisicodetexp;
    }

    public String getTratamientodetexp() {
        return tratamientodetexp;
    }

    public void setTratamientodetexp(String tratamientodetexp) {
        this.tratamientodetexp = tratamientodetexp;
    }

    public TbEncabezadoexpediente getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(TbEncabezadoexpediente idencabexp) {
        this.idencabexp = idencabexp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddetexp != null ? iddetexp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDetalleexpediente)) {
            return false;
        }
        TbDetalleexpediente other = (TbDetalleexpediente) object;
        if ((this.iddetexp == null && other.iddetexp != null) || (this.iddetexp != null && !this.iddetexp.equals(other.iddetexp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbDetalleexpediente[ iddetexp=" + iddetexp + " ]";
    }

    public Double getPresiondetexp() {
        return presiondetexp;
    }

    public void setPresiondetexp(Double presiondetexp) {
        this.presiondetexp = presiondetexp;
    }

    public Double getTemperaturadetexp() {
        return temperaturadetexp;
    }

    public void setTemperaturadetexp(Double temperaturadetexp) {
        this.temperaturadetexp = temperaturadetexp;
    }

    public Double getImcdetexp() {
        return imcdetexp;
    }

    public void setImcdetexp(Double imcdetexp) {
        this.imcdetexp = imcdetexp;
    }

    public Integer getVersiondetexp() {
        return versiondetexp;
    }

    public void setVersiondetexp(Integer versiondetexp) {
        this.versiondetexp = versiondetexp;
    }

}
