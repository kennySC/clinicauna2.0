// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import clinicaunaWS.util.LocalDateAdapter;
import clinicaunaWS.util.LocalDateTimeAdapter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "DetallecitaDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DetallecitaDto {
    
    private Long iddetcita;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fechacita;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime horacita;
    private String estadocita;
    private String motivocita;
    private Integer espaciocita;
    
    // FK //
    private MantmedicosDto idmed;
    private MantusuariosDto idus;
    private RegistropacientesDto idpaciente;
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public DetallecitaDto(){
        
    }
    
    public DetallecitaDto(TbDetallecita detcita){
        this.iddetcita = detcita.getIddetcita();
        this.fechacita = detcita.getFechacita().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        this.horacita = detcita.getHoracita().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.estadocita = detcita.getEstadocita();
        this.motivocita = detcita.getMotivocita();
        this.espaciocita = detcita.getEspaciomedcita();
        this.idmed = new MantmedicosDto(detcita.getIdmed());
        this.idus = new MantusuariosDto(detcita.getIdus());
        this.idpaciente = new RegistropacientesDto(detcita.getIdpaciente());
    }

    public Long getIddetcita() {
        return iddetcita;
    }

    public void setIddetcita(Long iddetcita) {
        this.iddetcita = iddetcita;
    }

    public LocalDate getFechacita() {
        return fechacita;
    }

    public void setFechacita(LocalDate fechacita) {
        this.fechacita = fechacita;
    }

    public LocalDateTime getHoracita() {
        return horacita;
    }

    public void setHoracita(LocalDateTime horacita) {
        this.horacita = horacita;
    }

    public String getEstadocita() {
        return estadocita;
    }

    public void setEstadocita(String estadocita) {
        this.estadocita = estadocita;
    }

    public String getMotivocita() {
        return motivocita;
    }

    public void setMotivocita(String motivocita) {
        this.motivocita = motivocita;
    }

    //------------------ FK ------------------//
    public MantmedicosDto getIdmed() {
        return idmed;
    }

    public void setIdmed(MantmedicosDto idmed) {
        this.idmed = idmed;
    }

    public MantusuariosDto getIdus() {
        return idus;
    }

    public void setIdus(MantusuariosDto idus) {
        this.idus = idus;
    }

    public RegistropacientesDto getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(RegistropacientesDto idpaciente) {
        this.idpaciente = idpaciente;
    }

    @Override
    public String toString() {
        return "DetallecitaDto{" + "iddetcita = " + iddetcita + ", fechacita = " + fechacita + 
                ", horacita = " + horacita + ", estadocita = " + estadocita + ", motivocita = " + motivocita +
                ", FK idmed = " + idmed + ", FK idus = " + idus + ", FK idp = " + idpaciente + '}';
    }

    public Integer getEspaciocita() {
        return espaciocita;
    }

    public void setEspaciocita(Integer espaciocita) {
        this.espaciocita = espaciocita;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    
    
}
