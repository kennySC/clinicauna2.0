/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_ANTECEDENTESHEREDOFAM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbAntecedentesheredofam.findAll", query = "SELECT t FROM TbAntecedentesheredofam t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbAntecedentesheredofam.findByIdantecedheredfam", query = "SELECT t FROM TbAntecedentesheredofam t WHERE t.idantecedheredfam = :idantecedheredfam", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbAntecedentesheredofam.findByEnfermheredanteced", query = "SELECT t FROM TbAntecedentesheredofam t WHERE t.enfermheredanteced = :enfermheredanteced", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbAntecedentesheredofam.findByIDExpediente", query = "SELECT t FROM TbAntecedentesheredofam t WHERE t.idencabexp.idencabexp = :idExpediente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbAntecedentesheredofam.findByParentescoanteced", query = "SELECT t FROM TbAntecedentesheredofam t WHERE t.parentescoanteced = :parentescoanteced", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbAntecedentesheredofam implements Serializable {

    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONANTECEDHEREDFAM")
    private Integer versionantecedheredfam;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_ANTECEDENTESHEREDOFAM_IDANTECEDHEREDFAM_GENERATOR", sequenceName = "CLINICAUNA.SEQ_ANTECEDENTESHEREDOFAM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_ANTECEDENTESHEREDOFAM_IDANTECEDHEREDFAM_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDANTECEDHEREDFAM")
    private Long idantecedheredfam;
    @Column(name = "ENFERMHEREDANTECED")
    private String enfermheredanteced;
    @Column(name = "PARENTESCOANTECED")
    private String parentescoanteced;
    @JoinColumn(name = "IDENCABEXP", referencedColumnName = "IDENCABEXP")
    @ManyToOne(fetch = FetchType.LAZY)
    private TbEncabezadoexpediente idencabexp;

    public TbAntecedentesheredofam() {
    }
    
    public TbAntecedentesheredofam(AntecedentesheredofamDto antecedhfDto){
        if(antecedhfDto.getIdantecedheredfam()!=null){
            this.idantecedheredfam = antecedhfDto.getIdantecedheredfam();
        }
        this.enfermheredanteced = antecedhfDto.getEnfermheredanteced();
        this.parentescoanteced = antecedhfDto.getParentescoanteced();
        this.idencabexp = new TbEncabezadoexpediente(antecedhfDto.getIdencabexp());
    }
    
    public TbAntecedentesheredofam(Long idantecedheredfam) {
        this.idantecedheredfam = idantecedheredfam;
    }

    public TbAntecedentesheredofam(Long idantecedheredfam, Integer versionantecedheredfam) {
        this.idantecedheredfam = idantecedheredfam;
        this.versionantecedheredfam = versionantecedheredfam;
    }

    public void actualizarAntecedenteHF(AntecedentesheredofamDto antecedHF){
        this.idantecedheredfam = antecedHF.getIdantecedheredfam();
        this.enfermheredanteced = antecedHF.getEnfermheredanteced();
        this.parentescoanteced = antecedHF.getParentescoanteced();
        this.idencabexp = new TbEncabezadoexpediente(antecedHF.getIdencabexp());
    }
    
    public Long getIdantecedheredfam() {
        return idantecedheredfam;
    }

    public void setIdantecedheredfam(Long idantecedheredfam) {
        this.idantecedheredfam = idantecedheredfam;
    }


    public String getEnfermheredanteced() {
        return enfermheredanteced;
    }

    public void setEnfermheredanteced(String enfermheredanteced) {
        this.enfermheredanteced = enfermheredanteced;
    }

    public String getParentescoanteced() {
        return parentescoanteced;
    }

    public void setParentescoanteced(String parentescoanteced) {
        this.parentescoanteced = parentescoanteced;
    }

    public TbEncabezadoexpediente getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(TbEncabezadoexpediente idencabexp) {
        this.idencabexp = idencabexp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idantecedheredfam != null ? idantecedheredfam.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbAntecedentesheredofam)) {
            return false;
        }
        TbAntecedentesheredofam other = (TbAntecedentesheredofam) object;
        if ((this.idantecedheredfam == null && other.idantecedheredfam != null) || (this.idantecedheredfam != null && !this.idantecedheredfam.equals(other.idantecedheredfam))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbAntecedentesheredofam[ idantecedheredfam=" + idantecedheredfam + " ]";
    }

    public Integer getVersionantecedheredfam() {
        return versionantecedheredfam;
    }

    public void setVersionantecedheredfam(Integer versionantecedheredfam) {
        this.versionantecedheredfam = versionantecedheredfam;
    }
    
}
