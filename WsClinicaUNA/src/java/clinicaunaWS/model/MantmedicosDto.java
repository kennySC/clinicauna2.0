// DTO DEL WEB SERVICE //
package clinicaunaWS.model;

import clinicaunaWS.util.LocalDateTimeAdapter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "MantmedicosDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MantmedicosDto {

    private Long idmed;
    private Long codigomed;
    private Long foliomed;
    private Integer carnemed;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime inijornadamed;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime finjornadamed;
    private Integer espaciosmed;
    private String estadomed;
//    private List<DetallecitaDto> tbDetallecitaList;

    private MantusuariosDto idus;  // FK
    private PersonaDto idPers;
    
    private boolean nuevo;

    // CONSTRUCTOR VACIO //
    public MantmedicosDto() {

    }

    public MantmedicosDto(Long idmed, Long codigomed, Long foliomed, Integer carnemed, LocalDateTime inijornadamed, LocalDateTime finjornadamed, Integer espaciosmed, String estadomed, MantusuariosDto idus, PersonaDto idPers) {
        this.idmed = idmed;
        this.codigomed = codigomed;
        this.foliomed = foliomed;
        this.carnemed = carnemed;
        this.inijornadamed = inijornadamed;
        this.finjornadamed = finjornadamed;
        this.espaciosmed = espaciosmed;
        this.estadomed = estadomed;
        this.idus = idus;
        this.idPers = idPers;
    }

    public MantmedicosDto(TbMantmedicos medico) {
        this.idmed = medico.getIdmed();
        this.codigomed = medico.getCodigomed();
        this.foliomed = medico.getFoliomed();
        this.carnemed = medico.getCarnemed();
        this.inijornadamed = medico.getInijornadamed().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.finjornadamed = medico.getFinjornadamed().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.espaciosmed = medico.getEspaciosmed();
        this.idus = new MantusuariosDto(medico.getIdus());   // FK
        this.idPers = new PersonaDto(medico.getIdpers());
    }

//    public void convertirListaDetalles(List<TbDetallecita> listDetalles){
//    this.tbDetallecitaList = new ArrayList<>();
//    if(listDetalles!=null && !listDetalles.isEmpty()){
//        for(TbDetallecita detcit : listDetalles){
//            DetallecitaDto detcitDto = new DetallecitaDto(detcit);
//            this.tbDetallecitaList.add(detcitDto);
//        }
//    }
//}
    public Long getIdmed() {
        return idmed;
    }

    public void setIdmed(Long idmed) {
        this.idmed = idmed;
    }

    public MantusuariosDto getIdus() {
        return idus;
    }

    public void setIdus(MantusuariosDto idus) {
        this.idus = idus;
    }

    public PersonaDto getIdPers() {
        return idPers;
    }

    public void setIdPers(PersonaDto idPers) {
        this.idPers = idPers;
    }

    public Long getCodigomed() {
        return codigomed;
    }

    public void setCodigomed(Long codigomed) {
        this.codigomed = codigomed;
    }

    public Long getFoliomed() {
        return foliomed;
    }

    public void setFoliomed(Long foliomed) {
        this.foliomed = foliomed;
    }

    public Integer getCarnemed() {
        return carnemed;
    }

    public void setCarnemed(Integer carnemed) {
        this.carnemed = carnemed;
    }

    public LocalDateTime getInijornadamed() {
        return inijornadamed;
    }

    public void setInijornadamed(LocalDateTime inijornadamed) {
        this.inijornadamed = inijornadamed;
    }

    public LocalDateTime getFinjornadamed() {
        return finjornadamed;
    }

    public void setFinjornadamed(LocalDateTime finjornadamed) {
        this.finjornadamed = finjornadamed;
    }

    public Integer getEspaciosmed() {
        return espaciosmed;
    }

    public void setEspaciosmed(Integer espaciosmed) {
        this.espaciosmed = espaciosmed;
    }

    public String getEstadomed() {
        return estadomed;
    }

    public void setEstadomed(String estadomed) {
        this.estadomed = estadomed;
    }

//    public List<DetallecitaDto> getTbDetallecitaList() {
//        return tbDetallecitaList;
//    }
//
//    public void setTbDetallecitaList(List<DetallecitaDto> tbDetallecitaList) {
//        this.tbDetallecitaList = tbDetallecitaList;
//    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }
    
    @Override
    public String toString() {
        return "MantmedicosDto{" + "idmed=" + idmed + ", codigomed=" + codigomed + ", foliomed=" + foliomed
                + ", carnemed=" + carnemed + ", inijornadamed=" + inijornadamed + ", finjornadamed=" + finjornadamed
                + ", espaciosmed=" + espaciosmed + ", estadomed=" + estadomed + ", idus=" + idus + '}';
    }

}
