package clinicaunaWS.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_EXAMENESPACIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbExamenespaciente.findAll", query = "SELECT t FROM TbExamenespaciente t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbExamenespaciente.findByIdexamp", query = "SELECT t FROM TbExamenespaciente t WHERE t.idexamp = :idexamp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbExamenespaciente.findByNombreexamp", query = "SELECT t FROM TbExamenespaciente t WHERE t.nombreexamp = :nombreexamp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbExamenespaciente.findByIDExpediente", query = "SELECT t FROM TbExamenespaciente t WHERE t.idencabexp.idencabexp = :idExpediente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbExamenespaciente.findByFechaexamp", query = "SELECT t FROM TbExamenespaciente t WHERE t.fechaexamp = :fechaexamp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbExamenespaciente.findByAnotacionesexamp", query = "SELECT t FROM TbExamenespaciente t WHERE t.anotacionesexamp = :anotacionesexamp", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbExamenespaciente implements Serializable {

    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONEXAMP")
    private Integer versionexamp;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_EXAMENESPACIENTE_IDEXAMP_GENERATOR", sequenceName = "CLINICAUNA.SEQ_EXAMENESPACIENTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_EXAMENESPACIENTE_IDEXAMP_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDEXAMP")
    private Long idexamp;
    @Column(name = "NOMBREEXAMP")
    private String nombreexamp;
    @Column(name = "FECHAEXAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaexamp;
    @Column(name = "ANOTACIONESEXAMP")
    private String anotacionesexamp;
    @JoinColumn(name = "IDENCABEXP", referencedColumnName = "IDENCABEXP")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbEncabezadoexpediente idencabexp;

    public TbExamenespaciente() {
    }

    public TbExamenespaciente(ExamenespacienteDto examen) {

        if (examen.getIdexamp() != null) {
            this.idexamp = examen.getIdexamp();
        }
        this.nombreexamp = examen.getNombreexamp();
        this.fechaexamp = examen.getFechaexamp();
        this.anotacionesexamp = examen.getAnotacionesexamp();
    }

    public void actualizarExamenPac(ExamenespacienteDto examen) {
        this.idexamp = examen.getIdexamp();
        this.nombreexamp = examen.getNombreexamp();
        this.fechaexamp = examen.getFechaexamp();
        this.anotacionesexamp = examen.getAnotacionesexamp();
        this.idencabexp = new TbEncabezadoexpediente(examen.getIdencexp());
    }

    public TbExamenespaciente(Long idexamp) {
        this.idexamp = idexamp;
    }

    public Long getIdexamp() {
        return idexamp;
    }

    public void setIdexamp(Long idexamp) {
        this.idexamp = idexamp;
    }

    public String getNombreexamp() {
        return nombreexamp;
    }

    public void setNombreexamp(String nombreexamp) {
        this.nombreexamp = nombreexamp;
    }

    public Date getFechaexamp() {
        return fechaexamp;
    }

    public void setFechaexamp(Date fechaexamp) {
        this.fechaexamp = fechaexamp;
    }

    public String getAnotacionesexamp() {
        return anotacionesexamp;
    }

    public void setAnotacionesexamp(String anotacionesexamp) {
        this.anotacionesexamp = anotacionesexamp;
    }

    public TbEncabezadoexpediente getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(TbEncabezadoexpediente idencabexp) {
        this.idencabexp = idencabexp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idexamp != null ? idexamp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbExamenespaciente)) {
            return false;
        }
        TbExamenespaciente other = (TbExamenespaciente) object;
        if ((this.idexamp == null && other.idexamp != null) || (this.idexamp != null && !this.idexamp.equals(other.idexamp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbExamenespaciente[ idexamp=" + idexamp + " ]";
    }

    public Integer getVersionexamp() {
        return versionexamp;
    }

    public void setVersionexamp(Integer versionexamp) {
        this.versionexamp = versionexamp;
    }

}
