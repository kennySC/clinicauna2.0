// DTO DEL WEBSERVICE //
package clinicaunaWS.model;

import clinicaunaWS.util.LocalDateAdapter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "RegistropacientesDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RegistropacientesDto {

    private Long idpaciente;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate fechanacimientop;
    private String generop;
    private PersonaDto idpers;
    private String telefonoPaciente;    
//    private Boolean nuevo;

    // CONSTRUCTOR VACIO //
    public RegistropacientesDto() {

    }

    public RegistropacientesDto(LocalDate fechanacimientop, String generop, PersonaDto idpers, String numtelefpaciente) {
        this.fechanacimientop = fechanacimientop;
        this.generop = generop;
        this.idpers = idpers;
        this.telefonoPaciente = numtelefpaciente;
    }

    public RegistropacientesDto(TbRegistropacientes paciente) {
        this.idpaciente = paciente.getIdpaciente();
        this.fechanacimientop = paciente.getFechanacimpaciente().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        this.generop = paciente.getGeneropaciente();
        this.idpers = new PersonaDto(paciente.getIdpers());
        this.telefonoPaciente = paciente.getNumtelefpaciente();
    }

    public Long getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(Long idp) {
        this.idpaciente = idp;
    }

    public PersonaDto getIdpers() {
        return idpers;
    }

    public void setIdpers(PersonaDto idpers) {
        this.idpers = idpers;
    }

    public LocalDate getFechanacimientop() {
        return fechanacimientop;
    }

    public void setFechanacimientop(LocalDate fechanacimientop) {
        this.fechanacimientop = fechanacimientop;
    }

    public String getGenerop() {
        return generop;
    }

    public void setGenerop(String generop) {
        this.generop = generop;
    }

    public String getTelefonoPaciente() {
        return telefonoPaciente;
    }

    public void setTelefonoPaciente(String telefonoPaciente) {
        this.telefonoPaciente = telefonoPaciente;
    }

//    public Boolean getNuevo() {
//        return nuevo;
//    }
//
//    public void setNuevo(Boolean nuevo) {
//        this.nuevo = nuevo;
//    }

    
    
    @Override
    public String toString() {
        return "RegistropacientesDto{" + "idpaciente=" + idpaciente + ", fechanacimientop=" + fechanacimientop
                + ", generop=" + generop + ", idpers=" + idpers + '}';
    }

}
