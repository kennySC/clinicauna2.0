// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "ExamenespacienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExamenespacienteDto {
    
    private Long idexamp;
    private String nombreexamp;
    private Date fechaexamp;
    private String anotacionesexamp;
    private RegistropacientesDto idpaciente;
    private DetalleexpedienteDto iddetexp;
    private EncabezadoexpedienteDto idencexp; // FK
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public ExamenespacienteDto(){
        
    }

    public ExamenespacienteDto(Long idexamp, String nombreexamp, Date fechaexamp, String anotacionesexamp, RegistropacientesDto idpaciente, DetalleexpedienteDto iddetexp, EncabezadoexpedienteDto idencexp) {
        this.idexamp = idexamp;
        this.nombreexamp = nombreexamp;
        this.fechaexamp = fechaexamp;
        this.anotacionesexamp = anotacionesexamp;
        this.idpaciente = idpaciente;
        this.iddetexp = iddetexp;
        this.idencexp = idencexp;
    }
    
    public ExamenespacienteDto(TbExamenespaciente exampaciente){
        this.idexamp = exampaciente.getIdexamp();
        this.nombreexamp = exampaciente.getNombreexamp();
        this.fechaexamp = exampaciente.getFechaexamp();
        this.anotacionesexamp = exampaciente.getAnotacionesexamp();
        this.idencexp = new EncabezadoexpedienteDto(exampaciente.getIdencabexp());
    }

    public Long getIdexamp() {
        return idexamp;
    }

    public void setIdexamp(Long idexamp) {
        this.idexamp = idexamp;
    }

    public String getNombreexamp() {
        return nombreexamp;
    }

    public void setNombreexamp(String nombreexamp) {
        this.nombreexamp = nombreexamp;
    }

    public Date getFechaexamp() {
        return fechaexamp;
    }

    public void setFechaexamp(Date fechaexamp) {
        this.fechaexamp = fechaexamp;
    }

    public String getAnotacionesexamp() {
        return anotacionesexamp;
    }

    public void setAnotacionesexamp(String anotacionesexamp) {
        this.anotacionesexamp = anotacionesexamp;
    }

    public DetalleexpedienteDto getIddetexp() {
        return iddetexp;
    }

    public void setIddetexp(DetalleexpedienteDto iddetexp) {
        this.iddetexp= iddetexp;
    }

    public RegistropacientesDto getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(RegistropacientesDto idpaciente) {
        this.idpaciente = idpaciente;
    }

    public EncabezadoexpedienteDto getIdencexp() {
        return idencexp;
    }

    public void setIdencexp(EncabezadoexpedienteDto idencexp) {
        this.idencexp = idencexp;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    @Override
    public String toString() {
        return "ExamenespacienteDto{" + "idexamp=" + idexamp + ", nombreexamp=" + nombreexamp + 
                ", fechaexamp=" + fechaexamp + ", anotacionesexamp=" + anotacionesexamp + ", iddetexp=" + idencexp + '}';
    }
    
    
    
}
