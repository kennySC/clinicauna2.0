//   DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "DetalleexpedienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DetalleexpedienteDto {
    
    private Long iddetexp;
    private LocalDate fechadetexp;
    private LocalDateTime horadetexp;
    private double presiondetexp;
    private String freccarddetexp;
    private double pesodetexp;
    private String talladetexp;
    private double temperaturadetexp;
    private double imcdetexp;
    private String motivoconsultadetexp;
    private String planatenciondetexp;
    private String anotacionesdetexp;
    private String observdetexp;
    private String examfisicodetexp;
    private String tratamientodetexp;
    
    private EncabezadoexpedienteDto idencabexp;    // FK 
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public DetalleexpedienteDto(){
        
    }
    
    public DetalleexpedienteDto(TbDetalleexpediente detexp){
        if(detexp.getIddetexp()!=null){
        this.iddetexp = detexp.getIddetexp();
        }
        this.fechadetexp = detexp.getFechadetexp().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        this.horadetexp = detexp.getHoradetexp().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        this.presiondetexp = detexp.getPresiondetexp();
        this.freccarddetexp = detexp.getFreccarddetexp();
        this.pesodetexp = detexp.getPesodetexp();
        this.talladetexp = detexp.getTalladetexp();
        this.temperaturadetexp = detexp.getTemperaturadetexp();
        this.imcdetexp = detexp.getImcdetexp();
        this.motivoconsultadetexp = detexp.getMotivoconsultadetexp();
        this.planatenciondetexp = detexp.getPlanatenciondetexp();
        this.anotacionesdetexp = detexp.getAnotacionesdetexp();
        this.observdetexp = detexp.getObservdetexp();
        this.examfisicodetexp = detexp.getExamfisicodetexp();
        this.tratamientodetexp = detexp.getTratamientodetexp();
        this.idencabexp = new EncabezadoexpedienteDto(detexp.getIdencabexp());
    }
    
    public Long getIddetexp() {
        return iddetexp;
    }

    public void setIddetexp(Long iddetexp) {
        this.iddetexp = iddetexp;
    }

    public LocalDate getFechadetexp() {
        return fechadetexp;
    }

    public void setFechadetexp(LocalDate fechadetexp) {
        this.fechadetexp = fechadetexp;
    }

    public LocalDateTime getHoradetexp() {
        return horadetexp;
    }

    public void setHoradetexp(LocalDateTime horadetexp) {
        this.horadetexp = horadetexp;
    }

    public double getPresiondetexp() {
        return presiondetexp;
    }

    public void setPresiondetexp(double presiondetexp) {
        this.presiondetexp = presiondetexp;
    }

    public String getFreccarddetexp() {
        return freccarddetexp;
    }

    public void setFreccarddetexp(String freccarddetexp) {
        this.freccarddetexp = freccarddetexp;
    }

    public double getPesodetexp() {
        return pesodetexp;
    }

    public void setPesodetexp(double pesodetexp) {
        this.pesodetexp = pesodetexp;
    }

    public String getTalladetexp() {
        return talladetexp;
    }

    public void setTalladetexp(String talladetexp) {
        this.talladetexp = talladetexp;
    }

    public double getTemperaturadetexp() {
        return temperaturadetexp;
    }

    public void setTemperaturadetexp(double temperaturadetexp) {
        this.temperaturadetexp = temperaturadetexp;
    }

    public double getImcdetexp() {
        return imcdetexp;
    }

    public void setImcdetexp(double imcdetexp) {
        this.imcdetexp = imcdetexp;
    }

    public String getMotivoconsultadetexp() {
        return motivoconsultadetexp;
    }

    public void setMotivoconsultadetexp(String motivoconsultadetexp) {
        this.motivoconsultadetexp = motivoconsultadetexp;
    }

    public String getPlanatenciondetexp() {
        return planatenciondetexp;
    }

    public void setPlanatenciondetexp(String planatenciondetexp) {
        this.planatenciondetexp = planatenciondetexp;
    }

    public String getAnotacionesdetexp() {
        return anotacionesdetexp;
    }

    public void setAnotacionesdetexp(String anotacionesdetexp) {
        this.anotacionesdetexp = anotacionesdetexp;
    }

    public String getObservdetexp() {
        return observdetexp;
    }

    public void setObservdetexp(String observdetexp) {
        this.observdetexp = observdetexp;
    }

    public String getExamfisicodetexp() {
        return examfisicodetexp;
    }

    public void setExamfisicodetexp(String examfisicodetexp) {
        this.examfisicodetexp = examfisicodetexp;
    }

    public String getTratamientodetexp() {
        return tratamientodetexp;
    }

    public void setTratamientodetexp(String tratamientodetexp) {
        this.tratamientodetexp = tratamientodetexp;
    }
    
    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto idencabexp) {
        this.idencabexp = idencabexp;
    }

    @Override
    public String toString() {
        return "DetalleexpedienteDto{" + "iddetexp=" + iddetexp + ", fechadetexp=" + fechadetexp + 
                ", horadetexp=" + horadetexp + ", presiondetexp=" + presiondetexp + ", freccarddetexp=" +
                freccarddetexp + ", pesodetexp=" + pesodetexp + ", talladetexp=" + talladetexp + ", temperaturadetexp=" + 
                temperaturadetexp + ", imcdetexp=" + imcdetexp + ", motivoconsultadetexp=" + motivoconsultadetexp + 
                ", planatenciondetexp=" + planatenciondetexp + ", anotacionesdetexp=" + anotacionesdetexp + ", observdetexp=" +
                observdetexp + ", examfisicodetexp=" + examfisicodetexp + ", tratamientodetexp=" + tratamientodetexp + 
                ", idencabexp=" + idencabexp + '}';
    }
    
    
    
}
