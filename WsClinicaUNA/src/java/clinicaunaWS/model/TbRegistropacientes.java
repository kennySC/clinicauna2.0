package clinicaunaWS.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_REGISTROPACIENTES", schema = "CLINICAUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRegistropacientes.findAll", query = "SELECT t FROM TbRegistropacientes t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbRegistropacientes.findByIdpaciente", query = "SELECT t FROM TbRegistropacientes t WHERE t.idpaciente = :idpaciente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbRegistropacientes.findByFechanacimpaciente", query = "SELECT t FROM TbRegistropacientes t WHERE t.fechanacimpaciente = :fechanacimpaciente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbRegistropacientes.findByGeneropaciente", query = "SELECT t FROM TbRegistropacientes t WHERE t.generopaciente = :generopaciente", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbRegistropacientes.findByIdPers_nom_ape_ced", query = "SELECT t FROM TbRegistropacientes t WHERE UPPER(t.idpers.nombrepers) like UPPER(:nombre) and UPPER(t.idpers.cedulapers) like UPPER(:cedula) and UPPER(t.idpers.apellidospersona) like UPPER(:apellidos)", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbRegistropacientes implements Serializable {
    
    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONPACIENTE")
    private Integer versionpaciente;
    @Basic(optional = false)
    @Column(name = "NUMTELEFPACIENTE")
    private String numtelefpaciente;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "REG_PACIENTES_ID_GENERATOR", sequenceName = "CLINICAUNA.SEQ_REGISTROPACIENTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REG_PACIENTES_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDPACIENTE")
    private Long idpaciente;
    @Basic(optional = false)
    @Column(name = "FECHANACIMPACIENTE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechanacimpaciente;
    @Basic(optional = false)
    @Column(name = "GENEROPACIENTE")
    private String generopaciente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpaciente", fetch = FetchType.LAZY)
    private List<TbDetallecita> tbDetallecitaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpaciente", fetch = FetchType.LAZY)
    private List<TbEncabezadoexpediente> tbEncabezadoexpedienteList;
    @JoinColumn(name = "IDPERS", referencedColumnName = "IDPERS")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbPersona idpers;

    public TbRegistropacientes() {
    }

    public TbRegistropacientes(Long idpaciente) {
        this.idpaciente = idpaciente;
    }

    public TbRegistropacientes(Long idpaciente, String telefpaciente, Date fechanacimpaciente, String generopaciente) {
        this.idpaciente = idpaciente;
        this.fechanacimpaciente = fechanacimpaciente;
        this.generopaciente = generopaciente;
    }

    public TbRegistropacientes(RegistropacientesDto paci) {
        if (paci.getIdpaciente() != null) {
            this.idpaciente = paci.getIdpaciente();
        }
        this.fechanacimpaciente = Date.from(paci.getFechanacimientop().atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
        this.generopaciente = paci.getGenerop();
        this.numtelefpaciente = paci.getTelefonoPaciente();
        this.idpers = new TbPersona(paci.getIdpers());
    }

    public void actualizarPaciente(RegistropacientesDto paci) {
        this.idpaciente = paci.getIdpaciente();
        this.idpers = new TbPersona(paci.getIdpers());
        this.generopaciente = paci.getGenerop();
        this.numtelefpaciente = paci.getTelefonoPaciente();
        this.fechanacimpaciente = Date.from(paci.getFechanacimientop().atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public Long getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(Long idpaciente) {
        this.idpaciente = idpaciente;
    }

    public Date getFechanacimpaciente() {
        return fechanacimpaciente;
    }

    public void setFechanacimpaciente(Date fechanacimpaciente) {
        this.fechanacimpaciente = fechanacimpaciente;
    }

    public String getGeneropaciente() {
        return generopaciente;
    }

    public void setGeneropaciente(String generopaciente) {
        this.generopaciente = generopaciente;
    }

    @XmlTransient
    public List<TbDetallecita> getTbDetallecitaList() {
        return tbDetallecitaList;
    }

    public void setTbDetallecitaList(List<TbDetallecita> tbDetallecitaList) {
        this.tbDetallecitaList = tbDetallecitaList;
    }

    @XmlTransient
    public List<TbEncabezadoexpediente> getTbEncabezadoexpedienteList() {
        return tbEncabezadoexpedienteList;
    }

    public void setTbEncabezadoexpedienteList(List<TbEncabezadoexpediente> tbEncabezadoexpedienteList) {
        this.tbEncabezadoexpedienteList = tbEncabezadoexpedienteList;
    }

    public TbPersona getIdpers() {
        return idpers;
    }

    public void setIdpers(TbPersona idpers) {
        this.idpers = idpers;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpaciente != null ? idpaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRegistropacientes)) {
            return false;
        }
        TbRegistropacientes other = (TbRegistropacientes) object;
        if ((this.idpaciente == null && other.idpaciente != null) || (this.idpaciente != null && !this.idpaciente.equals(other.idpaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbRegistropacientes[ idpaciente=" + idpaciente + " ]";
    }
    
    public String getNumtelefpaciente() {
        return numtelefpaciente;
    }

    public void setNumtelefpaciente(String numtelefpaciente) {
        this.numtelefpaciente = numtelefpaciente;
    }

    public Integer getVersionpaciente() {
        return versionpaciente;
    }

    public void setVersionpaciente(Integer versionpaciente) {
        this.versionpaciente = versionpaciente;
    }

}
