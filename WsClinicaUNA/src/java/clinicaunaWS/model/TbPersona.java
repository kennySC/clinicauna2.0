
package clinicaunaWS.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_PERSONA", schema = "CLINICAUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPersona.findAll", query = "SELECT t FROM TbPersona t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByIdpers", query = "SELECT t FROM TbPersona t WHERE t.idpers = :idpers", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByNombrepers", query = "SELECT t FROM TbPersona t WHERE t.nombrepers = :nombrepers", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByApellidospersona", query = "SELECT t FROM TbPersona t WHERE t.apellidospersona = :apellidospersona", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByCorreopers", query = "SELECT t FROM TbPersona t WHERE t.correopers = :correopers", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByCedulaNombrePapellido", query = "SELECT t FROM TbPersona t WHERE UPPER(t.nombrepers) like UPPER(:nombre) and UPPER(t.cedulapers) like UPPER(:cedula) and UPPER(t.apellidospersona) like UPPER(:pApellido)", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbPersona.findByCedulapers", query = "SELECT t FROM TbPersona t WHERE t.cedulapers = :cedulapers", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))})
public class TbPersona implements Serializable {

    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONPERS")
    private Integer versionpers;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_PERSONA_IDPERS_GENERATOR", sequenceName = "CLINICAUNA.SEQ_PERSONA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_PERSONA_IDPERS_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDPERS")
    private Long idpers;
    @Basic(optional = false)
    @Column(name = "NOMBREPERS")
    private String nombrepers;
    @Basic(optional = false)
    @Column(name = "APELLIDOSPERSONA")
    private String apellidospersona;
    @Basic(optional = false)
    @Column(name = "CORREOPERS")
    private String correopers;
    @Basic(optional = false)
    @Column(name = "CEDULAPERS")
    private String cedulapers;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpers", fetch = FetchType.LAZY)
    private List<TbMantusuarios> tbMantusuariosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpers", fetch = FetchType.LAZY)
    private List<TbMantmedicos> tbMantmedicosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpers", fetch = FetchType.LAZY)
    private List<TbRegistropacientes> tbRegistropacientesList;

    public TbPersona() {
    }

    public TbPersona(Long idpers) {
        this.idpers = idpers;
    }

    public TbPersona(Long idpers, String nombrepers, String apellidospersona, String correopers, String cedulapers) {
        this.idpers = idpers;
        this.nombrepers = nombrepers;
        this.apellidospersona = apellidospersona;
        this.correopers = correopers;
        this.cedulapers = cedulapers;
    }

    public TbPersona(PersonaDto persona) {
        if(persona.getIdpers()!=null){
        this.idpers = persona.getIdpers();
        }
        this.nombrepers = persona.getNombrepers();
        this.apellidospersona = persona.getApellidospersona();
        this.correopers = persona.getCorreopers();
        this.cedulapers = persona.getCedulapers();
    }
    
    public void actualizarPersona(PersonaDto pers){
        this.nombrepers = pers.getNombrepers();
        this.apellidospersona = pers.getApellidospersona();
        this.correopers = pers.getCorreopers();
        this.cedulapers = pers.getCedulapers();
    }

    public Long getIdpers() {
        return idpers;
    }

    public void setIdpers(Long idpers) {
        this.idpers = idpers;
    }

    public String getNombrepers() {
        return nombrepers;
    }

    public void setNombrepers(String nombrepers) {
        this.nombrepers = nombrepers;
    }

    public String getApellidospersona() {
        return apellidospersona;
    }

    public void setApellidospersona(String apellidospersona) {
        this.apellidospersona = apellidospersona;
    }

    public String getCorreopers() {
        return correopers;
    }

    public void setCorreopers(String correopers) {
        this.correopers = correopers;
    }

    public String getCedulapers() {
        return cedulapers;
    }

    public void setCedulapers(String cedulapers) {
        this.cedulapers = cedulapers;
    }

    @XmlTransient
    public List<TbMantusuarios> getTbMantusuariosList() {
        return tbMantusuariosList;
    }

    public void setTbMantusuariosList(List<TbMantusuarios> tbMantusuariosList) {
        this.tbMantusuariosList = tbMantusuariosList;
    }

    @XmlTransient
    public List<TbMantmedicos> getTbMantmedicosList() {
        return tbMantmedicosList;
    }

    public void setTbMantmedicosList(List<TbMantmedicos> tbMantmedicosList) {
        this.tbMantmedicosList = tbMantmedicosList;
    }

    @XmlTransient
    public List<TbRegistropacientes> getTbRegistropacientesList() {
        return tbRegistropacientesList;
    }

    public void setTbRegistropacientesList(List<TbRegistropacientes> tbRegistropacientesList) {
        this.tbRegistropacientesList = tbRegistropacientesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpers != null ? idpers.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPersona)) {
            return false;
        }
        TbPersona other = (TbPersona) object;
        if ((this.idpers == null && other.idpers != null) || (this.idpers != null && !this.idpers.equals(other.idpers))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbPersona[ idpers=" + idpers + " ]";
    }

    public Integer getVersionpers() {
        return versionpers;
    }

    public void setVersionpers(Integer versionpers) {
        this.versionpers = versionpers;
    }
    
}
