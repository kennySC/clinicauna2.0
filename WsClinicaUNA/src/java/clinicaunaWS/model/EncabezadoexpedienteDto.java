// DTO DEL WEBSERVICE //

package clinicaunaWS.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matthew Miranda
 */

@XmlRootElement(name = "EncabezadoexpedienteDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class EncabezadoexpedienteDto {
    
    private Long idencabexp;
    private Integer hospitalizacionesencabexp;
    private Integer operacionesencabexp;
    private String alergiasencabexp;
    private String patologicoencabexp;
    
    private RegistropacientesDto idpaciente;  // FK
//    private List<DetalleexpedienteDto> tbDetalleexpedienteList;
//    private List<ExamenespacienteDto> tbExamenespacienteList;
    
    private boolean nuevo;
    
    // CONSTRUCTOR VACIO //
    public EncabezadoexpedienteDto(){
        
    }
    
    public EncabezadoexpedienteDto(TbEncabezadoexpediente encabexp){
        this.idencabexp = encabexp.getIdencabexp();
        this.hospitalizacionesencabexp = encabexp.getHospitalizacionesencabexp();
        this.operacionesencabexp = encabexp.getOperacionesencabexp();
        this.alergiasencabexp = encabexp.getAlergiasencabexp();
        this.patologicoencabexp = encabexp.getPatologicoencabexp();
        this.idpaciente = new RegistropacientesDto(encabexp.getIdpaciente());
        
    }

    public Long getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(Long idencabexp) {
        this.idencabexp = idencabexp;
    }
    
    public Integer getHospitalizacionesencabexp() {
        return hospitalizacionesencabexp;
    }

    public void setHospitalizacionesencabexp(Integer hospitalizacionesencabexp) {
        this.hospitalizacionesencabexp = hospitalizacionesencabexp;
    }

    public Integer getOperacionesencabexp() {
        return operacionesencabexp;
    }

    public void setOperacionesencabexp(Integer operacionesencabexp) {
        this.operacionesencabexp = operacionesencabexp;
    }

    public String getAlergiasencabexp() {
        return alergiasencabexp;
    }

    public void setAlergiasencabexp(String alergiasencabexp) {
        this.alergiasencabexp = alergiasencabexp;
    }

    public String getPatologicoencabexp() {
        return patologicoencabexp;
    }

    public void setPatologicoencabexp(String patologicoencabexp) {
        this.patologicoencabexp = patologicoencabexp;
    }

    public RegistropacientesDto getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(RegistropacientesDto idpaciente) {
        this.idpaciente = idpaciente;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    @Override
    public String toString() {
        return "EncabezadoexpedienteDto{" + "idencabexp=" + idencabexp + ", hospitalizacionesencabexp=" + 
                hospitalizacionesencabexp + ", operacionesencabexp=" + operacionesencabexp + ", alergiasencabexp=" + 
                alergiasencabexp + ", enfermheredencabexp=" + 
                patologicoencabexp + ", idp=" + idpaciente + '}';
    }
    
    
    
}
