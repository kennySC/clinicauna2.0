
package clinicaunaWS.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kenneth Sibaja
 */
@Entity
@Table(name = "TB_MANTUSUARIOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMantusuarios.findAll", query = "SELECT t FROM TbMantusuarios t", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByIdus", query = "SELECT t FROM TbMantusuarios t WHERE t.idus = :idus", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByTipous", query = "SELECT t FROM TbMantusuarios t WHERE t.tipous = :tipous", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByUsernameus", query = "SELECT t FROM TbMantusuarios t WHERE t.usernameus = :usernameus", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByPasswordus", query = "SELECT t FROM TbMantusuarios t WHERE t.passwordus = :passwordus", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByEstadous", query = "SELECT t FROM TbMantusuarios t WHERE t.estadous = :estadous", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByIdiomaus", query = "SELECT t FROM TbMantusuarios t WHERE t.idiomaus = :idiomaus", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByUsuPass", query = "SELECT t FROM TbMantusuarios t WHERE t.usernameus = :usernameus and t.passwordus = :passwordus", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
    , @NamedQuery(name = "TbMantusuarios.findByIdPers_nom_ape_ced", query = "SELECT t FROM TbMantusuarios t WHERE UPPER(t.idpers.nombrepers) like UPPER(:nombre) and UPPER(t.idpers.cedulapers) like UPPER(:cedula) and UPPER(t.idpers.apellidospersona) like UPPER(:apellidos)", hints = @QueryHint(name = "eclipselink.refresh", value = "true"))
})
public class TbMantusuarios implements Serializable {

    @Basic(optional = false)
    @Version
    @Column(name = "VERSIONUS")
    private Integer versionus;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "TB_USUARIOS_IDUS_GENERATOR", sequenceName = "CLINICAUNA.SEQ_MANTUSUARIOS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_USUARIOS_IDUS_GENERATOR")
    @Basic(optional = false)
    @Column(name = "IDUS")
    private Long idus;
    @Basic(optional = false)
    @Column(name = "TIPOUS")
    private String tipous;
    @Basic(optional = false)
    @Column(name = "USERNAMEUS")
    private String usernameus;
    @Basic(optional = false)
    @Column(name = "PASSWORDUS")
    private String passwordus;
    @Basic(optional = false)
    @Column(name = "ESTADOUS")
    private String estadous;
    @Column(name = "IDIOMAUS")
    private String idiomaus;
    @JoinColumn(name = "IDPERS", referencedColumnName = "IDPERS")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbPersona idpers;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idus", fetch = FetchType.LAZY)
    private List<TbDetallecita> tbDetallecitaList;
    @OneToMany(mappedBy = "idus", fetch = FetchType.LAZY)
    private List<TbMantmedicos> tbMantmedicosList;

    public TbMantusuarios() {
    }

    public TbMantusuarios(Long idus) {
        this.idus = idus;
    }

    public TbMantusuarios(Long idus, String tipous, String usernameus, String passwordus, String estadous) {
        this.idus = idus;
        this.tipous = tipous;
        this.usernameus = usernameus;
        this.passwordus = passwordus;
        this.estadous = estadous;
    }

    public TbMantusuarios(MantusuariosDto mantUs) {
        if(mantUs.getIdUs()!=null){       
            this.idus = mantUs.getIdUs();
       
        }
        this.tipous = mantUs.getTipous();
        this.usernameus = mantUs.getUsernameus();
        this.passwordus = mantUs.getPasswordus();
        this.estadous = mantUs.getEstadous();
        this.idiomaus = mantUs.getIdiomaus();
        this.idpers = new TbPersona(mantUs.getIdpers());
    }
    
    public void actualizarUsuario(MantusuariosDto usDto){
        this.idus = usDto.getIdUs();
        this.tipous = usDto.getTipous();
        this.usernameus = usDto.getUsernameus();
        this.passwordus = usDto.getPasswordus();
        this.estadous = usDto.getEstadous();
        this.idiomaus = usDto.getIdiomaus();
        this.idpers = new TbPersona(usDto.getIdpers());
    }

    public Long getIdus() {
        return idus;
    }

    public void setIdus(Long idus) {
        this.idus = idus;
    }

    public String getTipous() {
        return tipous;
    }

    public void setTipous(String tipous) {
        this.tipous = tipous;
    }

    public String getUsernameus() {
        return usernameus;
    }

    public void setUsernameus(String usernameus) {
        this.usernameus = usernameus;
    }

    public String getPasswordus() {
        return passwordus;
    }

    public void setPasswordus(String passwordus) {
        this.passwordus = passwordus;
    }

    public String getEstadous() {
        return estadous;
    }

    public void setEstadous(String estadous) {
        this.estadous = estadous;
    }

    public String getIdiomaus() {
        return idiomaus;
    }

    public void setIdiomaus(String idiomaus) {
        this.idiomaus = idiomaus;
    }

    public TbPersona getIdpers() {
        return idpers;
    }

    public void setIdpers(TbPersona idpers) {
        this.idpers = idpers;
    }

    @XmlTransient
    public List<TbDetallecita> getTbDetallecitaList() {
        return tbDetallecitaList;
    }

    public void setTbDetallecitaList(List<TbDetallecita> tbDetallecitaList) {
        this.tbDetallecitaList = tbDetallecitaList;
    }

    @XmlTransient
    public List<TbMantmedicos> getTbMantmedicosList() {
        return tbMantmedicosList;
    }

    public void setTbMantmedicosList(List<TbMantmedicos> tbMantmedicosList) {
        this.tbMantmedicosList = tbMantmedicosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idus != null ? idus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMantusuarios)) {
            return false;
        }
        TbMantusuarios other = (TbMantusuarios) object;
        if ((this.idus == null && other.idus != null) || (this.idus != null && !this.idus.equals(other.idus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clinicaunaWS.model.TbMantusuarios[ idus=" + idus + " ]";
    }

    public Integer getVersionus() {
        return versionus;
    }

    public void setVersionus(Integer versionus) {
        this.versionus = versionus;
    }
    
}
