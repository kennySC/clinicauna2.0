/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.services;

import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Kenneth Sibaja
 */
@Stateless
@LocalBean
public class ReportesService {

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    @Resource(name = "jdbc/Clinica")
    DataSource ds;

    public Respuesta getReporteExpediente(Long idPaciente) {
        try {

            Connection conn = ds.getConnection();
            
            JasperReport rep = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("../Reportes/reporteExpediente.jasper"));
            Map parametros = new HashMap();
            parametros.put("idpaciente", idPaciente);

            JasperPrint report = JasperFillManager.fillReport(rep, parametros, conn);

            System.out.println("llego al reporte");
            System.out.println(" Pages size: " + report.getPages().size());
            System.out.println(report.getName());
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "reporte", report);

        } catch (Exception ex) {
            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return new Respuesta(false, CodigoRespuesta.ERROR_ACCESO, "Error al guardar a la persona", "guardarPersona" + ex.getMessage());
        }
    }

    public Respuesta getReporteAgenda(Long idMedico, Date f1, Date f2) {
        try {

            Connection conn = ds.getConnection();

            System.out.println("llego al reporte");
            JasperReport rep = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("../Reportes/reporteAgendaRangoDias.jasper"));
            Map parametros = new HashMap();
            parametros.put("idMedico", idMedico);
            parametros.put("fecha1", f1);
            parametros.put("fecha2", f2);

            JasperPrint report = JasperFillManager.fillReport(rep, parametros, conn);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "reporte", report);

        } catch (Exception ex) {

            Logger.getLogger(ReportesService.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return new Respuesta(false, CodigoRespuesta.ERROR_ACCESO, "Error al guardar a la persona", "guardarPersona" + ex.getMessage());

        }
    }

}
