package clinicaunaWS.services;

import clinicaunaWS.model.PersonaDto;
import clinicaunaWS.model.TbPersona;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author DJork
 */
@Stateless
@LocalBean
public class PersonaService {

    private static final Logger LOG = Logger.getLogger(PersonaService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarPersona(PersonaDto persDto) {
        System.out.println("Llego al service");
        try {
            TbPersona persona;
            // PRIMERO CONSULTAMOS SI LA PERSONA YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if (persDto.getIdpers() != null && persDto.getIdpers() > 0) {
                persona = em.find(TbPersona.class, persDto.getIdpers());
                if (persona == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar la persona", "guardarPersona NoResultExcepton");
                }
                persona.actualizarPersona(persDto);
                persona = em.merge(persona);
            } // SI NO EXISTE LA PERSONA ENTONCES SE PROCEDE A CREAR UNA NUEVA //
            else {               
                persona = new TbPersona(persDto);
                em.persist(persona);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Persona", new PersonaDto(persona));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_ACCESO, "Error al guardar a la persona", "guardarPersona" + ex.getMessage());
        }
    }

    public Respuesta getPersona(Long id){
        try {
            Query qryActividad = em.createNamedQuery("TbPersona.findByIdpers", TbPersona.class);
            qryActividad.setParameter("idpers", id);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Persona", new PersonaDto((TbPersona) qryActividad.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe una persona con el ID ingresado.", "getPersona NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la persona.", "getPersona NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la persona.", "getPersona " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarPersona(Long id){
        try{
            TbPersona personaelim;
            if(id != null && id > 0){
                personaelim = em.find(TbPersona.class, id);
                if(personaelim == null){
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la persona a eliminar.", "eliminarPersona NoResultException");
                }
                em.remove(personaelim);
            }
            else{
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la persona a eliminar.", "eliminarPersona NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        }
        catch(Exception e){
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar a la persona porque se encuentra relacionada con un paciente, medico, o usuario.", "eliminarPersona " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la persona.", "eliminarPersona " + e.getMessage());
        }
    }
    
}
