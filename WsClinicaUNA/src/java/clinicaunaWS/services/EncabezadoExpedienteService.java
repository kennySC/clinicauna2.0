package clinicaunaWS.services;

import clinicaunaWS.model.EncabezadoexpedienteDto;
import clinicaunaWS.model.TbEncabezadoexpediente;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matthew Miranda
 */
@Stateless
@LocalBean
public class EncabezadoExpedienteService {

    private static final Logger LOG = Logger.getLogger(EncabezadoExpedienteService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarEncabExp(EncabezadoexpedienteDto encabexpDto) {

        try {
            TbEncabezadoexpediente encabexpediente;
            if (encabexpDto.getIdencabexp() != null && encabexpDto.getIdencabexp() > 0) {
                encabexpediente = em.find(TbEncabezadoexpediente.class, encabexpDto.getIdencabexp());
                if (encabexpediente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el encabezado de expediente", "guardarEncabExp NoResultExcepton");
                }
                encabexpediente.actualizarEncabExp(encabexpDto);
                encabexpediente = em.merge(encabexpediente);
            } else {
                encabexpediente = new TbEncabezadoexpediente(encabexpDto);
                em.persist(encabexpediente);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Expediente", new EncabezadoexpedienteDto(encabexpediente));
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el encabezado de expediente.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el encabezado de expediente", "guardarEncabExp" + e.getMessage());
        }

    }

    public Respuesta cargarExpediente(Long idPaciente) {
        try {
            Query qryEncabezado = em.createNamedQuery("TbEncabezadoexpediente.findByIdPaciente", TbEncabezadoexpediente.class);
            qryEncabezado.setParameter("idPaciente", idPaciente);
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Expediente", new EncabezadoexpedienteDto((TbEncabezadoexpediente) qryEncabezado.getSingleResult()));
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un expediente perteneciente a ese paciente.", "cargarExpediente NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el Expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cargar el expediente.", "cargarExpediente NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el Expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cargar el expediente.", "cargarExpediente " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarExpediente(Long id) {
        try {
            TbEncabezadoexpediente Expedienteelim;
            if (id != null && id > 0) {
                Expedienteelim = em.find(TbEncabezadoexpediente.class, id);
                if (Expedienteelim == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el Expediente a eliminar.", "eliminarExpediente NoResultException");
                }
                em.remove(Expedienteelim);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el Expediente a eliminar.", "eliminarExpediente NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al Expediente.", "eliminarExpediente " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el Expediente.", "eliminarExpediente " + e.getMessage());
        }
    }

}
