package clinicaunaWS.services;

import clinicaunaWS.model.DetallecitaDto;
import clinicaunaWS.model.TbDetallecita;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author DJork
 */
@Stateless
@LocalBean
public class DetalleCitaService {

    private static final Logger LOG = Logger.getLogger(DetalleCitaService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarDetCita(DetallecitaDto detCitaDto) {

        try {
            TbDetallecita detcita;
            // PRIMERO CONSULTAMOS SI EL PACIENTE YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if (detCitaDto.getIddetcita() != null && detCitaDto.getIddetcita() > 0) {
                detcita = em.find(TbDetallecita.class, detCitaDto.getIddetcita());
                if (detcita == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar la cita", "guardarDetCita NoResultExcepton");
                }
                detcita.actualizarDetCita(detCitaDto);
                detcita = em.merge(detcita);
            } // SI NO EXISTE EL PACIENTE ENTONCES SE PROCEDE A CREAR UNO NUEVO //
            else {
                detcita = new TbDetallecita(detCitaDto);
                em.persist(detcita);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Cita", new DetallecitaDto(detcita));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando la cita.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar la cita", "guardarCita" + ex.getMessage());
        }
    }

    public Respuesta obtenerCita(Long idMed/*, Date fecha/*, Date hora*/) {
        try {
            Query qryActividad = em.createNamedQuery("TbDetallecita.findByMedicoFechaHora", TbDetallecita.class);
            qryActividad.setParameter("idmedico", idMed);
//            qryActividad.setParameter("fechaCita", fecha);
//            qryActividad.setParameter("horaCita", fecha);
            List<TbDetallecita> citas = qryActividad.getResultList();
            List<DetallecitaDto> citasDto = new ArrayList<>();
            for (TbDetallecita paciente : citas) {
                citasDto.add(new DetallecitaDto(paciente));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Citas", citasDto);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error obteniendo las citas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al obtener las citas", "obtenerCitas" + ex.getMessage());
        }
    }

    public Respuesta eliminarCita(Long id) {
        try {
            TbDetallecita citaelim;
            if (id != null && id > 0) {
                citaelim = em.find(TbDetallecita.class, id);
                if (citaelim == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró la cita a eliminar.", "eliminarCita NoResultException");
                }
                em.remove(citaelim);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe elegir la cita que desea eliminar.", "eliminarCita NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se pudo eliminar la cita.", "eliminarCita " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la cita", "eliminarCita " + e.getMessage());
        }
    }
    
     public Respuesta obtenerCitaPorPaciente(Long idpaciente) {
        try {
            Query qryActividad = em.createNamedQuery("TbDetallecita.findByPaciente", TbDetallecita.class);
            qryActividad.setParameter("idpaciente", idpaciente);
            List<TbDetallecita> citas = qryActividad.getResultList();
            List<DetallecitaDto> citasDto = new ArrayList<>();
            for (TbDetallecita paciente : citas) {
                citasDto.add(new DetallecitaDto(paciente));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Citas", citasDto);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error obteniendo las citas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al obtener las citas", "obtenerCitas" + ex.getMessage());
        }
    }

}
