package clinicaunaWS.services;

import clinicaunaWS.model.MantusuariosDto;
import clinicaunaWS.model.TbMantusuarios;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.PasswordHelper;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Kenneth Sibaja
 */
@Stateless
@LocalBean
public class UsuarioService {

    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta validarUsuario(String usuario, String clave) {
        try {
            Query qryUsuario = em.createNamedQuery("TbMantusuarios.findByUsuPass", TbMantusuarios.class);
            qryUsuario.setParameter("usernameus", usuario);
            qryUsuario.setParameter("passwordus", clave);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new MantusuariosDto((TbMantusuarios) qryUsuario.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con las credenciales ingresadas.", "validarUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario " + ex.getMessage());
        }
    }

    public Respuesta guardarUsuario(MantusuariosDto usuarioDto) {
        System.out.println("Llego al service");
        try {
            TbMantusuarios usuario;
            // PRIMERO CONSULTAMOS SI EL PACIENTE YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if (usuarioDto.getIdUs() != null && usuarioDto.getIdUs() > 0) {
                usuario = em.find(TbMantusuarios.class, usuarioDto.getIdUs());
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el paciente", "guardarPaciente NoResultExcepton");
                }
                usuario.actualizarUsuario(usuarioDto);
                usuario = em.merge(usuario);
            } // SI NO EXISTE EL PACIENTE ENTONCES SE PROCEDE A CREAR UNO NUEVO //
            else {
                usuario = new TbMantusuarios(usuarioDto);
                em.persist(usuario);
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new MantusuariosDto(usuario));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el paciente", "guardarPaciente" + ex.getMessage());
        }
    }

    public Respuesta eliminarUsuario(Long id) {
        try {
            TbMantusuarios usuarioelim;
            if (id != null && id > 0) {
                usuarioelim = em.find(TbMantusuarios.class, id);
                if (usuarioelim == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el usuario a eliminar.", "eliminarUsuario NoResultException");
                }
                em.remove(usuarioelim);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al usuario.", "eliminarUsuario " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + e.getMessage());
        }
    }

    public Respuesta getUsuarios(String cedula, String nombre, String apellidos) {
        try {
            Query qryUsuario = em.createNamedQuery("TbMantusuarios.findByIdPers_nom_ape_ced", TbMantusuarios.class);
            qryUsuario.setParameter("cedula", cedula);
            qryUsuario.setParameter("nombre", nombre);
            qryUsuario.setParameter("apellidos", apellidos);
            List<TbMantusuarios> usuarios = qryUsuario.getResultList();
            List<MantusuariosDto> usuaiosDto = new ArrayList<>();
            for (TbMantusuarios usuario : usuarios) {
                usuaiosDto.add(new MantusuariosDto(usuario));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuarios", usuaiosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen usuarios con los criterios ingresados.", "getPacientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta activarUsuario(Long id) {
        try {
            if (id != null && id > 0) {
                TbMantusuarios usuario;
                usuario = em.find(TbMantusuarios.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el usuario a eliminar.", "eliminarUsuario NoResultException");
                }
                usuario.setEstadous("A");
                usuario = em.merge(usuario);
                return new Respuesta(false, CodigoRespuesta.CORRECTO, "Usuario Activado", "Usuairio activado correctamente");
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario NoResultException");
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al activar el usuario", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta recuperarContrasena(String username) {
        try {
            TbMantusuarios usuario;
            Query qryUsuario = em.createNamedQuery("TbMantusuarios.findByUsernameus", TbMantusuarios.class);
            qryUsuario.setParameter("usernameus", username);
            usuario = (TbMantusuarios) qryUsuario.getSingleResult();
            if (usuario == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el usuario con ese nombre.", "recuperarContrasena NoResultException");
            }
            usuario.setPasswordus("cT" + PasswordHelper.generatePassword(5) + "Tc");
            usuario = em.merge(usuario);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new MantusuariosDto(usuario));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al activar el usuario", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }
    
    public Respuesta cambiarContrasena(Long id, String newPassword){
        try {
            
            TbMantusuarios usuario;
            if (id != null && id > 0) {
                usuario = em.find(TbMantusuarios.class, id);                
                if(usuario == null){
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el usuario con ese ID.", "cambiarContrasena NoResultException");
                }
                usuario.setPasswordus(newPassword);
                usuario = em.merge(usuario);
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new MantusuariosDto(usuario));
            }else{
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el usuario con ese ID.", "cambiarContrasena NoResultException");
            }
            
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al activar el usuario", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }

}
