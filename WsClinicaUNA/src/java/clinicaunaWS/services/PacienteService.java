/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.services;

import clinicaunaWS.model.RegistropacientesDto;
import clinicaunaWS.model.TbRegistropacientes;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Kenneth Sibaja
 */
@Stateless
@LocalBean
public class PacienteService {

    private static final Logger LOG = Logger.getLogger(PacienteService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarPaciente(RegistropacientesDto pacDto) {
        System.out.println("Llego al service");
        try {
            TbRegistropacientes paciente;
            RegistropacientesDto pacienteDto;
            // PRIMERO CONSULTAMOS SI EL PACIENTE YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if (pacDto.getIdpaciente() != null && pacDto.getIdpaciente() > 0) {
                paciente = em.find(TbRegistropacientes.class, pacDto.getIdpaciente());
                if (paciente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el paciente", "guardarPaciente NoResultExcepton");
                }
                paciente.actualizarPaciente(pacDto);                
                paciente = em.merge(paciente);                
            } // SI NO EXISTE EL PACIENTE ENTONCES SE PROCEDE A CREAR UNO NUEVO //
            else {
                paciente = new TbRegistropacientes(pacDto);
                em.persist(paciente);
            }
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Paciente",  new RegistropacientesDto(paciente));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el paciente", "guardarPaciente" + ex.getMessage());
        }
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!! ARREGLAR !!!!!!!!!!!!!!!!!!!!!!!!!!!! //
    // ----------------------------  on it ------------------------------ //
    public Respuesta getPacientes(String cedula, String nombre, String apellidos) {
        try {
            Query qryPaciente = em.createNamedQuery("TbRegistropacientes.findByIdPers_nom_ape_ced", TbRegistropacientes.class);
            qryPaciente.setParameter("cedula", cedula);
            qryPaciente.setParameter("nombre", nombre);
            qryPaciente.setParameter("apellidos", apellidos);
            List<TbRegistropacientes> pacientes = qryPaciente.getResultList();
            List<RegistropacientesDto> pacientesDto = new ArrayList<>();
            for (TbRegistropacientes paciente : pacientes) {
                pacientesDto.add(new RegistropacientesDto(paciente));
            }
            
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Pacientes", pacientesDto);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen pacientes con los criterios ingresados.", "getPacientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el paciente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el paciente.", "getPacientes " + ex.getMessage());
        }
    }

    public Respuesta eliminarPaciente(Long id) {
        try {
            TbRegistropacientes elimpaciente;
            if (id != null && id > 0) {
                elimpaciente = em.find(TbRegistropacientes.class, id);
                if (elimpaciente == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el paciente a eliminar.", "eliminarPersona NoResultException");
                }
                em.remove(elimpaciente);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el paciente a eliminar.", "eliminarPaciente NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al paciente ...", "eliminarPaciente " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar al paciente.", "eliminarPaciente " + e.getMessage());
        }
    }

}
