/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.services;

import clinicaunaWS.model.DetalleexpedienteDto;
import clinicaunaWS.model.TbDetalleexpediente;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author DJork
 */
@Stateless
@LocalBean
public class DetalleExpedienteService {
         private static final Logger LOG = Logger.getLogger(DetalleExpedienteService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    
    public Respuesta guardarDetExp(DetalleexpedienteDto detExpDto){
       
        try{
            TbDetalleexpediente detexp;
            // PRIMERO CONSULTAMOS SI EL PACIENTE YA EXISTE, DE SER ASI, SUS DATOS SE ACTUALIZAN //
            if(detExpDto.getIddetexp()!= null && detExpDto.getIddetexp()> 0){
                detexp = em.find(TbDetalleexpediente.class, detExpDto.getIddetexp());
                if(detexp == null){
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el detalle del expediente", "guardarDetExpediente NoResultExcepton");
                }
                detexp.actualizarDetExp(detExpDto);
                detexp = em.merge(detexp);
            }
            // SI NO EXISTE EL PACIENTE ENTONCES SE PROCEDE A CREAR UNO NUEVO //
            else{
                detexp = new TbDetalleexpediente(detExpDto);
                em.persist(detexp);
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "DetExp", new DetalleexpedienteDto(detexp));
        }catch(Exception ex){
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el detalle del expediente.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el detalle del expediente", "guardarDetExpediente"+ex.getMessage());
        }
       
    }
    
    public Respuesta eliminarDetalle(Long id) {
        try {
            TbDetalleexpediente Detalleelim;
            if (id != null && id > 0) {
                Detalleelim = em.find(TbDetalleexpediente.class, id);
                if (Detalleelim == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el Detalle a eliminar.", "eliminarDetalle NoResultException");
                }
                em.remove(Detalleelim);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el Detalle a eliminar.", "eliminarDetalle NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al Detalle.", "eliminarDetalle " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el Detalle.", "eliminarDetalle " + e.getMessage());
        }
    }

    public Respuesta getDetallesExp(Long idExpediente) {
        try {
            Query qryDetalle = em.createNamedQuery("TbDetalleexpediente.findByIdExpediente", TbDetalleexpediente.class);
            qryDetalle.setParameter("idExpediente", idExpediente);            
            List<TbDetalleexpediente> Detalles = qryDetalle.getResultList();
            List<DetalleexpedienteDto> usuaiosDto = new ArrayList<>();
            for (TbDetalleexpediente Detalle : Detalles) {
                usuaiosDto.add(new DetalleexpedienteDto(Detalle));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Detalles", usuaiosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen Detalles con los criterios ingresados.", "getPacientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el Detalle", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el Detalle.", "getDetalle " + ex.getMessage());
        }
    }
    
}
