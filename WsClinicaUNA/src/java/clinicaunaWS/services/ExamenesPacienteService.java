
package clinicaunaWS.services;

import clinicaunaWS.model.ExamenespacienteDto;
import clinicaunaWS.model.TbExamenespaciente;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matthew Miranda
 */
@Stateless
@LocalBean
public class ExamenesPacienteService {
    
    private static final Logger LOG = Logger.getLogger(ExamenesPacienteService.class.getName());
    
    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;
    
    public Respuesta guardarExamPaciente(ExamenespacienteDto exampacDto){
        try{
            TbExamenespaciente exampaciente;
            if(exampacDto.getIdexamp() != null && exampacDto.getIdexamp() > 0){
                exampaciente = em.find(TbExamenespaciente.class, exampacDto.getIdexamp());
                if(exampaciente == null){
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el examen del paciente", "guardarExamPaciente NoResultExcepton");
                }
                exampaciente.actualizarExamenPac(exampacDto);
                exampaciente = em.merge(exampaciente);
            }
            else{
                exampaciente = new TbExamenespaciente(exampacDto);
                em.persist(exampaciente);
            }
            return new Respuesta(false, CodigoRespuesta.CORRECTO, "", "", "ExamenPaciente", new ExamenespacienteDto(exampaciente));
        }
        catch(Exception e){
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el examen del paciente.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el examen del paciente", "guardarPaciente"+e.getMessage());
        }
    }
    
    public Respuesta eliminarExamen(Long id) {
        try {
            TbExamenespaciente Examenelim;
            if (id != null && id > 0) {
                Examenelim = em.find(TbExamenespaciente.class, id);
                if (Examenelim == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el Examen a eliminar.", "eliminarExamen NoResultException");
                }
                em.remove(Examenelim);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el Examen a eliminar.", "eliminarExamen NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al Examen.", "eliminarExamen " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el Examen.", "eliminarExamen " + e.getMessage());
        }
    }

    public Respuesta getExamenes(Long idExpediente) {
        try {
            Query qryExamen = em.createNamedQuery("TbExamenespaciente.findByIDExpediente", TbExamenespaciente.class);
            qryExamen.setParameter("idExpediente", idExpediente);
            List<TbExamenespaciente> Examens = qryExamen.getResultList();
            List<ExamenespacienteDto> usuaiosDto = new ArrayList<>();
            for (TbExamenespaciente Examen : Examens) {
                usuaiosDto.add(new ExamenespacienteDto(Examen));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Examenes", usuaiosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen Examen con los criterios ingresados.", "getExamenes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el Examen", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el Examenes.", "getExamen " + ex.getMessage());
        }
    }
    
}
