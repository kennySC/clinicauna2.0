package clinicaunaWS.services;

import clinicaunaWS.model.AntecedentesheredofamDto;
import clinicaunaWS.model.TbAntecedentesheredofam;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Matthew Miranda
 */
@Stateless
@LocalBean
public class AntecedentesHeredoFamService {

    private static final Logger LOG = Logger.getLogger(AntecedentesHeredoFamService.class.getName());

    @PersistenceContext(unitName = "WsClinicaUNAPU")
    private EntityManager em;

    public Respuesta guardarAntecedenteHF(AntecedentesheredofamDto antecedentehfDto) {
        try {
            TbAntecedentesheredofam antecedentehf;
            if (antecedentehfDto.getIdantecedheredfam() != null && antecedentehfDto.getIdantecedheredfam() > 0) {
                antecedentehf = em.find(TbAntecedentesheredofam.class, antecedentehfDto.getIdantecedheredfam());
                if (antecedentehf == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el antecedente heredo familiar", "guardarAntecedenteHF NoResultExcepton");
                }
                antecedentehf.actualizarAntecedenteHF(antecedentehfDto);
                antecedentehf = em.merge(antecedentehf);
            }
            else{
                antecedentehf = new TbAntecedentesheredofam(antecedentehfDto);
                em.persist(antecedentehf);
            }
            return new Respuesta(false, CodigoRespuesta.CORRECTO, "", "", "AntecedenteHeredoFamiliar", new AntecedentesheredofamDto(antecedentehf));
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error guardando el antecedente heredo familiar.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Error al guardar el antecedente heredo familiar", "guardarAntecedenteHF" + e.getMessage());
        }
    }

    public Respuesta eliminarAntecedente(Long id) {
        try {
            TbAntecedentesheredofam antecedenteEliminar;
            if (id != null && id > 0) {
                antecedenteEliminar = em.find(TbAntecedentesheredofam.class, id);
                if (antecedenteEliminar == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el Antecedente a eliminar.", "eliminarAntecedente NoResultException");
                }
                em.remove(antecedenteEliminar);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el Antecedente a eliminar.", "eliminarAntecedente NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar al Antecedente.", "eliminarAntecedente " + e.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el empleado.", e);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el Antecedente.", "eliminarAntecedente " + e.getMessage());
        }
    }

    public Respuesta getAntecedentes(Long idExpediente) {
        try {
            Query qryAntecedente = em.createNamedQuery("TbAntecedentesheredofam.findByIDExpediente", TbAntecedentesheredofam.class);
            qryAntecedente.setParameter("idExpediente", idExpediente);            
            List<TbAntecedentesheredofam> Antecedentes = qryAntecedente.getResultList();
            List<AntecedentesheredofamDto> usuaiosDto = new ArrayList<>();
            for (TbAntecedentesheredofam Antecedente : Antecedentes) {
                usuaiosDto.add(new AntecedentesheredofamDto(Antecedente));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Antecedentes", usuaiosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen Antecedentes con los criterios ingresados.", "getPacientes NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el Antecedente", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el Antecedente.", "getAntecedente " + ex.getMessage());
        }
    }
    
}
