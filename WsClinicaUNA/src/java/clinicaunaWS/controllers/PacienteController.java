
package clinicaunaWS.controllers;

import clinicaunaWS.model.RegistropacientesDto;
import clinicaunaWS.services.PacienteService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kenneth Sibaja
 */

@Path("/PacienteController")
public class PacienteController {
    
    @EJB
    PacienteService pacienteServ;
    
    @POST
    @Path("/guardarPaciente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPaciente(RegistropacientesDto paciente){
        System.out.println("LLego al controller");
        try{                            
            Respuesta resPaciente = pacienteServ.guardarPaciente(paciente);
            if(!resPaciente.getEstado()){
                return Response.status(resPaciente.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((RegistropacientesDto) resPaciente.getResultado("Paciente")).build();                                      
        }catch(Exception ex){
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el Paciente").build();
        }
    }
    
    @GET
    @Path("/pacientes/{cedula}/{nombre}/{apellidos}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPacientes(@PathParam("cedula") String cedula, @PathParam("nombre") String nombre, @PathParam("apellidos") String apellidos) {
        try {
            Respuesta res = pacienteServ.getPacientes(cedula, nombre, apellidos);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<RegistropacientesDto>>((List<RegistropacientesDto>) res.getResultado("Pacientes")){}).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los pacientes").build();
        }        
    }    
    
    @DELETE
    @Path("/eliminarPaciente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarPaciente(@PathParam("id")Long id){
        try{
            Respuesta resp = pacienteServ.eliminarPaciente(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        }
        catch(Exception e){
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el paciente").build();
        }
    }        
    
}
