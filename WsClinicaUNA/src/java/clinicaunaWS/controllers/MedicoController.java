package clinicaunaWS.controllers;

import clinicaunaWS.model.MantmedicosDto;
import clinicaunaWS.model.RegistropacientesDto;
import clinicaunaWS.services.MedicoService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author DJork
 */
@Path("/MedicoController")
public class MedicoController {

    @EJB
    MedicoService mService;

    @POST
    @Path("/GuardarMedico")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response GuardarMedico(MantmedicosDto medi) {
        System.out.println("Controller");
        try {
            Respuesta res = mService.guardarMedico(medi);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((MantmedicosDto) res.getResultado("Medico")).build();
        } catch (Exception ex) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el Medico").build();
        }
    }

    @GET
    @Path("/medicos/{cedula}/{nombre}/{apellidos}")/*/{carne}*/
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMedicos(@PathParam("cedula") String cedula, @PathParam("nombre") String nombre, @PathParam("apellidos") String apellidos/*, @PathParam("carne")Integer carne*/) {
        try {
            Respuesta resp = mService.getMedicos(nombre, apellidos, cedula/*, carne*/);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<MantmedicosDto>>((List<MantmedicosDto>) resp.getResultado("Medicos")) {
            }).build();
        } catch (Exception e) {
            Logger.getLogger(MedicoController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los medicos").build();
        }
    }

    @DELETE
    @Path("/eliminarMedico/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarMedico(@PathParam("id") Long id) {
        try {
            Respuesta resp = mService.eliminarMedico(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando al medico").build();
        }
    }

}
