package clinicaunaWS.controllers;

import clinicaunaWS.model.DetallecitaDto;
import clinicaunaWS.services.DetalleCitaService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author DJork
 */
@Path("/DetalleCitaController")
public class DetalleCitaController {

    @EJB
    DetalleCitaService citaService;

    @POST
    @Path("/guardarDetCita")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarDetCita(DetallecitaDto detCitaDto) {
        try {
            Respuesta res = citaService.guardarDetCita(detCitaDto);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((DetallecitaDto) res.getResultado("Cita")).build();
        } catch (Exception ex) {
            Logger.getLogger(DetalleCitaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando la cita").build();
        }
    }
    
    @GET
    @Path("/obtenerCitas/{idMed}/"/*{fecha}"/{hora}"*/)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerCita(@PathParam("idMed")Long idMed/*, @PathParam("fecha")LocalDate fecha/*, @PathParam("hora") LocalDateTime hora*/){
        try {
            Respuesta res = citaService.obtenerCita(idMed/*, Date.valueOf(fecha)/*, Date.from(hora.atZone(ZoneId.systemDefault()).toInstant())*/);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok(new GenericEntity<List<DetallecitaDto>>((List<DetallecitaDto>) res.getResultado("Citas")){}).build();
        } catch (Exception ex) {
            Logger.getLogger(DetalleCitaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la cita").build();
        }
    }
   @GET
    @Path("/obtenerCitasPaciente/{idpaciente}/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerCitasPaciente(@PathParam("idpaciente")Long idpaciente){
        try {
            Respuesta res = citaService.obtenerCitaPorPaciente(idpaciente);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok(new GenericEntity<List<DetallecitaDto>>((List<DetallecitaDto>) res.getResultado("Citas")){}).build();
        } catch (Exception ex) {
            Logger.getLogger(DetalleCitaController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la cita").build();
        }
    }
    @DELETE
    @Path("/eliminarCita/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarCita(@PathParam("id") Long id) {
        try {
            Respuesta resp = citaService.eliminarCita(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(DetalleCitaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la cita").build();
        }
    }

}
