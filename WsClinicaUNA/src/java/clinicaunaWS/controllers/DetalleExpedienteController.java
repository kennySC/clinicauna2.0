/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicaunaWS.controllers;
import clinicaunaWS.model.DetalleexpedienteDto;
import clinicaunaWS.services.DetalleExpedienteService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 *
 * @author DJork
 */
@Path("/DetalleExpedienteController")
public class DetalleExpedienteController {
     @EJB
     DetalleExpedienteService dService;
    
    @POST
    @Path("/guardarDetalle")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response GuardarDetExp(DetalleexpedienteDto detexp){
        try{
            Respuesta res = dService.guardarDetExp(detexp);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((DetalleexpedienteDto) res.getResultado("DetExp")).build();
        }catch(Exception ex){
            Logger.getLogger(DetalleExpedienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el detalle del expediente").build();
        }
    }
    
    @DELETE
    @Path("/eliminarDetalle/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarDetalle(@PathParam("id") Long id) {
        try {
            Respuesta resp = dService.eliminarDetalle(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(DetalleExpedienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando al Detalle").build();
        }
    }

    @GET
    @Path("/Detalles/{idExpediente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDetalles(@PathParam("idExpediente")  Long idExpediente) {
        try {
            Respuesta res = dService.getDetallesExp(idExpediente);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<DetalleexpedienteDto>>((List<DetalleexpedienteDto>) res.getResultado("Detalles")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(DetalleExpedienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los Detalles").build();
        }
    }
    
}
