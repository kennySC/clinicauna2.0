package clinicaunaWS.controllers;

import clinicaunaWS.model.MantusuariosDto;
import clinicaunaWS.services.UsuarioService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kenneth Sibaja
 */
@Path("/UsuarioController")
public class UsuarioController {

    @EJB
    UsuarioService usService;

    @GET
    @Path("/validarUsuario/{usuario}/{clave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsuario(@PathParam("usuario") String usuario, @PathParam("clave") String clave) {
        try {
            Respuesta res = usService.validarUsuario(usuario, clave);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((MantusuariosDto) res.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el usuario").build();
        }
    }

    @POST
    @Path("/guardarUsuario")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarUsuario(MantusuariosDto usuario) {
        System.out.println("LLego al controller");
        try {
            //  Primero intentamos guardar la persona del Usuario                               
            Respuesta resUsuario = usService.guardarUsuario(usuario);
            if (!resUsuario.getEstado()) {
                return Response.status(resUsuario.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((MantusuariosDto) resUsuario.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el Usuario").build();
        }
    }

    @DELETE
    @Path("/eliminarUsuario/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarUsuario(@PathParam("id") Long id) {
        try {
            Respuesta resp = usService.eliminarUsuario(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando al usuario").build();
        }
    }

    @GET
    @Path("/usuarios/{cedula}/{nombre}/{apellidos}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarios(@PathParam("cedula") String cedula, @PathParam("nombre") String nombre, @PathParam("apellidos") String apellidos) {
        try {
            Respuesta res = usService.getUsuarios(cedula, nombre, apellidos);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<MantusuariosDto>>((List<MantusuariosDto>) res.getResultado("Usuarios")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    
    @GET
    @Path("/activarUsuario/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response activarUsuario(@PathParam("id") Long id){
        try {
            Respuesta res = usService.activarUsuario(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    
    @GET
    @Path("/recuperarContrasena/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response recuperarContrasena(@PathParam("username") String username){
        try {
            Respuesta res = usService.recuperarContrasena(username);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((MantusuariosDto) res.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    
    @GET
    @Path("/cambiarContrasena/{id}/{newPassword}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cambiarContrasena(@PathParam("id") Long id, @PathParam("newPassword") String newPassword){
        try {
            Respuesta res = usService.cambiarContrasena(id, newPassword);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((MantusuariosDto) res.getResultado("Usuario")).build();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los usuarios").build();
        }
    }
    
}
