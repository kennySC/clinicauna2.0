
package clinicaunaWS.controllers;

import clinicaunaWS.model.PersonaDto;
import clinicaunaWS.services.PersonaService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matthew Miranda
 */
@Path("/PersonaController")
public class PersonaController {
    
    @EJB
    PersonaService personaServ;
    
    @POST
    @Path("/guardarPersona")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarPersona(PersonaDto persona){
        System.out.println("LLego al controller");
        try{            
            Respuesta res = personaServ.guardarPersona(persona);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((PersonaDto)res.getResultado("Persona")).build();
        }
        catch(Exception e){
            Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando la persona").build();
        }
    }
    
    @DELETE
    @Path("/eliminarPersona/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarPersona(@PathParam("id")Long id){
        try{
            Respuesta resp = personaServ.eliminarPersona(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        }
        catch(Exception e){
            Logger.getLogger(PersonaController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la persona").build();
        }
    }
    
}
