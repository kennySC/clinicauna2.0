
package clinicaunaWS.controllers;

import clinicaunaWS.model.ExamenespacienteDto;
import clinicaunaWS.services.ExamenesPacienteService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matthew Miranda
 */
@Path("/ExamenesPacienteController")
public class ExamenesPacienteController {
    
    @EJB
    ExamenesPacienteService exampacServ;
    
    @POST
    @Path("/guardarExamPaciente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    
    public Response guardarExamenPaciente(ExamenespacienteDto exampacDto){
        try{
            Respuesta res = exampacServ.guardarExamPaciente(exampacDto);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok(res.getCodigoRespuesta().getValue()).build();
        }
        catch(Exception e){
            Logger.getLogger(ExamenesPacienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el Paciente").build();
        }
    }
    
    @DELETE
    @Path("/eliminarExamen/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarExamen(@PathParam("id") Long id) {
        try {
            Respuesta resp = exampacServ.eliminarExamen(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(ExamenesPacienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando al Examen").build();
        }
    }

    @GET
    @Path("/Examenes/{idExpediente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExamenes(@PathParam("idExpediente")  Long idExpediente) {
        try {
            Respuesta res = exampacServ.getExamenes(idExpediente);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<ExamenespacienteDto>>((List<ExamenespacienteDto>) res.getResultado("Examenes")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(ExamenesPacienteController.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los Examenes").build();
        }
    }
    
}
