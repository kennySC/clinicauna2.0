
package clinicaunaWS.controllers;

import clinicaunaWS.model.EncabezadoexpedienteDto;
import clinicaunaWS.services.EncabezadoExpedienteService;
import clinicaunaWS.util.CodigoRespuesta;
import clinicaunaWS.util.Respuesta;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author Matthew Miranda
 */
@Path("/ExpedienteController")
public class EncabezadoExpedienteController {
    
    @EJB
    EncabezadoExpedienteService encabexpServ;
    
    @POST
    @Path("/guardarExpediente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarEncabezadoExpediente(EncabezadoexpedienteDto encabexpDto){
        try{
            Respuesta res = encabexpServ.guardarEncabExp(encabexpDto);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((EncabezadoexpedienteDto) res.getResultado("Expediente")).build();
        }
        catch(Exception e){
            Logger.getLogger(EncabezadoExpedienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el encabezado de expediente").build();
        }
    }
        
    @GET
    @Path("/expediente/{idPaciente}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)  
    public Response cargarExpediente(@PathParam("idPaciente") Long idPaciente){
        try {
            Respuesta res = encabexpServ.cargarExpediente(idPaciente);
            if(!res.getEstado()){
                return Response.status(res.getCodigoRespuesta().getValue()).build();
            }
            return Response.ok((EncabezadoexpedienteDto) res.getResultado("Expediente")).build();
        } catch (Exception e) {
            Logger.getLogger(EncabezadoExpedienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error Guardando el encabezado de expediente").build();            
        }
    }
    
    @DELETE
    @Path("/eliminarExpediente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarExpediente(@PathParam("id") Long id) {
        try {
            Respuesta resp = encabexpServ.eliminarExpediente(id);
            if (!resp.getEstado()) {
                return Response.status(resp.getCodigoRespuesta().getValue()).entity(resp.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception e) {
            Logger.getLogger(EncabezadoExpedienteController.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el Expediente").build();
        }
    }
    
}
