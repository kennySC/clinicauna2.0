package clinicaunaWS.model;

import clinicaunaWS.model.TbDetalleexpediente;
import clinicaunaWS.model.TbExamenespaciente;
import clinicaunaWS.model.TbRegistropacientes;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbEncabezadoexpediente.class)
public class TbEncabezadoexpediente_ { 

    public static volatile SingularAttribute<TbEncabezadoexpediente, String> patologicoencabexp;
    public static volatile ListAttribute<TbEncabezadoexpediente, TbDetalleexpediente> tbDetalleexpedienteList;
    public static volatile ListAttribute<TbEncabezadoexpediente, TbExamenespaciente> tbExamenespacienteList;
    public static volatile SingularAttribute<TbEncabezadoexpediente, Integer> versionencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, String> operacionesencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, String> tratamientosencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, Integer> hospitalizacionesencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, String> parentescoencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, TbRegistropacientes> idpaciente;
    public static volatile SingularAttribute<TbEncabezadoexpediente, Long> idencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, String> alergiasencabexp;
    public static volatile SingularAttribute<TbEncabezadoexpediente, String> enfermheredencabexp;

}