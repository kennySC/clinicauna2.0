package clinicaunaWS.model;

import clinicaunaWS.model.TbMantmedicos;
import clinicaunaWS.model.TbMantusuarios;
import clinicaunaWS.model.TbRegistropacientes;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbDetallecita.class)
public class TbDetallecita_ { 

    public static volatile SingularAttribute<TbDetallecita, Date> fechacita;
    public static volatile SingularAttribute<TbDetallecita, Integer> espaciomedcita;
    public static volatile SingularAttribute<TbDetallecita, Integer> versioncita;
    public static volatile SingularAttribute<TbDetallecita, Date> horacita;
    public static volatile SingularAttribute<TbDetallecita, String> estadocita;
    public static volatile SingularAttribute<TbDetallecita, TbMantmedicos> idmed;
    public static volatile SingularAttribute<TbDetallecita, TbMantusuarios> idus;
    public static volatile SingularAttribute<TbDetallecita, Long> iddetcita;
    public static volatile SingularAttribute<TbDetallecita, String> motivocita;
    public static volatile SingularAttribute<TbDetallecita, TbRegistropacientes> idpaciente;

}