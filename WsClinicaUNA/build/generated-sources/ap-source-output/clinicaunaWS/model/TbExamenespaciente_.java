package clinicaunaWS.model;

import clinicaunaWS.model.TbDetalleexpediente;
import clinicaunaWS.model.TbEncabezadoexpediente;
import clinicaunaWS.model.TbRegistropacientes;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbExamenespaciente.class)
public class TbExamenespaciente_ { 

    public static volatile SingularAttribute<TbExamenespaciente, TbDetalleexpediente> iddetexp;
    public static volatile SingularAttribute<TbExamenespaciente, String> nombreexamp;
    public static volatile SingularAttribute<TbExamenespaciente, String> anotacionesexamp;
    public static volatile SingularAttribute<TbExamenespaciente, Integer> versionexamp;
    public static volatile SingularAttribute<TbExamenespaciente, Date> fechaexamp;
    public static volatile SingularAttribute<TbExamenespaciente, Long> idexamp;
    public static volatile SingularAttribute<TbExamenespaciente, TbRegistropacientes> idpaciente;
    public static volatile SingularAttribute<TbExamenespaciente, TbEncabezadoexpediente> idencabexp;

}