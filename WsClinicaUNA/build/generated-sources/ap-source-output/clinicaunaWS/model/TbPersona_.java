package clinicaunaWS.model;

import clinicaunaWS.model.TbMantmedicos;
import clinicaunaWS.model.TbMantusuarios;
import clinicaunaWS.model.TbRegistropacientes;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbPersona.class)
public class TbPersona_ { 

    public static volatile SingularAttribute<TbPersona, Integer> versionpers;
    public static volatile SingularAttribute<TbPersona, Long> idpers;
    public static volatile SingularAttribute<TbPersona, String> correopers;
    public static volatile SingularAttribute<TbPersona, String> cedulapers;
    public static volatile SingularAttribute<TbPersona, String> nombrepers;
    public static volatile ListAttribute<TbPersona, TbMantusuarios> tbMantusuariosList;
    public static volatile ListAttribute<TbPersona, TbRegistropacientes> tbRegistropacientesList;
    public static volatile SingularAttribute<TbPersona, String> apellidospersona;
    public static volatile ListAttribute<TbPersona, TbMantmedicos> tbMantmedicosList;

}