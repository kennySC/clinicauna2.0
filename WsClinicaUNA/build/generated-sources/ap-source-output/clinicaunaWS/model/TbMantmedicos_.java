package clinicaunaWS.model;

import clinicaunaWS.model.TbDetallecita;
import clinicaunaWS.model.TbMantusuarios;
import clinicaunaWS.model.TbPersona;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbMantmedicos.class)
public class TbMantmedicos_ { 

    public static volatile SingularAttribute<TbMantmedicos, Integer> espaciosmed;
    public static volatile SingularAttribute<TbMantmedicos, Long> foliomed;
    public static volatile SingularAttribute<TbMantmedicos, String> estadomed;
    public static volatile SingularAttribute<TbMantmedicos, TbPersona> idpers;
    public static volatile SingularAttribute<TbMantmedicos, Long> codigomed;
    public static volatile SingularAttribute<TbMantmedicos, Long> idmed;
    public static volatile SingularAttribute<TbMantmedicos, Integer> carnemed;
    public static volatile ListAttribute<TbMantmedicos, TbDetallecita> tbDetallecitaList;
    public static volatile SingularAttribute<TbMantmedicos, TbMantusuarios> idus;
    public static volatile SingularAttribute<TbMantmedicos, Date> finjornadamed;
    public static volatile SingularAttribute<TbMantmedicos, Integer> versionmed;
    public static volatile SingularAttribute<TbMantmedicos, Date> inijornadamed;

}