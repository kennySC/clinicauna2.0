package clinicaunaWS.model;

import clinicaunaWS.model.TbDetallecita;
import clinicaunaWS.model.TbMantmedicos;
import clinicaunaWS.model.TbPersona;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbMantusuarios.class)
public class TbMantusuarios_ { 

    public static volatile SingularAttribute<TbMantusuarios, TbPersona> idpers;
    public static volatile SingularAttribute<TbMantusuarios, String> idiomaus;
    public static volatile SingularAttribute<TbMantusuarios, Integer> versionus;
    public static volatile ListAttribute<TbMantusuarios, TbDetallecita> tbDetallecitaList;
    public static volatile SingularAttribute<TbMantusuarios, Long> idus;
    public static volatile SingularAttribute<TbMantusuarios, String> tipous;
    public static volatile SingularAttribute<TbMantusuarios, String> passwordus;
    public static volatile SingularAttribute<TbMantusuarios, String> usernameus;
    public static volatile SingularAttribute<TbMantusuarios, String> estadous;
    public static volatile ListAttribute<TbMantusuarios, TbMantmedicos> tbMantmedicosList;

}