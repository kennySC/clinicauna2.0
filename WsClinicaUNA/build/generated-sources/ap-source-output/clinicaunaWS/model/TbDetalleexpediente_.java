package clinicaunaWS.model;

import clinicaunaWS.model.TbEncabezadoexpediente;
import clinicaunaWS.model.TbExamenespaciente;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-10-10T10:13:13")
@StaticMetamodel(TbDetalleexpediente.class)
public class TbDetalleexpediente_ { 

    public static volatile ListAttribute<TbDetalleexpediente, TbExamenespaciente> tbExamenespacienteList;
    public static volatile SingularAttribute<TbDetalleexpediente, String> talladetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> planatenciondetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Double> imcdetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Date> horadetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, TbEncabezadoexpediente> idencabexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Double> presiondetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Long> iddetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Double> temperaturadetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> anotacionesdetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> observdetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> freccarddetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Date> fechadetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> tratamientodetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> motivoconsultadetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Double> pesodetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, Integer> versiondetexp;
    public static volatile SingularAttribute<TbDetalleexpediente, String> examfisicodetexp;

}